from artiq.experiment import *
from artiq.language.environment import NumberValue
from artiq.language.scan import RangeScan
import numpy as np


class PDCalibration(EnvExperiment):
    def build(self):
        self.setattr_device("core")
        self.setattr_device("sampler0")
        self.setattr_argument("Notes", StringValue())
        self.setattr_argument("pd_channel_num", NumberValue(ndecimals=0,step=1))
        self.setattr_argument("power_meter_channel_num", NumberValue(ndecimals=0,step=1))
        self.setattr_argument("power_meter_reading", NumberValue(ndecimals=3, step=1))
        self.setattr_argument("power_meter_range", NumberValue(ndecimals=3, step=1))
        self.setattr_argument("n_datapoints", NumberValue(default=100, ndecimals=0, step=1))

        self.a = 1 #needed if running artiq 7.8123

    @kernel
    def run(self):

        self.a #needed if running artiq 7.8123
        
        ### Core initialization
        self.core.reset()
        
        ### Sampler initialization
        self.sampler0.init()
        self.sampler0.set_gain_mu(0,0)
        self.sampler0.set_gain_mu(1,0)
        self.sampler0.set_gain_mu(2,0)
        self.sampler0.set_gain_mu(3,0)


        ## Creating datasets
        self.set_dataset("Notes", self.Notes)
        self.set_dataset("pd_channel_num", self.pd_channel_num)
        self.set_dataset("power_meter_reading", self.power_meter_reading)
        self.set_dataset("power_meter_range", self.power_meter_range)
        self.set_dataset("n_datapoints", self.n_datapoints)
        

        smp_arr_pd0 = [0.0]*self.n_datapoints
        smp_arr_pd1 = [0.0]*self.n_datapoints
        smp_arr_pd2 = [0.0]*self.n_datapoints
        smp_arr_pd3 = [0.0]*self.n_datapoints

        for j in range(self.n_datapoints):
            smp = [0.0]*8
            delay(1*ms)
            self.sampler0.sample(smp)
            smp_arr_pd0[j] = smp[0]
            smp_arr_pd1[j] = smp[1]
            smp_arr_pd2[j] = smp[2]
            smp_arr_pd3[j] = smp[3]

        self.set_dataset("pd0", smp_arr_pd0, broadcast=True)
        self.set_dataset("pd1", smp_arr_pd1, broadcast=True)
        self.set_dataset("pd2", smp_arr_pd2, broadcast=True)
        self.set_dataset("pd3", smp_arr_pd3, broadcast=True)

