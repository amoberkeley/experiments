from artiq.experiment import *
import numpy as np

class TTL2_1Hz_chop(EnvExperiment):
    
    def build(self):
        self.setattr_device("core")
        self.setattr_device("ttl0")
        self.setattr_device("ttl1")
        #self.setattr_device("ttl2")
        # self.setattr_device("ttl3")
        # self.setattr_device("ttl5")
        # self.setattr_device("ttl6")
        self.setattr_device("zotino0")
        # self.setattr_device("urukul0_ch3")
        # self.setattr_device("urukul0_ch2")
        self.a = 1


    @kernel
    def run(self):

        self.core.reset()

        self.zotino0.init()

        self.ttl0.output()
        self.ttl1.output()
        #self.ttl2.output()
        self.a
        delay(10*ms)
        self.core.break_realtime()

        for i in range(1000):
            #delay(25*ms-500*us)
            delay(80*ms)
            self.ttl0.pulse(100*us)
            self.ttl1.pulse(100*us)
        # self.ttl1.output()
        # self.ttl2.output()
        # self.ttl3.output()
        # self.ttl5.output()
        # self.ttl6.output()
        # self.zotino0.init()
        # self.a

        # #######################
        # ### Urukul initialization
        # self.core.break_realtime()
        # delay(3*s)
        # self.urukul0_ch2.cpld.init()
        # self.urukul0_ch2.init()
        # self.urukul0_ch3.cpld.init()
        # self.urukul0_ch3.init()


        # freq = 200*MHz
        # amp = 0.12
        # attenuation = 0.0*dB

        # self.urukul0_ch2.set_att(attenuation)
        # self.urukul0_ch2.set(220 * MHz, amplitude = 0.025)
        # self.core.break_realtime()
        # self.urukul0_ch2.sw.on()

        # self.urukul0_ch3.set_att(attenuation)
        # self.urukul0_ch3.set(freq, amplitude = amp)
        # self.core.break_realtime()
        # self.urukul0_ch3.sw.on()

        # self.ttl2.off()

        # delay(10*ms)

        # # self.zotino0.set_dac([0.200], [0])
        # self.zotino0.set_dac([0.3], [0])
        # delay(100*ms)
        # with parallel:
        #     self.ttl2.on()
        #     # self.ttl3.on()
        #     # self.ttl5.on()
        #     # self.ttl6.on()
        # for i in range(50):
        #     with parallel:
        #         self.ttl2.off()
        #         # self.ttl3.off()
        #         # self.ttl6.off()
        #     delay(0.5*s)
        #     with parallel:
        #         self.ttl2.on()
        #         # self.ttl3.on()
        #         # self.ttl6.on()
        #     delay(0.5*s)
        # with parallel:
        #     self.ttl2.off()
        #     # self.ttl3.on()
        #     # self.ttl6.on()