from artiq.experiment import *
import numpy as np

class TiCoolingHFS(EnvExperiment):
	def build(self):

		self.setattr_device('core') #artiq core CPU
		self.setattr_device('ttl1') #ablation laser trigger
		self.setattr_device('ttl0') #red pitaya trigger
		self.setattr_device('urukul0_ch0') #AO drive
		self.setattr_device('redpitaya0') #red pitaya

		self.setattr_argument('laser_scan_frequencies_MHz', Scannable(global_max=20,
		  														global_min=-20,
		 														ndecimals=6,
		  														default=[RangeScan(-25, 25, 5)]))
		#add an argument for Ntraces???
		self.a=1

		self.u0_ch0_freq_MHz = 80 * 10e-6
		self.u_ch0_amp_V = 0.12*V
		self.u0_ch0_att_dB = 0.0*dB

	def prepare(self):
		self.laser_scan_frequencies = np.array(len(laser_scan_frequencies_MHz)) * 10e-6

	###in order to be able to average the number of traces I take
	@kernel(flags={"fast-math"})
    def get_mean_and_std(self, arr):
        meanN = 0.0
        stdNS = 0.0
        N = len(arr)

        #mean
        for a in arr:
            meanN += a

        mean = meanN/N

        #std
        for a in arr:
            stdNS += (a-mean)**2

        std = np.sqrt(stdNS/N)

        return mean, std

	@kernel
	def run(self):

		#init CPU(core)
		self.core.wait_until_mu(now_mu())
		self.core.reset()
		self.a

		#init ttl's
		self.ttl0.output()
		self.ttl1.output()

		#init urukul
		self.core.break_realtime()
		delay(3*s)
		self.urukul0_ch0.init()

		#setting freq, amps, attentuation
		self.urukul0_ch0.set(self.u0_ch0_freq_MHz, amplitude = self.u_ch0_amp_V)
		self.urukul0_ch0.set_att(self.u0_ch0_att_dB)
		self.core.break_realtime()
		delay(0.1*s)

		#initialize redpitaya
		self.redpitaya0.initialize()

		#turning on urukul ch0
		self.core.break_realtime()
		self.urukul0_ch0.sw.on()

		###take scan
		self.redpitaya0.record_next_N_traces(len(self.laser_scan_frequencies), 1000, decimation=64)
		self.core.break_realtime()
		delay(0.1*s)

		### initialize wavemeter here (when code is not buggy)
		### add code that measures frequencies from wavemeter

		### loop
		for shift in self.laser_scan_frequencies:

			self.ttl0.pulse(1*ms)
			delay(0.1*s)



###psudocode for script after initializations (in line)

	###from redpitaya0, take scan, this scan could be equal to the length of array self.laser_scan_frequencies (in MHz)
	###loop through frequency shifts from AOM in steps

		#shift_count=0
		#pulse_count=0
		#for shift in self.laser_scan_frequencies:

			### set laser frequency with wavemeter
			### wait for laser control to settle before measuring this frequency
			### measure and record laser frequency
			### set a fresh time cursor

			###loop through Ntraces to take throughout scan
			#for i in range(self.Ntraces):
				###only laser (no Ti) bright field trace
				#self.ttl0.pulse(1*ms)
				#delay(0.1*s)

				###laser bright field trace (yes Ti: trigger ablation)
				#self.ttl0.pulse(1*ms)
				#for j in range(25):
					#with parallel:
						#self.ttl1.pulse(100*us) #pulse ablation laser
						#delay(1*ms)
				#delay(0.1*s)
				###for above loops, at the end of each, could add a pulse_count += 1
				###could also add shif_count += 1 (add delays between)

			### wait until all pulse sequences complete
            #self.core.break_realtime()
            #delay(1*s)
            #self.core.wait_until_mu(now_mu())
            #print("Done.)

        ###when all sequences are done, run this prior to data analysis function definition
        #self.core.wait_until_mu(now_mu())
		#print("Appending to buffer...")
		#self.redpitaya0.append_traces_to_buffer()
		#print("Plotting...")
		#self.redpitaya0.show_avg_traces(chb_on=False)
		#self.redpitaya0.save_buffer_to_dataset(self)
		#print("Done.")


    ###data analysis        
	# def analyze(self):
	# 	print('saving...')
	# 	self.set_dataset('laser_scan_frequencies', self.laser_scan_frequencies)
	#	add a set_dataset for Ntraces??? this would have to be done after argument deinition

