from artiq.experiment import *
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

class AblationSpectroTi(EnvExperiment):
	def build(self):

		self.setattr_device('core') #artiq CPU
		self.setattr_device('ttl0') #redpitaya trigger --> rerouted to PICOSCOPE trigger
		self.setattr_device('ttl1') #ablation laser trigger
		self.setattr_device('urukul0_ch1') #200 MHz tuning AO
		# self.setattr_device('redpitaya0') #redpitaya
		self.setattr_device("wlm0") # wavemeter
		self.a=1

		self.setattr_argument('coarse_scan_freq_THz', Scannable(global_min=601.6135, global_max=601.6175, ndecimals=6, default=[RangeScan(601.615, 601.616, 4)]))
		self.setattr_argument('AO_scan_freq_MHz', Scannable(global_min=-40, global_max=40, ndecimals=0, default=[RangeScan(-20, 20, 41)]))
		self.setattr_argument('N_samples', NumberValue(default=10000, ndecimals=0, step=1))
		self.setattr_argument('N_avgs', NumberValue(default=10, ndecimals=0, step=1))

		self.u0_ch1_freq = 200 * MHz
		self.u0_ch1_amp_V = 0.35*V
		self.u0_ch1_att_dB = 0.0*dB
		#self.scan_center_freq = 601.6159 * 10e6 #for wavemeter

		self.N_warm_up_shots = 5

		self.wlm_ch = 5

	def prepare(self):

		self.coarse_scan_freq_THz = np.array(list(self.coarse_scan_freq_THz))
		self.AO_scan_freq_MHz = np.array(list(self.AO_scan_freq_MHz))

		self.coarse_scan_freq_THz_strings = [str(freq) for freq in self.coarse_scan_freq_THz]
		print(self.coarse_scan_freq_THz_strings)
		self.N_avgs = int(self.N_avgs)
		# self.AO_scan_freq_MHz = np.array([shift for shift in self.AO_scan_freq_MHz])
		# self.measured_AO_scan_freq_MHz = np.zeros(len(self.AO_scan_freq_MHz))

	@kernel
	def run(self):

		#init CPU
		self.core.reset()
		self.a
		
		#init ttl
		self.ttl0.output()
		self.ttl1.output()

		#init urukul
		self.urukul0_ch1.init()
		self.urukul0_ch1.set(self.u0_ch1_freq, amplitude = self.u0_ch1_amp_V)
		self.urukul0_ch1.set_att(self.u0_ch1_att_dB)
		self.core.break_realtime()
		delay(1*s)

		#initialize redpitaya
		# self.redpitaya0.initialize()
		# self.redpitaya0.record_next_N_traces(1, 15000, decimation=64)
		# self.core.break_realtime()
		# delay(1*s)


		#turning urukul ch0 on
		self.core.break_realtime()
		delay(1*ms)
		self.urukul0_ch1.sw.on()

		# ## save the background trace
		# self.core.wait_until_mu(now_mu())
		# delay(100*ms)
		# self.redpitaya0.append_traces_to_buffer()
		# self.redpitaya0.save_buffer_to_dataset(self, names=("mts_dark",  "dopp_dark"))
		# self.redpitaya0.clear_buffers()


		###taking scan
		self.core.break_realtime()
		delay(1*s)

		for i in range(len(self.coarse_scan_freq_THz)):

			print("Tuning coarse wlm lock (THz): ", self.coarse_scan_freq_THz[i])

			## tune wavemeter lock
			self.wlm0.SetPIDCourseNum(self.wlm_ch, self.coarse_scan_freq_THz[i])
			self.core.break_realtime()
			delay(0.5*s)
			self.core.wait_until_mu(now_mu())

			print(len(self.AO_scan_freq_MHz)*self.N_avgs)
			# self.redpitaya0.record_next_N_traces(len(self.AO_scan_freq_MHz)*self.N_avgs, self.N_samples, decimation=64, Naverages=self.N_avgs)
			self.core.break_realtime()
			delay(0.5*s)


			### AO Loop
			for shift in self.AO_scan_freq_MHz:
				# print("Tuning AOM shift (MHz): ", shift)
				# self.urukul0_ch1.set(self.u0_ch1_freq + shift*MHz/2, amplitude = self.u0_ch1_amp_V) # divided by 2 b/c double pass
				print(shift)

				for j in range(self.N_avgs + self.N_warm_up_shots):
					delay(40*ms)
					###signal (Ti and probe)
					with parallel:
						if j >= self.N_warm_up_shots:
							self.ttl0.pulse(100*us) #picoscope trigger, aquisition includes 8192 samples before trigger
						self.ttl1.pulse(100*us) #ablation trigger

					delay(40*ms) # dealy to acheive 25Hz rep rate
		
			##wait until entire sequence is complete (not sure why this isnt working)
			self.core.wait_until_mu(now_mu())
			# self.core.break_realtime()
			# delay(1*s)
			# print('Appending to buffer...')
			# self.redpitaya0.append_traces_to_buffer()
			# print('Saving buffers...')
			# self.redpitaya0.save_buffer_to_dataset(self, names=("mts_" + self.coarse_scan_freq_THz_strings[i] + "_THz",  "dopp_" + self.coarse_scan_freq_THz_strings[i] + "_THz"))
			# # print('Plotting traces...')
			# # self.redpitaya0.show_traces()
			# print('Clearing buffers...')
			# self.redpitaya0.clear_buffers()
			self.core.break_realtime()
		
		print('Done.')

	def groupedAvg(self, myArray, N=2):
		result = np.cumsum(myArray, 0)[N-1::N]/float(N)
		result[1:] = result[1:] - result[:-1]
		return result

	# def analyze(self):

	# 	self.set_dataset("coarse_scan_freq_THz", self.coarse_scan_freq_THz)
	# 	self.set_dataset("AO_scan_freq_MHz", self.AO_scan_freq_MHz)

	# 	N_traces = len(self.AO_scan_freq_MHz)

	# 	dopp_offset = 0.0#np.mean(np.array(self.get_dataset("dopp_dark")))

	# 	for i,freq_str in enumerate(self.coarse_scan_freq_THz_strings):

	# 		mts_buffer = np.array(self.get_dataset("mts_" + freq_str + "_THz"))
	# 		dopp_buffer = np.array(self.get_dataset("dopp_" + freq_str + "_THz"))

	# 		mts_maxs = np.zeros(len(mts_buffer))
	# 		mts_mins = np.zeros(len(mts_buffer))

	# 		dopp_maxs = np.zeros(len(dopp_buffer))
	# 		dopp_mins = np.zeros(len(dopp_buffer))

	# 		for j in range(len(mts_buffer)):
	# 			print("mts max at: ", np.where(mts_buffer[j]==np.max(mts_buffer[j]))[0])
	# 			#max_idx =  np.where(mts_buffer[j]==np.max(mts_buffer[j]))[0][0]

	# 			mts_maxs[j] = np.max(mts_buffer[j][1700-500:1700+500])
	# 			mts_mins[j] = np.mean(mts_buffer[j][self.N_samples-1000:-500]) # min is average of last 1000 samples in arraynp.min(mts_buffer[j])

	# 			dopp_maxs[j] = np.mean(dopp_buffer[j][self.N_samples-1000:-500]) #max is average of lase 1000 samples np.max(dopp_buffer[j])
	# 			dopp_mins[j] = np.min(dopp_buffer[j])

	# 		mts_peaks = mts_maxs - mts_mins
	# 		dopp_transmissions = (dopp_mins-dopp_offset) / (dopp_maxs-dopp_offset)

	# 		absolute_frequencies_THz = self.coarse_scan_freq_THz[i] + self.AO_scan_freq_MHz*1e-6

	# 		self.set_dataset("mts_peaks" + freq_str + "_THz", mts_peaks)
	# 		self.set_dataset("dopp_transmissions" + freq_str + "_THz", dopp_transmissions)
	# 		self.set_dataset("absolute_frequencies" + freq_str + "_THz", absolute_frequencies_THz)

	# 		plt.plot(absolute_frequencies_THz, mts_peaks)

	# 	plt.xlabel("Frequency (THz)")
	# 	plt.ylabel("MTS Sig (arb.)")
	# 	plt.show()


		# print('Plotting...')
		# in_phase = self.redpitaya0.cha_buffer 
		# quad = self.redpitaya0.chb_buffer

		# peaks = np.zeros(len(in_phase))
		# for i,trace in enumerate(in_phase):
		# 	peak = np.max(trace)
		# 	peaks[i] = peak

		# plt.plot(self.AO_scan_freq_MHz, peaks)
		# plt.show()
		# self.redpitaya0.show_traces()
		# self.redpitaya0.save_buffer_to_dataset(self)


