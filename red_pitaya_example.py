from artiq.experiment import*
import numpy

class RedPitayaExample(EnvExperiment):
	def build(self):
		self.setattr_device("core")
		self.setattr_device("ttl0") #rp trigger
		self.setattr_device("ttl7") #ablation trigger
		self.setattr_device("redpitaya0")
		self.setattr_argument("trigger_delay_ms", NumberValue(default=40, ndecimals=0, step=1))
		self.setattr_argument("N_triggers", NumberValue(default=10, ndecimals=0, step=1))
		self.setattr_argument("N_samples", NumberValue(default=15000, ndecimals=0, step=1))
		self.setattr_argument("decimation", NumberValue(default=64, ndecimals=0, step=1))
		self.setattr_argument("N_averages", NumberValue(default=1, ndecimals=0, step=1))
		self.a=1


	@kernel
	def run(self):
		self.core.reset()
		self.a
		self.ttl0.output()
		self.ttl7.output()
		self.redpitaya0.initialize()

		self.redpitaya0.record_next_N_traces(self.N_triggers, self.N_samples, decimation=self.decimation, Naverages=self.N_averages)

		self.core.break_realtime()
		delay_times = [100,150,200,250,300,350,400,450,500,550]
		delay(1*s)
		print("Taking images...")
		for i in range(self.N_triggers):

			self.ttl0.pulse(100*us)
			delay(delay_times[i]*us)
			self.ttl7.pulse(1*ms)
			delay(self.trigger_delay_ms*ms)
		self.core.wait_until_mu(now_mu())
		print("Appending to buffer...")
		self.redpitaya0.append_traces_to_buffer()
		print("Plotting...")
		self.redpitaya0.show_traces()
		self.redpitaya0.save_buffer_to_dataset(self)
		print("Done.")
