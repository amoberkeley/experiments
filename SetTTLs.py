from artiq.experiment import*

class SetTTLs(EnvExperiment):
    
    def build(self):
        self.setattr_device("core")
        
        # self.setattr_device("ttl0")
        # self.setattr_device("ttl1")
        self.setattr_device("ttl2")
        # self.setattr_device("ttl3")
        # self.setattr_device("ttl4")
        # self.setattr_device("ttl5")
        # self.setattr_device("ttl6")
        # self.setattr_device("ttl7")

        # self.setattr_argument("TTL0_normal_state", BooleanValue(default=False), group="TTL0")
        # self.setattr_argument("TTL0_chop", BooleanValue(default=False), group="TTL0")
        # self.setattr_argument("TTL0_chop_freq", NumberValue(default=1.0*Hz, step=1.0, ndecimals=4, unit="Hz"), group="TTL0")

        # self.setattr_argument("TTL1_normal_state", BooleanValue(default=False), group="TTL1")
        # self.setattr_argument("TTL1_chop", BooleanValue(default=False), group="TTL1")
        # self.setattr_argument("TTL1_chop_freq", NumberValue(default=1.0*Hz, step=1.0, ndecimals=4, unit="Hz"), group="TTL1")

        # self.setattr_argument("TTL2_normal_state", BooleanValue(default=False), group="TTL2")
        # self.setattr_argument("TTL2_chop", BooleanValue(default=False), group="TTL2")
        # self.setattr_argument("TTL2_chop_freq", NumberValue(default=1.0*Hz, step=1.0, ndecimals=4, unit="Hz"), group="TTL2")

        # self.setattr_argument("TTL3_normal_state", BooleanValue(default=False), group="TTL3")
        # self.setattr_argument("TTL3_chop", BooleanValue(default=False), group="TTL3")
        # self.setattr_argument("TTL3_chop_freq", NumberValue(default=1.0*Hz, step=1.0, ndecimals=4, unit="Hz"), group="TTL3")

        # self.setattr_argument("TTL4_normal_state", BooleanValue(default=False), group="TTL4")
        # self.setattr_argument("TTL4_chop", BooleanValue(default=False), group="TTL4")
        # self.setattr_argument("TTL4_chop_freq", NumberValue(default=1.0*Hz, step=1.0, ndecimals=4, unit="Hz"), group="TTL4")

        # self.setattr_argument("TTL5_normal_state", BooleanValue(default=False), group="TTL5")
        # self.setattr_argument("TTL5_chop", BooleanValue(default=False), group="TTL5")
        # self.setattr_argument("TTL5_chop_freq", NumberValue(default=1.0*Hz, step=1.0, ndecimals=4, unit="Hz"), group="TTL5")

        # self.setattr_argument("TTL6_normal_state", BooleanValue(default=False), group="TTL6")
        # self.setattr_argument("TTL6_chop", BooleanValue(default=False), group="TTL6")
        # self.setattr_argument("TTL6_chop_freq", NumberValue(default=1.0*Hz, step=1.0, ndecimals=4, unit="Hz"), group="TTL6")

        # self.setattr_argument("TTL7_normal_state", BooleanValue(default=False), group="TTL7")
        # self.setattr_argument("TTL7_chop", BooleanValue(default=False), group="TTL7")
        # self.setattr_argument("TTL7_chop_freq", NumberValue(default=1.0*Hz, step=1.0, ndecimals=4, unit="Hz"), group="TTL7")

        self.a=1
    
    @kernel
    def run(self):  
        
        self.core.reset()
        self.a
        delay(2*s)

        # #self.urukul0_ch0.cpld.init() #sometimes dont need this line, but sometimes get an underflow that this line solves? No dont think thats true actually
        # self.ttl0.output()
        #self.ttl1.output()
        self.ttl2.output()
        self.ttl2.off()
        # self.ttl3.output()
        # self.ttl4.output()
        # self.ttl5.output()
        # self.ttl6.output()
        # self.ttl7.output()
        # self.core.break_realtime()

        # # print(self.TTL0_normal_state)
        # # if self.TTL0_normal_state:
        # #     print('foo')
        # #     self.ttl0.on()
        # # else:
        # #     print("bar")
        # #     self.ttl0.off()
        # # self.core.break_realtime()

        # # if self.TTL1_normal_state:
        # #     self.ttl1.on()
        # # else:
        # #     self.ttl1.off()
        # # self.core.break_realtime()

        # if self.TTL2_normal_state:
        #     self.ttl2.on()
        # else:
        #     self.ttl2.off()
        # self.core.break_realtime()

        # # if self.TTL3_normal_state:
        # #     self.ttl3.on()
        # # else:
        # #     self.ttl3.off()
        # # self.core.break_realtime()

        # # if self.TTL4_normal_state:
        # #     self.ttl4.on()
        # # else:
        # #     self.ttl4.off()
        # # self.core.break_realtime()

        # # if self.TTL5_normal_state:
        # #     self.ttl5.on()
        # # else:
        # #     self.ttl5.off()
        # # self.core.break_realtime()

        # # if self.TTL6_normal_state:
        # #     self.ttl6.on()
        # # else:
        # #     self.ttl6.off()
        # # self.core.break_realtime()

        # # if self.TTL7_normal_state:
        # #     self.ttl7.on()
        # # else:
        # #     self.ttl7.off()
        # # self.core.break_realtime()
