from artiq.experiment import*

class UrukulSingleFreq(EnvExperiment):
    
    def build(self):

        self.setattr_device("core")
        
        self.setattr_device("urukul0_ch0")
        self.setattr_device("urukul0_ch1")
        self.setattr_device("urukul0_ch2")
        self.setattr_device("urukul0_ch3")

        self.setattr_device("ttl2")

        # self.setattr_device("urukul1_ch0")
        # self.setattr_device("urukul1_ch1")
        # self.setattr_device("urukul1_ch2")
        # self.setattr_device("urukul1_ch3")

        self.setattr_argument("u0_ch0_on_off", BooleanValue(default=False), group="Urukul0 Ch0")
        self.setattr_argument("u0_ch0_1Hz_chop", BooleanValue(default=False), group="Urukul0 Ch0")
        self.setattr_argument("u0_ch0_freq_MHz", NumberValue(default=80.0*MHz, step=1.0, ndecimals=4, unit="MHz"), group="Urukul0 Ch0")
        self.setattr_argument("u0_ch0_amp_V", NumberValue(default=0.1, step=0.1, max=1.0, ndecimals=3, unit="V"), group="Urukul0 Ch0")
        self.setattr_argument("u0_ch0_att_dB", NumberValue(default=0.0*dB, step=1.0, max=100.0, ndecimals=1, unit="dB"), group="Urukul0 Ch0")

        self.setattr_argument("u0_ch1_on_off", BooleanValue(default=False), group="Urukul0 Ch1")
        self.setattr_argument("u0_ch1_1Hz_chop", BooleanValue(default=False), group="Urukul0 Ch1")
        self.setattr_argument("u0_ch1_freq_MHz", NumberValue(default=200.0*MHz, step=1.0, ndecimals=4, unit="MHz"), group="Urukul0 Ch1")
        self.setattr_argument("u0_ch1_amp_V", NumberValue(default=0.005, step=0.1, max=1.0, ndecimals=3, unit="V"), group="Urukul0 Ch1")
        self.setattr_argument("u0_ch1_att_dB", NumberValue(default=0.0*dB, step=1.0, max=100.0, ndecimals=1, unit="dB"), group="Urukul0 Ch1")

        self.setattr_argument("u0_ch2_on_off", BooleanValue(default=False), group="Urukul0 Ch2")
        self.setattr_argument("u0_ch2_1Hz_chop", BooleanValue(default=False), group="Urukul0 Ch2")
        self.setattr_argument("u0_ch2_freq_MHz", NumberValue(default=200.0*MHz, step=1.0, ndecimals=4, unit="MHz"), group="Urukul0 Ch2")
        self.setattr_argument("u0_ch2_amp_V", NumberValue(default=0.18, step=0.1, max=1.0, ndecimals=3, unit="V"), group="Urukul0 Ch2")
        self.setattr_argument("u0_ch2_att_dB", NumberValue(default=0.0*dB, step=1.0, max=100.0, ndecimals=1, unit="dB"), group="Urukul0 Ch2")

        self.setattr_argument("u0_ch3_on_off", BooleanValue(default=False), group="Urukul0 Ch3")
        self.setattr_argument("u0_ch3_1Hz_chop", BooleanValue(default=False), group="Urukul0 Ch3")
        self.setattr_argument("u0_ch3_freq_MHz", NumberValue(default=80.0*MHz, step=1.0, ndecimals=4, unit="MHz"), group="Urukul0 Ch3")
        self.setattr_argument("u0_ch3_amp_V", NumberValue(default=0.12, step=0.1, max=1.0, ndecimals=3, unit="V"), group="Urukul0 Ch3")
        self.setattr_argument("u0_ch3_att_dB", NumberValue(default=0.0*dB, step=1.0, max=100.0, ndecimals=1, unit="dB"), group="Urukul0 Ch3")

        self.break_realtime_delay = 100 * ms

    
    @kernel
    def run(self):  
        
        self.core.reset()
        # delay(2*s)
        # self.ttl2.off()
        # delay(10 * ms)
        delay(100 * ms)

        self.urukul0_ch0.cpld.init() #sometimes dont need this line, but sometimes get an underflow that this line solves? No dont think thats true actually
        self.urukul0_ch0.init()
        delay(1 * ms)
        self.urukul0_ch1.init()
        delay(1 * ms)
        self.urukul0_ch2.init()
        delay(1 * ms)
        self.urukul0_ch3.init()
        delay(1 * ms)
        self.core.break_realtime()
        delay(self.break_realtime_delay)

        
        ### Setting freqs, amps and attenuation

        self.urukul0_ch0.set(self.u0_ch0_freq_MHz, amplitude = self.u0_ch0_amp_V)
        self.urukul0_ch0.set_att(self.u0_ch0_att_dB)
        self.core.break_realtime()
        delay(self.break_realtime_delay)

        self.urukul0_ch1.set(self.u0_ch1_freq_MHz, amplitude = self.u0_ch1_amp_V)
        self.urukul0_ch1.set_att(self.u0_ch1_att_dB)
        self.core.break_realtime()
        delay(self.break_realtime_delay)

        self.urukul0_ch2.set(self.u0_ch2_freq_MHz, amplitude = self.u0_ch2_amp_V)
        self.urukul0_ch2.set_att(self.u0_ch2_att_dB)
        self.core.break_realtime()
        delay(self.break_realtime_delay)

        self.urukul0_ch3.set(self.u0_ch3_freq_MHz, amplitude = self.u0_ch3_amp_V)
        self.urukul0_ch3.set_att(self.u0_ch3_att_dB)
        self.core.break_realtime()
        delay(self.break_realtime_delay)


        ### setting urukuls in specified final state

        ## ch0
        if self.u0_ch0_on_off:
            self.urukul0_ch0.sw.on()
        else:
            self.urukul0_ch0.sw.off()
        self.core.break_realtime()
        delay(self.break_realtime_delay)

        ## ch1
        if self.u0_ch1_on_off:
            self.urukul0_ch1.sw.on()
        else:
            self.urukul0_ch1.sw.off()
        self.core.break_realtime()
        delay(self.break_realtime_delay)

        ## ch2
        if self.u0_ch2_on_off:
            self.urukul0_ch2.sw.on()
        else:
            self.urukul0_ch2.sw.off()
        self.core.break_realtime()
        delay(self.break_realtime_delay)

        ## ch3
        if self.u0_ch3_on_off:
            self.urukul0_ch3.sw.on()
        else:
            self.urukul0_ch3.sw.off()
        self.core.break_realtime()
        delay(self.break_realtime_delay)

        
        ### Chop loop
        if self.u0_ch0_1Hz_chop or self.u0_ch1_1Hz_chop or self.u0_ch2_1Hz_chop or self.u0_ch3_1Hz_chop:
            print("Entering Chop loop...")
            for i in range(100):
                delay(0.5*s)
                with parallel:
                    if self.u0_ch0_1Hz_chop:
                        self.urukul0_ch0.sw.on()
                    if self.u0_ch1_1Hz_chop:
                        self.urukul0_ch1.sw.on()
                    if self.u0_ch2_1Hz_chop:
                        self.urukul0_ch2.sw.on()
                    if self.u0_ch3_1Hz_chop:
                        self.urukul0_ch3.sw.on()
                delay(0.5*s)
                with parallel:
                    if self.u0_ch0_1Hz_chop:
                        self.urukul0_ch0.sw.off()
                    if self.u0_ch1_1Hz_chop:
                        self.urukul0_ch1.sw.off()
                    if self.u0_ch2_1Hz_chop:
                        self.urukul0_ch2.sw.off()
                    if self.u0_ch3_1Hz_chop:
                        self.urukul0_ch3.sw.off()


        ### leaving urukuls in specified final state

        ## ch0
        if self.u0_ch0_on_off:
            self.urukul0_ch0.sw.on()
        else:
            self.urukul0_ch0.sw.off()
        self.core.break_realtime()
        delay(self.break_realtime_delay)

        ## ch1
        if self.u0_ch1_on_off:
            self.urukul0_ch1.sw.on()
        else:
            self.urukul0_ch1.sw.off()
        self.core.break_realtime()
        delay(self.break_realtime_delay)

        ## ch2
        if self.u0_ch2_on_off:
            self.urukul0_ch2.sw.on()
        else:
            self.urukul0_ch2.sw.off()
        self.core.break_realtime()
        delay(self.break_realtime_delay)

        ## ch3
        if self.u0_ch3_on_off:
            self.urukul0_ch3.sw.on()
        else:
            self.urukul0_ch3.sw.off()
        self.core.break_realtime()
        delay(self.break_realtime_delay)

