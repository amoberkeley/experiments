from artiq.experiment import *
import numpy as np

class AblationBeam(EnvExperiment):
    
    def build(self):
        self.setattr_device("core")
        self.setattr_device("ttl0") # camera trigger
        self.setattr_device("ttl1") # ablation laser
        self.setattr_device("urukul0_ch0")
        #self.setattr_device("thorcam0")
        self.setattr_device("wlm0")
        self.setattr_device("sampler0")
        self.setattr_argument("flow_rate_sccm", NumberValue(default=1, ndecimals=2, step=1))
        self.setattr_argument("pressure_mTorr", NumberValue(default=1, ndecimals=2, step=1))
        self.setattr_argument("wlm_channel", NumberValue(default=1, ndecimals=0, step=1))
        self.setattr_argument("Nshots", NumberValue(default=1, ndecimals=0, step=1))
        self.setattr_argument("wlm_channel", NumberValue(default=1, ndecimals=0, step=1))
        self.setattr_argument("probe_frequencies", Scannable(global_max=400, ndecimals=6, default=[RangeScan(300.807945 - 0.0002 - 0.001, 300.807945 - 0.0002 + 0.001, 20)]))
        self.a = 1

    def prepare(self):
        self.probe_frequencies = np.array([freq for freq in self.probe_frequencies])
        self.measured_frequencies_before = np.zeros(len(self.probe_frequencies))
        self.measured_frequencies_after = np.zeros(len(self.probe_frequencies))
        self.pd_cal = np.zeros(len(self.probe_frequencies))
        self.pd_cal_err = np.zeros(len(self.probe_frequencies))

    @kernel(flags={"fast-math"})
    def get_mean_and_std(self, arr):
        meanN = 0.0
        stdNS = 0.0
        N = len(arr)

        #mean
        for a in arr:
            meanN += a

        mean = meanN/N

        #std
        for a in arr:
            stdNS += (a-mean)**2

        std = np.sqrt(stdNS/N)

        return mean, std


    @kernel
    def run(self):

        #######################
        ### core initialization
        self.core.wait_until_mu(now_mu())
        self.core.reset() #Clear RTIO FIFOs, release RTIO PHY reset, and set the time cursor at the current value of the hardware RTIO counter plus a margin of 125000 machine units.
        self.a

        #######################
        ### ttl initialization
        self.ttl0.output()
        self.ttl1.output()

        #######################
        ### Urukul initialization
        self.core.break_realtime()
        delay(3*s)
        self.urukul0_ch0.cpld.init()
        self.urukul0_ch0.init()


        freq = 200*MHz
        amp = 0.4
        attenuation = 0.0*dB

        self.urukul0_ch0.set_att(attenuation)
        self.urukul0_ch0.set(freq, amplitude = amp)
        self.core.break_realtime()
        self.urukul0_ch0.sw.off()

        # #######################
        # ### Thorcam initialization
        # self.thorcam0.connect()
        # self.thorcam0.initialize(exposure_time_us=0.8e6, image_poll_timeout=2000, roi=[0,0,1440,1080], wavelength_nm=498, gain=350)
        # #self.thorcam0.arm(self.Nshots*4)

        #######################
        ### WLM initialization
        self.wlm0.SetPIDCourseNum(self.wlm_channel, self.probe_frequencies[0])
        ##rint(self.wlm0.GetRegulationStatus())
        ##self.wlm0.SetRegulationStatus(True)

        #######################
        ### Sampler initialization
        self.core.break_realtime()
        self.sampler0.init()
        self.sampler0.set_gain_mu(0,0)


        #######################
        #######################
        ### Main Loop

        ####################################
        ### loop through probe frequencies
        freq_count = 0
        for probe_frequency in self.probe_frequencies:

            ### arm thorcam for this many images. 4 is for dark, bright probe, bright abl, and sig
            #self.thorcam0.arm(self.Nshots*4)
            
            ### Set probe laser frequency
            print("Tuning probe laser...")
            self.wlm0.SetPIDCourseNum(self.wlm_channel, probe_frequency)
            
            ### wait for laser control to settle before measuring laser freq
            self.core.break_realtime()
            delay(1*s)
            self.core.wait_until_mu(now_mu())

            ### record measured laser freq
            self.measured_frequencies_before[freq_count] = self.wlm0.GetFrequency(self.wlm_channel)
            self.core.break_realtime()

            ### taking a pd cal measurement
            n_samples = 50
            smp_arr = [0.0]*n_samples

            delay(1*ms)
            self.urukul0_ch0.sw.on()
            for j in range(n_samples):
                smp = [0.0]*8
                delay(2*ms)
                self.sampler0.sample(smp)
                smp_arr[j] = smp[0]
            delay(1*ms)
            self.urukul0_ch0.sw.off()

            self.pd_cal[freq_count], self.pd_cal_err[freq_count] = self.get_mean_and_std(smp_arr)
            

            ### fresh time cursor for next block
            self.core.break_realtime()
            delay(1*s)


            ####################################
            ### loop through Nshots images to take per detuning
            for i in range(self.Nshots):
                print("Image: ", i)

                # print(self.core.mu_to_seconds(now_mu()), self.core.mu_to_seconds(self.core.get_rtio_counter_mu()))
                
                ### Dark Field Image
                self.ttl0.pulse(200*us) # trigger camera
                delay(0.9*s)
                

                ### Probe Brightfield Image
                self.ttl0.pulse(200*us) # trigger camera
                for j in range(20):
                    with parallel:
                        with sequential:
                            self.urukul0_ch0.sw.on()
                            delay(1*ms)
                            self.urukul0_ch0.sw.off()
                        delay(40*ms)
                delay(0.1*s)


                ### Ablation Brightfield Image
                self.ttl0.pulse(200*us) # trigger camera
                for j in range(20):
                    with parallel:
                        self.ttl1.pulse(100*us)
                        delay(40*ms)
                delay(0.1*s)

               
                ### Signal Image
                self.ttl0.pulse(200*us) # trigger camera
                for j in range(20):
                    with parallel:
                        with sequential:
                            self.ttl1.pulse(100*us) ## Trigger ablation
                            delay(40*us) ## Delay to let Q-switch delay elapse
                            self.urukul0_ch0.sw.on()
                            delay(1*ms)
                            self.urukul0_ch0.sw.off()
                        delay(40*ms)
                delay(0.1*s)


            ### wait until all the triggers have happened
            self.core.break_realtime()
            delay(1*s)
            self.core.wait_until_mu(now_mu())
            print("Done triggering.")

            ### record measured laser freq
            self.measured_frequencies_after[freq_count] = self.wlm0.GetFrequency(self.wlm_channel)

            ### Polling camera for images and adding them to local buffer
            print("Polling camera for images...")
            # self.thorcam0.poll_for_frame_and_append_to_buffer(['dark', 'probe', 'ablation', 'signal'], self.Nshots*4)
            # self.thorcam0.disarm() 
            # self.thorcam0.disarm() 
            freq_count += 1


    def analyze(self):
        print("Saving...")
        #self.thorcam0.save_all(self)
        self.set_dataset("measured_frequencies_before", self.measured_frequencies_before)
        self.set_dataset("measured_frequencies_after", self.measured_frequencies_after)
        self.set_dataset("pd_cal", self.pd_cal)
        self.set_dataset("pd_cal_errs", self.pd_cal_err)
        #self.thorcam0.show_img_buffer('signal')
        print("Done.")



