from artiq.experiment import*
from scipy import signal
import numpy as np

class zotino_MOT_coils(EnvExperiment):
	def build(self):
		self.setattr_device("core")
		self.setattr_device("zotino0") 
		self.setattr_device('ttl2')#coil switch - low = coil on, high = coil off
		self.setattr_device('ttl4')
		self.setattr_device("urukul0_ch0")
		self.setattr_device("urukul0_ch2")
		self.setattr_argument("MOT_coil_current", NumberValue(default=4, ndecimals=2,unit="A", max=4.5, min=0.0), tooltip="Range between 0 and 4.5 A")
		self.setattr_argument("x_bias_current", NumberValue(default=0, ndecimals=2,unit="A", max=5.0, min=-5.0), tooltip="Range between -5 and 5 A")
		self.setattr_argument("y_bias_current", NumberValue(default=0, ndecimals=2,unit="A", max=5.0, min=-5.0), tooltip="Range between -5 and 5 A")
		self.camera_jitter_delay = 20 * us
		self.imaging_pulse_length = 100 * us
		self.a =1

	def prepare(self):

		self.detunings = np.linspace(0,25,26)

		#set test currents
		self.MOT_Voltage=self.MOT_coil_current/10.

		print('============================')
		print("Chosen MOT Kepco programming voltage: %.2f V"%self.MOT_Voltage)

		if self.MOT_Voltage<0 or self.MOT_Voltage>0.45:
			raise Exception("Error:Voltage (%.2f V) out of allowed range (0 to 0.45 V)"%self.MOT_Voltage)

		self.x_bias_Voltage=self.x_bias_current * 2.

		print("Chosen x bias programming voltage: %.2f V"%self.x_bias_Voltage)

		if self.x_bias_Voltage<-10 or self.x_bias_Voltage>10:
			raise Exception("Error:Voltage (%.2f V) out of allowed range (-10 to 10 V)"%self.x_bias_Voltage)

		self.y_bias_Voltage=self.y_bias_current * 2.

		print("Chosen y bias programming voltage: %.2f V"%self.y_bias_Voltage)

		if self.y_bias_Voltage<-10 or self.y_bias_Voltage>10:
			raise Exception("Error:Voltage (%.2f V) out of allowed range (-10 to 10 V)"%self.y_bias_Voltage)


	@kernel
	def run(self):
		
		self.core.reset()
		self.core.break_realtime()
		self.zotino0.init()
		self.a
		delay(1*s)
		self.ttl2.output()
		self.ttl4.output()
		delay(1*s)
		self.zotino0.set_dac([self.MOT_Voltage], [0])
		self.zotino0.set_dac([self.x_bias_Voltage], [1])
		self.zotino0.set_dac([self.y_bias_Voltage], [2])
		delay(1*s)
		# self.ttl2.on()
		# delay(1*s)
		# for i in range(360):
		# 	self.ttl2.pulse(1.0*s)
		# 	delay(1.0*s)


		# for j in range(30):
		# 	for i in range(2000):
		# 		delay(1*ms)
		# 		self.zotino0.set_dac([self.MOT_Voltage * i / 2000], [0])
		# 	for i in range(2000):
		# 		delay(1*ms)
		# 		self.zotino0.set_dac([self.MOT_Voltage * (2000 - i) / 2000], [0])
		# 		# delay(1*s)
		# 		# self.zotino0.set_dac([0.0], [0])
		# self.zotino0.set_dac([0.0], [1])
		# self.zotino0.set_dac([0.0], [2])

		# delay(2*s)
		
		# self.urukul0_ch0.init()
		# self.urukul0_ch2.init()

		# self.urukul0_ch0.set(200*MHz, amplitude = 1.0)
		# self.urukul0_ch0.set_att(7*dB)
		# self.urukul0_ch2.set(200*MHz, amplitude = 1.0)
		# self.urukul0_ch2.set_att(4*dB)

		# self.core.break_realtime()
		# delay(1*s)

		# self.urukul0_ch0.sw.off()
		# self.urukul0_ch2.sw.on()

		# delay(100*ms)

		# self.zotino0.set_dac([0.0], [0]) # turn the field on, load the MOT

		# self.core.break_realtime()
		# delay(100*ms)

		# for detuning in self.detunings:

		# 	print("detuning: ", detuning)

		# 	self.core.break_realtime()
		# 	delay(2*s)

		# 	self.urukul0_ch0.set((200+detuning)*MHz, amplitude = 1.0)
		# 	self.urukul0_ch2.set((200-detuning)*MHz, amplitude = 1.0)

		# 	### Trigger here
		# 	self.ttl4.pulse(100*us) # abs probe off background
		# 	delay(100*ms)

		# 	### Trigger here
		# 	self.ttl4.pulse(100*us) # abs probe on background
		# 	delay(self.camera_jitter_delay)
		# 	self.urukul0_ch0.sw.on() # turn abs probe on
		# 	delay(self.imaging_pulse_length) # probe light pulse width
		# 	self.urukul0_ch0.sw.off() # turn off abs while loading
		# 	delay(10*us)

		# 	self.zotino0.set_dac([self.Voltage], [0]) # turn the field on, load the MOT
		# 	delay(1.5*s)
			
		# 	### Trigger here
		# 	self.ttl4.pulse(100*us) # take signal image
		# 	delay(self.camera_jitter_delay)
		# 	self.urukul0_ch0.sw.on() # turn on abs for image
		# 	delay(self.imaging_pulse_length) # probe light pulse width
		# 	self.urukul0_ch0.sw.off()

		# 	self.zotino0.set_dac([0.0], [0]) ## turn coils off

		# self.core.wait_until_mu(now_mu())

		print("Done")
