from artiq.experiment import *
import numpy as np

class AblationBeam_SignalSearch(EnvExperiment):
    
    def build(self):
        self.setattr_device("core")
        self.setattr_device("ttl0") # camera trigger
        self.setattr_device("ttl1") # ablation laser
        self.setattr_device("urukul0_ch0")
        self.setattr_argument("Nshots", NumberValue(default=5, ndecimals=0, step=1))
        self.a = 1


    @kernel
    def run(self):

        #######################
        ### core initialization
        self.core.wait_until_mu(now_mu())
        self.core.reset() #Clear RTIO FIFOs, release RTIO PHY reset, and set the time cursor at the current value of the hardware RTIO counter plus a margin of 125000 machine units.
        self.a

        #######################
        ### ttl initialization
        self.ttl0.output()
        self.ttl1.output()

        #######################
        ### Urukul initialization
        delay(0.5*s)
        self.urukul0_ch0.cpld.init()
        self.urukul0_ch0.init()

        freq = 200*MHz
        amp = 0.4
        attenuation = 0.0*dB

        self.urukul0_ch0.set_att(attenuation)
        self.urukul0_ch0.set(freq, amplitude = amp)
        self.core.break_realtime()
        self.urukul0_ch0.sw.off()


        #######################
        #######################
        ### Main Loop
           
        ### Signal Image
        for j in range(self.Nshots):
            if j%20==0:
                self.ttl0.pulse(1*ms)
            with parallel:
                with sequential:
                    self.ttl1.pulse(100*us) ## Trigger ablation
                    self.urukul0_ch0.sw.on()
                    delay(1*ms)
                    self.urukul0_ch0.sw.off()
                delay(40*ms)




