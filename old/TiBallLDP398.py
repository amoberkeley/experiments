from artiq.experiment import *
from artiq.language.environment import NumberValue
from artiq.language.scan import RangeScan
import numpy as np

### TiBallTDP398 class
class TiBallLDP398(EnvExperiment):
    def build(self):
        self.setattr_device("core")
        self.setattr_device("ttl6")
        #self.setattr_device("ttl5")
        self.setattr_device("thorcam1")
        self.setattr_device("wlm0")
        self.setattr_device("sampler0")
        self.setattr_argument("Notes", StringValue())
        self.setattr_argument("n_datapoints", NumberValue(default=10, ndecimals=0, step=1))
        self.setattr_argument("n_pd_samples_per_datapoint", NumberValue(default=10, ndecimals=0, step=1))
        #self.setattr_argument("sample_time_us", NumberValue(ndecimals=0, step=1))

        self.a = 1 #needed if running artiq 7.8123

    @kernel
    def get_mean(self, data_arr):
        avg = 0.0
        for d in data_arr:
            avg += d
        return avg/len(data_arr)

    @kernel
    def get_stddev_of_mean(self, data_arr):
        mu = self.get_mean(data_arr)
        chi = 0.0
        for d in data_arr:
            chi += (d-mu)**2
        return np.sqrt(chi)/len(data_arr)

    @kernel
    def run(self):

        self.a #needed if running artiq 7.8123
        
        ### Core initialization
        self.core.reset()

        ### TTL initialization
        self.ttl6.output()
        #self.ttl5.output()

        ### Thorcam initialization
        #self.thorcam1.set_roi(650,230,725,1000)
        self.thorcam1.set_roi(602,380,802,980)
        delay(1*s)
        exposure_time_us = self.thorcam1.get_exposure_time_us()
        gain = self.thorcam1.get_gain()
        self.thorcam1.arm()
        
        ### Sampler initialization
        self.sampler0.init()
        self.sampler0.set_gain_mu(0,0)
        self.sampler0.set_gain_mu(2,0)
        pd_sample_time = exposure_time_us/self.n_pd_samples_per_datapoint


        ## Creating datasets
        #delay(1*s)
        self.set_dataset("Notes", self.Notes)
        self.set_dataset("n_datapoints", self.n_datapoints)
        self.set_dataset("n_pd_samples_per_datapoint", self.n_pd_samples_per_datapoint)

        self.set_dataset("exposure_time_us", exposure_time_us)
        self.set_dataset("gain", gain)

        self.set_dataset("pd0", np.full(self.n_datapoints, np.nan), broadcast=True)
        self.set_dataset("pd2", np.full(self.n_datapoints, np.nan), broadcast=True)
        self.set_dataset("pd0_err", np.full(self.n_datapoints, np.nan), broadcast=True)
        self.set_dataset("pd2_err", np.full(self.n_datapoints, np.nan), broadcast=True)
        #self.set_dataset("roi_counts", np.full(self.n_datapoints, np.nan), broadcast=True)
        self.set_dataset("roi_images", [np.full((604, 204), np.nan)], broadcast=True)
        self.set_dataset("398nm_freqs_before", np.full(self.n_datapoints, np.nan), broadcast=True)
        self.set_dataset("398nm_freqs_after", np.full(self.n_datapoints, np.nan), broadcast=True)

        
        ### Looping through points
        for i in range(self.n_datapoints):
            
            delay(1.41*s)
            smp_arr_pd0 = [0.0]*self.n_pd_samples_per_datapoint
            smp_arr_pd2 = [0.0]*self.n_pd_samples_per_datapoint

            with parallel:
                freq_before_398nm = self.wlm0.GetFrequency(2) ### Get frequency of 398nm
                self.ttl6.pulse(1*ms) ## Trigger camera

                ## While exposing, take pd samples
                for j in range(self.n_pd_samples_per_datapoint): ## loop through number of user specified pd samples
                    smp = [0.0]*8 ## initialize array to hole samples. One index for each channel
                    with parallel: ## sample and delay by exposure_time_us/n_pd_samples_per_datapoint
                        delay(pd_sample_time*us)
                        #self.ttl5.pulse(0.5*ms)
                        self.sampler0.sample(smp)
                    smp_arr_pd0[j] = smp[0]
                    smp_arr_pd2[j] = smp[2]
            delay(1*ms)
            #roi_count = self.thorcam1.capture_and_sum_roi()
            roi_img = self.thorcam1.capture()
            freq_after_398nm = self.wlm0.GetFrequency(2) ### Get frequency of 398nm

            self.mutate_dataset("pd0", i, self.get_mean(smp_arr_pd0))
            self.mutate_dataset("pd2", i, self.get_mean(smp_arr_pd2))
            self.mutate_dataset("pd0_err", i, self.get_stddev_of_mean(smp_arr_pd0))
            self.mutate_dataset("pd2_err", i, self.get_stddev_of_mean(smp_arr_pd2))
            #self.mutate_dataset("roi_counts", i, roi_count)
            self.append_to_dataset('roi_images', np.array(roi_img, dtype=np.int32))
            self.mutate_dataset("398nm_freqs_before", i, freq_before_398nm)
            self.mutate_dataset("398nm_freqs_after", i, freq_after_398nm)

