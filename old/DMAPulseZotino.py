from artiq.experiment import*
import numpy as np
import sys
sys.path.append("..")

class DMAPulseZotino(EnvExperiment):

    def build(self):
        self.setattr_device("core")
        self.setattr_device("core_dma")
        self.setattr_device("zotino0")
        #self.setattr_argument("time", NumberValue(default=100))

    @kernel
    def record(self):
        """
        Record the sequene set.

        Returns
        -------
        None.

        """
        #time = self.time
        # volt0 = self.zotino0.voltage_to_mu(0)
        # volt1 = self.zotino0.voltage_to_mu(3)

        # time = np.arange(0,500,1)
        self.samples = 10 #samples per period
        self.freq = 50*kHz
        self.period = 1.0/self.freq
        self.amp = 1.0
        self.offset = 0.0
        times = np.linspace(0,1,samples)
        self.signal_volts = self.amp*np.sin(2*np.pi*times) + self.offset
        self.interval = self.period/self.samples


        with self.core_dma.record("sine"):
            counter = 0
            for i in range(samples):
                self.zotino0.set_dac([self.signal_voltages[i]], [0])
                delay(self.interval)
            # for i in range(500):

            #     signal_mu = self.zotino.voltage_to_mu(signal_volts[i])
            #     self.zotino0.set_dac_mu([volt1], [0])
            #     delay(interval)  # minimum for signal is 1.5*us
            #     # time = float(i)
            #     # signal_volts = amp*np.sin(2*np.pi*freq*time) + offset
            #     # signal = self.zotino0.voltage_to_mu(signal_volts)
            #     # self.zotino0.set_dac_mu([signal],[0])
            #     # self.interval = self.period/self.sample
            #     # #delay(time *us)

    @kernel
    def run(self):
        """
        Reset the core device, load the pre-defined sequence set and play this
        set.

        Returns
        -------
        None.

        """
        self.core.reset()
        self.zotino0.init()
        delay(10*us)
        self.record()
        
        pulse_handle = self.core_dma.get_handle("sine")
        self.core.break_realtime()
        
        while True:
            self.core_dma.playback_handle(pulse_handle)