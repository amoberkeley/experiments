from artiq.experiment import *
import numpy as np
import sys
sys.path.append("..")

class FlashAOM(EnvExperiment):
    
    def build(self):
        self.setattr_device("core")
        self.setattr_device("urukul0_ch0")
        self.setattr_device("urukul0_ch1")
        self.a=1

    @kernel
    def run(self):
        self.core.wait_until_mu(now_mu())
        self.core.reset()
        self.core.break_realtime()
        self.a

        ### Urukul Ch0 initialization
        self.urukul0_ch0.cpld.init()
        self.urukul0_ch0.init()
        delay(10 * ms)
        ch0_freq = 350*MHz
        ch0_amp = 0.1
        ch0_attenuation = 0.0

        ### Urukul Ch1 initialization
        #self.urukul0_ch1.cpld.init()
        self.urukul0_ch1.init()
        delay(10 * ms)
        ch1_freq = 200*MHz
        ch1_amp = 0.1
        ch1_attenuation = 0.0

        self.urukul0_ch0.set_att(ch0_attenuation)
        self.urukul0_ch0.set(ch0_freq, amplitude = ch0_amp)
        self.urukul0_ch0.sw.on()

        self.core.break_realtime()
        
        self.urukul0_ch1.set_att(ch1_attenuation)
        self.urukul0_ch1.set(ch1_freq, amplitude = ch1_amp)
        self.urukul0_ch1.sw.on()
        # for i in range(1000):
        #     self.urukul0_ch1.sw.off()
        #     delay(0.5*s)
        #     self.urukul0_ch1.sw.on()