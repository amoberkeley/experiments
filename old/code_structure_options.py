from artiq.experiment import *
import sys
sys.path.append("..")

class FullExperiment(EnvExperiment):

	def build(self):

		self.setattr_device("core")

		''' 
		Option 1 
		This is what I see people doing. SubSystem1 object is never instantiated, methods are just called from class namespace and self
		is passed to each method so that the method actually operates on the FullExperiment instantiation.
		'''
		#SubSystem1.build(self)
		self.sub_system1 = SubSystem1(self)


		'''
		Option 2
		This is what I would guess was the intention. A SubSystem2 object is instantiated as sub_system2. sub_system2 is registered 
		as a child of self (FullExperiment) and its build method is called in the __init__ function of SubSytem2.
		'''
		self.sub_system2 = SubSystem2(self)

	@kernel
	def run(self):

		self.core.reset()

		self.sub_system1.initialize()
		self.sub_system2.initialize()

		## Option 1
		self.sub_system1.do_something()

		## Option 2
		self.sub_system2.do_something()






class SubSystem1(HasEnvironment):

	def build(self):
		self.setattr_device("core")
		self.setattr_device("ttl0")
		self.setattr_device("zotino0")
		self.setattr_device("urukul0_ch0")
		self.setattr_argument("Nshots", NumberValue(default=1, ndecimals=0, step=1))

	@kernel
	def initialize(self):
		self.core.reset()
		self.ttl0.output()

		self.urukul0_ch0.cpld.init()
		self.urukul0_ch0.init()

		freq = 200*MHz
		amp = 1.0
		attenuation = 10*dB

		self.urukul0_ch0.set_att(attenuation)
		self.urukul0_ch0.set(freq, amplitude = amp)
		self.core.break_realtime()
		self.urukul0_ch0.sw.on()
		print("done")

	@kernel
	def do_something(self):
		print("sub1")



class SubSystem2(HasEnvironment):

	def build(self):
		self.setattr_device("core")
		self.setattr_device("ttl1")
		self.setattr_device("zotino0")
		self.setattr_device("urukul0_ch0")
		self.setattr_argument("Nshots", NumberValue(default=2, ndecimals=0, step=1))

	@kernel
	def initialize(self):
		self.core.reset()
		self.ttl1.output()
		self.urukul0_ch0.cpld.init()
		self.urukul0_ch0.init()

	@kernel
	def do_something(self):
		print("sub2")
		## do some stuff with hardware
