from artiq.experiment import *
from scipy import signal
import numpy as np

class MOT3D(EnvExperiment):

	def build(self):
		self.setattr_device("core")
		self.setattr_device("zotino0")

	def prepare(self):
		self.period = 1.0*s
		self.sample = 128
		t = np.linspace(0, 1, self.sample)
		self.voltages = 8*signal.sawtooth(2*np.pi*t, 0.5)
		self.interval = self.period/self.sample



	@kernel
	def run(self):
		self.core.reset()
		self.core.break_realtime()
		self.zotino0.init()                     #initialises zotino0
		delay(1*ms)                           #200us delay, needed to prevent underflow on initialisation
		
		delay(1*ms)
		self.zotino0.set_dac(0.0,[0.0])
		# counter = 0
		# while True:
		# 	self.zotino0.set_dac([self.voltages[counter]], [0])
		# 	counter = (counter + 1) % self.sample
		# 	delay(self.interval)