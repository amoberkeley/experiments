from artiq.experiment import *
from artiq.coredevice.ad9910 import *

class UrukulFrequencyModulation(EnvExperiment):

    def build(self):

        self.setattr_device("core")
        self.setattr_device("urukul1_ch0")

    @kernel
    def run(self):  
        self.core.reset()
        self.urukul2_ch0.cpld.init()
        self.urukul2_ch0.init()

        delay(10 * ms)
        freq = 1*MHz
        amp = 1.0
        attenuation= 1.0

        #self.urukul2_ch1.set_att(attenuation)
        self.urukul2_ch0.sw.on()

        self.urukul2_ch0.set(freq, amplitude = amp)
        delay(10*s)
        self.urukul2_ch0.sw.off()

    @kernel
    def run(self):  
        self.core.reset()
        self.urukul1_ch0.cpld.init()
        self.urukul1_ch0.init()


        n = 10
        data = [0]*(1 << n)
        for i in range(len(data)//2):
            data[i] = i << (32 - (n - 1))
            data[i + len(data)//2] = 0xffff << 16

        delay(10 * ms)
        freq = 1*MHz
        amp = 1.0
        attenuation= 1.0

        self.urukul1_ch0.set_att(attenuation)
        self.urukul1_ch0.sw.on()


        self.urukul1_ch0.set(freq, amplitude = amp)
        delay(50*s)
        self.urukul1_ch0.sw.off()
