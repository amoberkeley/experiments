from artiq.experiment import *
from artiq.coredevice import ad9910
import numpy as np
#there are 1024 values for each profile, and the control parameters are the CF, MF, and MD
#The standard value has been set as N = 1024
#first write a simple script and then looking to optimise it.
import sys
sys.path.append("..")

N  = 25

#f1 = 99.*MHz
#f2 = 100.*MHz


#CF, MD, and MF are all in MHz
CF = 200
MF = 0.2
MD = 0.5

#Converting to time period in seconds. 
time_period = (1/(MF*MHz))*(1e9)
print(time_period)
time_step= (time_period/N)
print(time_step)
t_step = int(round(time_step/4))
print(t_step, "t_step")
T = int(1e2)  
 
class DDS_freq_ramp(EnvExperiment):
 
    def build(self):
        self.setattr_device("core")
        self.setattr_device("urukul0_cpld") #2nd Urukul module
        self.setattr_device("urukul0_ch0") #Urukul module
        self.setattr_device("urukul0_ch1") #Urukul module
        self.u = self.urukul0_ch0
        self.u2 = self.urukul0_ch1
 
    def prepare(self):
        
        #create list of frequencies in FTW format to write to RAM

        self.f = [0.]*N
        #self.f2 = [0.]*N
        self.f_ram = [0]*N
        #self.f2_ram = [0.]*N
        time = np.arange(0,N,1)
        amplitude = np.sin(2*np.pi*time/(N-1))
        amplitude2 = amplitude*MD
        amplitude3 = amplitude2+CF
        amplitude4 = amplitude3*MHz
        #f_span = f2 - f1
        #f_step = f_span / N
        #print(f_step)
        print(amplitude4)
        #I want to see the first step
        #print(f1+f_step)
        for i in range(N):
            self.f[i] = amplitude4[i]
            #self.f2[i] = CF*MHz
        self.u.frequency_to_ram(self.f,self.f_ram)
        #self.u2.frequency_to_ram(self.f2,self.f2_ram)
 
    @kernel
    def run(self):
        #self.core.reset()
        self.core.break_realtime()
        '''initialize DDS channel'''
        self.u.cpld.init()
        self.u.init()
        self.u.cpld.io_update.pulse(100*ns)
        #self.core.break_realtime()
        self.u.set_amplitude(1.)
        self.u.set_att(0.*dB)
        self.u.set(200.*MHz)

        self.u2.cpld.init()
        self.u2.init()
        #self.u2.cpld.io_update.pulse(100*ns)
        #self.core.break_realtime()
        self.u2.set_amplitude(1.)
        self.u2.set_att(0.*dB)
        self.u2.sw.on()
        self.u2.set(200.*MHz)
 
        '''prepare RAM profile:'''
        self.u.set_cfr1(ram_enable=0) #disable RAM for writing data
        self.u.cpld.io_update.pulse(100*ns) #I/O pulse to enact RAM change
        self.u.set_profile_ram(start=0, end=N-1, step=t_step, profile=0, mode=ad9910.RAM_MODE_CONT_RAMPUP)
        self.u.cpld.set_profile(0)
        delay(10 * us)
        self.u.cpld.io_update.pulse(100*ns)
    
 
        '''write data to RAM:'''
        delay(10*us)
        self.u.write_ram(self.f_ram)
        delay(10*us)
 
        '''enable RAM mode (enacted by IO pulse) and fix other parameters:'''
        self.u.set_cfr1(internal_profile=0, ram_enable = 1, ram_destination=ad9910.RAM_DEST_FTW, manual_osk_external=0, osk_enable=1, select_auto_osk=0)
        #with parallel:
        self.u.sw.on()
        #self.u2.sw.on()
        delay(10 *us)
        self.u.cpld.io_update.pulse_mu(8)
        delay(1 * ms)
        self.u.set_cfr1(ram_enable =0)