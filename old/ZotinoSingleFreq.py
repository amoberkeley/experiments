from artiq.experiment import*
from scipy import signal
import numpy

class ZotinoSingleFreq(EnvExperiment):
	def build(self):
		self.setattr_device("core")
		self.setattr_device("zotino0")
	
	def prepare(self):
		self.period = 0.06*s
		self.sample = 128
		t = numpy.linspace(0, 1, self.sample)
		self.voltages = 8*signal.sawtooth(2*numpy.pi*t, 0.5)
		self.interval = self.period/self.sample
	
	@kernel
	def run(self):
		self.core.reset()
		self.core.break_realtime()
		self.zotino0.init()
		print(self.period)
		delay(1*ms)
		counter = 0
		while True:
			self.zotino0.set_dac([self.voltages[counter]], [0])
			counter = (counter + 1) % self.sample
			delay(self.interval)