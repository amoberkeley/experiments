
from artiq.experiment import*                                   #imports everything from the artiq experiment library

#This code outputs a predefined frequency at a fixed amplitude on a single channel of the urukul
#The ouput persists for 2 seconds and the turns off

class Urukul_Frequency_Pulse(EnvExperiment):
    """Urukul Single Frequency Pulse"""
    def build(self): #This code runs on the host device

        self.setattr_device("core")                             #sets core device drivers as attributes
        self.setattr_device("mirny0_ch0")                      #sets urukul0, channel 1 device drivers as attributes
        
    
    @kernel #This code runs on the FPGA
    def run(self):  
        self.core.reset()                                       #resets core device
        #self.mirny0_ch0.Mirny.init()                            #initialises CPLD on channel 1
        self.mirny0_ch0.init()                                 #initialises channel 1
        delay(10 * ms)                                          #10ms delay
        
        freq = 100*MHz                                          #defines frequency variable
        amp = 1.0                                               #defines amplitude variable as an amplitude scale factor(0 to 1)
        attenuation= 1.0                                        #defines attenuation variable

        
        self.mirny0_ch0.set_att(attenuation)                   #writes attenuation to urukul channel
        self.mirny0_ch0.sw.on()                                #switches urukul channel on
           
            
        self.mirny0_ch0.set(freq, amplitude = amp)             #writes frequency and amplitude variables to urukul channel thus outputting function
        delay(2*s)                                              #2s delay
        self.mirny0_ch0.sw.off()     
