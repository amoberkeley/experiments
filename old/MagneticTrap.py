from artiq.experiment import *
import numpy as np
import sys
sys.path.append("..")

class MagneticTrap(EnvExperiment):
    
    def build(self):
        self.setattr_device("core")
        self.setattr_device("ttl4") # optical pumping shutter
        self.setattr_device("ttl5") # thorcam trigger
        self.setattr_device("ttl6") # mot shutter
        self.setattr_device("wlm0_old") #old driver
        self.setattr_device("thorcam3")
        self.setattr_argument("mot_coil_current_A", NumberValue(ndecimals=2))
        # self.setattr_argument("cooling_freq_THz", NumberValue(ndecimals=7))
        self.a = 1

    @rpc(flags={"async"})
    def save_imgs(self):
        sig_imgs, sig_img_errs = self.thorcam3.get_flattened_buffer('sig')
        dark_imgs, dark_img_errs = self.thorcam3.get_flattened_buffer('dark')
        self.set_dataset('sig_imgs', sig_imgs)
        self.set_dataset('sig_img_errs', sig_img_errs)
        self.set_dataset('dark_imgs', dark_imgs)
        self.set_dataset('dark_img_errs', dark_img_errs)

    @kernel
    def run(self):

        frames_per_trigger = 5

        self.core.wait_until_mu(now_mu())
        self.core.reset()
        self.core.break_realtime()
        self.a

        ### ttl initialization
        self.ttl4.output()
        self.ttl5.output()
        self.ttl6.output()
        self.ttl4.off()
        self.ttl5.off()
        self.ttl6.on()

        ### wlm initialization
        self.wlm0_old.connect()

        ### Thorcam initialization
        self.thorcam3.connect()
        self.thorcam3.initialize(gain=480, exposure_time_us=1000, frames_per_trigger_zero_for_unlimited=1)
        delay(1*s)
        self.thorcam3.set_roi(500, 600, 656, 756)
        delay(0.5*s)
        self.thorcam3.arm()
        delay(2*s) # dont change this very stupid number

        self.core.break_realtime()

        ### take darkfield bg

        # Turn on MOT w/out OP
        self.ttl6.off()

        delay(0.5*s)
        print("taking bg")
        self.ttl5.pulse(50*ms)
        delay(1*s)
        self.thorcam3.poll_for_frame_and_append_to_buffer('dark', Nframes=frames_per_trigger)

        # Turn MOT light back off
        self.ttl6.on()
        self.core.break_realtime()
        

        #######################
        # # wlm GetFrequency call. fyi can be slow since its an rpc
        channel = 1
        cooling_freq_THz = self.wlm0_old.GetFrequency(channel)


        ## Save metadata
        self.set_dataset("cooling_freq_THz", cooling_freq_THz)
        self.set_dataset("mot_coil_current_A", self.mot_coil_current_A)
       
        ## Run mott n times
        delays = range(25,301,25)
        for d in delays:
            
            print(d)
            self.core.break_realtime()
            
            ### OP, mot light on            
            self.ttl4.on()
            self.ttl6.off()
            
            ### Load mot for 0.5s
            delay(0.5*s)

            ### Turn off OP, then mot beams, load magnetic trap
            self.ttl4.off()
            delay(5*ms)
            self.ttl6.on()

            ### variable delay
            delay(d*ms)

            ### turn MOT beams back on
            self.ttl6.off()
            delay(20*ms)

            ### trigger camera
            for i in range(frames_per_trigger):
                self.ttl5.pulse(2*ms)
                delay(1*ms)

            delay(20*ms)
            self.thorcam3.poll_for_frame_and_append_to_buffer('sig', Nframes=frames_per_trigger)


        print("Reading out...")
        self.save_imgs()
        self.core.break_realtime()
        self.ttl4.on()
        self.ttl6.off()

        print("Finished.")
