from artiq.experiment import *
from mini_mot import absorption_beam, mot_beams, mot_coils
# from experiments import global_parameters
from global_params import mini_mot_global_params as global_parameters
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid
from scipy import constants as cnst

class FluorMagTrapLifetime(EnvExperiment):
    ### Meausres mag trap lifetime via fluorescence imaging 
    ### Since we have absorption scripts now working to accurately calculate the atom number,
    ### it seems less valuable to do option 2 - we can rescale things with a single data point of
    ### absorption data. And reducing the amount of time to take data is good!

    ### upon trying the above, option 1 didn't seem to work that well - there were many dropped
    ### frames, so we will have to go with option 2 even if it takes longer - 2024.10.11
    

    def build(self):

        ## Device building
        self.setattr_device("core")
        self.setattr_device("ttl2") # coil control
        self.setattr_device("ttl4") # mot camera 4/30/2024
        self.setattr_device("ttl5") # srs optical pumping shutter 4/30/2024
        self.setattr_device("wlm0") # wavemeter
        self.setattr_device("sampler0") # MOT power monitor
        self.setattr_device("thorcam1")
        self.absorption_beam = absorption_beam.AbsorptionBeam(self) # urukul0_0, urukul0_1, ttl_3
        self.mot_beams = mot_beams.MOTBeams(self) # urukul0_3, ttl_6
        self.mot_coils = mot_coils.MOTCoils(self) # ttl_2,  zotino_0

        ## Saved/ tunable attribute building

        # image timing params
        self.setattr_argument("lifetime_delay_times_ms", Scannable(global_min=0.02,
                                                                global_max=10000,
                                                                ndecimals=3,
                                                                default=[RangeScan(1., 100., 10)]))


        ## Single attributes, not scanned

        # image params
        self.setattr_argument("imag_t_exp_ms", NumberValue(default=3, ndecimals=1, step=10))
        self.setattr_argument("imag_delay_time_ms", NumberValue(default=0.1, ndecimals=1, step=0.1))
        self.setattr_argument("imag_aom_amp_V", NumberValue(default=0.18, ndecimals=3, step=0.01))
        self.setattr_argument("imag_detuning_gamma", NumberValue(default=-3.5, ndecimals=2, step=0.1))
        
        # MOT params
        self.setattr_argument("mot_current_A", NumberValue(default=4.5, ndecimals=5, step=0.1))
        self.setattr_argument("mot_detuning_gamma", NumberValue(default=-5., ndecimals=5, step=0.1))
        self.setattr_argument("mot_aom_amp_V", NumberValue(default=0.18, ndecimals=5, step=0.1))
        self.setattr_argument('mot_loading_time_s', NumberValue(default=2, ndecimals=2, step=0.1))

        # 2nd stage MOT params
        self.setattr_argument("sec_stage_mot_current_A", NumberValue(default=4., ndecimals=5, step=0.1))
        self.setattr_argument("sec_stage_mot_detuning_gamma", NumberValue(default=-3., ndecimals=5, step=0.1))
        self.setattr_argument("sec_stage_mot_aom_amp_V", NumberValue(default=0.18, ndecimals=5, step=0.1))
        self.setattr_argument('sec_stage_mot_loading_time_ms', NumberValue(default=10, ndecimals=2, step=1))
        self.setattr_argument("sec_stage_mot_bool", BooleanValue(default=False))
        
        # camera params
        self.setattr_argument('camera_gain_dB', NumberValue(default=48, ndecimals=0, step=1))
        self.setattr_argument('camera_t_exp_ms', NumberValue(default=5, ndecimals=3, step=0.1))
        self.setattr_argument('cam_roi_x_0', NumberValue(default=760, ndecimals=0, step=1))
        self.setattr_argument('cam_roi_y_0', NumberValue(default=620, ndecimals=0, step=1))
        self.setattr_argument('cam_roi_w_x', NumberValue(default=100, ndecimals=0, step=1))
        self.setattr_argument('cam_roi_w_y', NumberValue(default=100, ndecimals=0, step=1))

        # other params
        self.setattr_argument("Nshots", NumberValue(default=5, ndecimals=0, step=1))
        self.setattr_argument('isotope', NumberValue(default=48, ndecimals=0, step=1))
        self.setattr_argument('num_power_samples', NumberValue(default=50, ndecimals=0, step=1))

        ## Other attributes, non-saved
        self.a = 1 # random thing that must be set here
        self.camera_trigger_delay = 20 * us
        self.op_shutter_delay = 3*ms
        self.mot_shutter_delay = 2*ms
        self.op_shutter_on_delay = 3*ms
        self.mot_shutter_on_delay = 1.5*ms
        self.shutter_delay_delta = self.op_shutter_delay - self.mot_shutter_delay
        # self.mot_aom_freq = 80e-6 #THz
        # self.mot_aom_amp = 0.12
        # self.brightfield_frequency = 300.80795 #THz
        self.mot_loading_time = 0.5 * s
        self.mot_sampler_channel = 0
        self.mot_power_samples = 50

    def prepare(self):

         ## set up scan
        self.lifetime_delay_times = np.array([t * ms for t in self.lifetime_delay_times_ms])

        ## set up camera ROI
        self.roi = [int(self.cam_roi_x_0), int(self.cam_roi_y_0),
                    int(self.cam_roi_x_0 + self.cam_roi_w_x),
                    int(self.cam_roi_y_0 + self.cam_roi_w_y)]

        # pull the atomic frequencies from global params
        self.cooling_freq = global_parameters.cooling_line_freq[str(self.isotope)]
        self.op_freq = global_parameters.op_line_freq[str(self.isotope)]

        # calculate the detuning that the lasers should be locked to on the wavemeter
        self.vexlum_freq = (self.cooling_freq # set Vexlum freq where there will be max AOM efficiency at MOT dets
                            + (self.mot_detuning_gamma * global_parameters.cooling_gamma_linear
                                - 2 * self.mot_beams.ch2_center_freq) / 10**12) 
        self.sec_stage_mot_aom_freq = (((self.sec_stage_mot_detuning_gamma - self.mot_detuning_gamma) *
                                        (global_parameters.cooling_gamma_linear / 2)) + 
                                        self.mot_beams.ch2_center_freq)
        self.imag_aom_freq = (((self.imag_detuning_gamma - self.mot_detuning_gamma) *
                                (global_parameters.cooling_gamma_linear / 2)) + 
                                self.mot_beams.ch2_center_freq)
        print(self.imag_aom_freq * 1e-6)

        # prep data shape and arrays
        self.data_shape = (len(self.lifetime_delay_times_ms), self.Nshots)
        self.n_imags = len(self.lifetime_delay_times_ms) * self.Nshots

        # prep power monitor arrays
        self.meas_sig_power = np.zeros(self.data_shape)
        self.meas_sig_power_err = np.zeros(self.data_shape)
        self.meas_bright_power = np.zeros(self.data_shape)
        self.meas_bright_power_err = np.zeros(self.data_shape)
        self.meas_mot_power = np.zeros(self.data_shape)
        self.meas_mot_power_err = np.zeros(self.data_shape)


    @kernel(flags={"fast-math"})
    def get_mean_and_std(self, arr):# -> TList(TFloat):
        meanN = 0.0
        stdNS = 0.0
        N = len(arr)

        #mean
        for a in arr:
            meanN += a

        mean = meanN/N

        #std
        for a in arr:
            stdNS += (a-mean)**2

        std = np.sqrt(stdNS/N)

        return mean, std

    @kernel
    def run(self):

        self.a # random thing that must be called here
        self.core.wait_until_mu(now_mu())
        self.core.reset()

        ######## INITIALIZATIONS ########

        ### wrapped initializations
        self.mot_beams.init()
        self.mot_coils.init()
        # self.absorption_beam.init()

        ### ttl initialization
        # self.ttl2.output()
        self.ttl4.output()
        self.ttl5.output()

        # delay(1 * ms)
        # self.ttl2.off() # MOT coils on
        delay(1 * ms)
        self.ttl4.off() # Camera TTL low
        delay(1 * ms)
        self.ttl5.on() # OP shutter open

        ### Sampler initialization
        self.core.break_realtime()
        self.sampler0.init()
        self.sampler0.set_gain_mu(0,0) # check this!
        self.sampler0.set_gain_mu(1,0) # check this!
        self.sampler0.set_gain_mu(2,0) # check this!
        self.sampler0.set_gain_mu(3,0) # check this!

        ### thorcam initialization
        self.thorcam1.connect()
        self.thorcam1.initialize(experiment=self,
                                exposure_time_us = self.camera_t_exp_ms * 1000,
                                operation_mode = 1,
                                frames_per_trigger_zero_for_unlimited = 1,
                                image_poll_timeout_ms = 5000,
                                gain = self.camera_gain_dB * 10,
                                roi = self.roi,
                                trigger_polarity = 0, # 0 for rising edge 1 for falling edge
                                wavelength_nm = 498,
                                )
        self.core.break_realtime()

        img_count = 0
        missing_frame_count = 0
        shot_count = 0

        print("Tuning MOT beams...")
        self.wlm0.SetPIDCourseNum(3, self.op_freq)
        self.wlm0.SetPIDCourseNum(5, self.vexlum_freq)
        self.core.break_realtime()
        delay(0.5 * s)

        ### Loop through imaging delay times
        for i in range(len(self.lifetime_delay_times)):
            t_delay = self.lifetime_delay_times[i]
            print(100 * float(img_count) / self.n_imags, ' percent done')
            
            ### take N shots for each delay time
            for j in range(self.Nshots):

            ### Add a check for missing frames - we only will loop a max of 10 times per frame
                missing_frame_check = 0
                while missing_frame_check == 0:
                    shot_count += 1
                    missing_sig_check = 0
                    missing_bright_check = 0
                    missing_dark_check = 0
                    self.core.break_realtime()

                    ### Arm the camera
                    self.thorcam1.arm(3)
                    self.core.break_realtime()
                    delay(0.2 * ms)

                    ### Load the MOT
                    self.mot_coils.set_current(self.mot_current_A)
                    delay(0.2 * ms)
                    self.mot_coils.turn_on()
                    delay(0.2 * ms)
                    self.ttl5.on() # OP shutter open
                    delay(0.2 * ms)
                    self.mot_beams.set_aom(self.mot_beams.ch2_center_freq, self.mot_beams.ch2_amp)
                    delay(0.2 * ms)
                    self.mot_beams.turn_on()
                    delay(0.2 * ms)
                    self.absorption_beam.turn_off()
                    with parallel:
                        smp_arr = [0.0] * self.num_power_samples
                        # smp_arr_2 = [0.0] * self.mot_power_samples
                    # with parallel:
                        # smp_arr_2 = [[0.0] * 8] * self.num_power_samples
                        delay(self.mot_loading_time_s)
                        # with sequential:
                        #     delay(self.mot_loading_time_s * s / 10)
                        #     for n in range(self.mot_power_samples):
                        #         smp = [0.0] * 8
                        #         self.sampler0.sample(smp)
                        #         smp_arr_2[n] = smp[self.mot_sampler_channel]
                        #         delay(0.8 * self.mot_loading_time_s * s / (self.mot_power_samples - 1))
                        #     self.meas_mot_power[i,j], self.meas_mot_power_err[i,j] = self.get_mean_and_std(smp_arr_2)

                    ### if running a second MOT stage, do that
                    if self.sec_stage_mot_bool:
                        with parallel:
                            self.mot_beams.set_aom(self.sec_stage_mot_aom_freq,
                                                    self.sec_stage_mot_aom_amp_V)
                            self.mot_coils.set_current(self.sec_stage_mot_current_A)
                            delay(self.sec_stage_mot_loading_time_ms * ms)

                    ### Take a signal image after some decay time
                    self.ttl5.off() ### hard coding shuttering off OP 12/3
                    delay(0.2*ms) ### hard coding shuttering off OP 12/3 (approximate shutter delay)
                    with parallel:
                        self.mot_beams.turn_off()
                        # after appropriate delay, send imaging pulse to camera
                        with sequential:
                            delay(t_delay - self.camera_trigger_delay)
                            self.ttl4.pulse(100 * us)
                        with sequential:
                            delay(t_delay - self.imag_delay_time_ms * ms)
                            self.mot_beams.fluor_imag_pulse(self.imag_t_exp_ms * ms,
                                                            self.imag_delay_time_ms * ms,
                                                            self.imag_aom_freq,
                                                            self.imag_aom_amp_V)
                                # self.mot_coils.turn_off() # MOT coils off
                        with sequential:
                            delay(t_delay + self.imag_t_exp_ms * ms / 10)
                            for n in range(self.num_power_samples):
                                smp = [0.0] * 8
                                self.sampler0.sample(smp)
                                smp_arr[n] = smp[self.mot_sampler_channel]
                                delay(0.8 * self.imag_t_exp_ms * ms / (self.num_power_samples - 1))
                    delay(100 * ms)
                    self.meas_sig_power[i,j], self.meas_sig_power_err[i,j] = self.get_mean_and_std(smp_arr)
                    self.core.break_realtime()

                    ### Take a brightfield image
                    with parallel:
                        smp_arr = [0.0] * self.num_power_samples
                        # smp_arr_2 = [[0.0] * 8] * self.num_power_samples
                        self.mot_coils.turn_off() # MOT coils off
                        delay(100 * ms) # let atoms fly away ## jack/anke changed this 12/3 from 50 to 100ms. We were seeing atoms in brightfield
                    with parallel:
                        # after appropriate delay, send imaging pulse to camera
                        with sequential:
                            delay(self.mot_beams.pulse_delay - self.camera_trigger_delay)
                            self.ttl4.pulse(100 * us)
                        self.mot_beams.fluor_imag_pulse(self.imag_t_exp_ms * ms,
                                                        self.mot_beams.pulse_delay,
                                                        self.imag_aom_freq,
                                                        self.imag_aom_amp_V)
                        with sequential: # Power calibration stage
                            delay(self.mot_beams.pulse_delay + self.imag_t_exp_ms * ms / 10)
                            for n in range(self.num_power_samples):
                                smp = [0.0] * 8
                                self.sampler0.sample(smp)
                                smp_arr[n] = smp[self.mot_sampler_channel]
                                delay(0.8 * self.imag_t_exp_ms * ms / (self.num_power_samples - 1))
                            # delay(t_exp / 10)
                    delay(100 * ms)
                    self.meas_bright_power[i,j], self.meas_bright_power_err[i,j] = self.get_mean_and_std(smp_arr)
                    self.core.break_realtime()

                    ### Take a darkfield image
                    self.mot_beams.turn_off()
                    # delay(10 * ms)
                    self.ttl4.pulse(100 * us)

                    # print("Polling camera for images...")
                    self.core.break_realtime()
                    delay(0.5 * ms)
                    self.core.wait_until_mu(now_mu())
                    delay(100 * ms)
                    self.thorcam1.poll_for_frame_and_append_to_buffer(['signal', 'bright', 'dark'], 3)
                    self.core.break_realtime()
                    self.thorcam1.disarm()
                    self.core.break_realtime()

                    ### handle missing frame by comparing against the expected number of images at this point
                    if self.thorcam1.get_buffer_len('signal') < img_count + 1:
                        missing_sig_check = 1
                        # print('missing sig frame')
                    if self.thorcam1.get_buffer_len('bright') < img_count + 1:
                        missing_bright_check = 1
                        # print('missing bright frame')
                    if self.thorcam1.get_buffer_len('dark') < img_count + 1:
                        missing_dark_check = 1
                        # print('missing dark frame')
                    if ((missing_sig_check == 0) and
                        (missing_bright_check == 0) and
                        (missing_dark_check == 0)):
                        missing_frame_check = 1
                    if missing_frame_check == 0:
                        print('missing frame caught, retaking image')
                        missing_frame_count += 1
                        if missing_sig_check == 0:
                            # print('popping extra sig frame')
                            self.thorcam1.pop_buffer('signal')
                        if missing_bright_check == 0:
                            # print('popping extra bright frame')
                            self.thorcam1.pop_buffer('bright')
                        if missing_dark_check == 0:
                            # print('popping extra dark frame')
                            self.thorcam1.pop_buffer('dark')
                    self.core.break_realtime()

                img_count += 1


        self.mot_coils.turn_on()
        self.mot_beams.turn_on()
                
        print("Done with run stage.")
        print('Missing frame count: ', missing_frame_count)
        print('Missing frame frac: ', float(missing_frame_count) / shot_count)

    def analyze(self):
        print("Saving...")
        self.set_dataset("mot_pwr_mon", self.meas_mot_power)
        self.set_dataset("mot_pwr_mon_err", self.meas_mot_power_err)
        self.set_dataset("sig_pwr_mon", self.meas_sig_power)
        self.set_dataset("sig_pwr_mon_err", self.meas_sig_power_err)
        self.set_dataset("bright_pwr_mon", self.meas_bright_power)
        self.set_dataset("bright_pwr_mon_err", self.meas_bright_power_err)

        self.thorcam1.save_all(self)
        # print(self.thorcam1.get_img_shape())
        img_shape = self.thorcam1.get_img_shape()
        data_array_shape = self.data_shape + (img_shape[0], img_shape[1])
        # print(data_array_shape)
        print('reading signals')
        signal, signal_err = self.thorcam1.get_buffer_and_buffer_errs('signal')
        # print(np.array(signal).shape)
        print('reading brights')
        bright, bright_err = self.thorcam1.get_buffer_and_buffer_errs('bright')
        print('reading darks')
        dark, dark_err = self.thorcam1.get_buffer_and_buffer_errs('dark')
        print('reshaping data')
        signal = np.reshape(np.array(signal), data_array_shape)
        print(signal.shape)
        signal_err = np.reshape(np.array(signal_err), data_array_shape)
        bright = np.reshape(np.array(bright), data_array_shape)
        bright_err = np.reshape(np.array(bright_err), data_array_shape)
        dark = np.reshape(np.array(dark), data_array_shape)
        dark_err = np.reshape(np.array(dark_err), data_array_shape)

        sig_imag_for_plotting = np.reshape(np.mean(signal, axis=-3),
                                            (len(self.lifetime_delay_times_ms),
                                            data_array_shape[-2],
                                            data_array_shape[-1])
                                            )
        bright_imag_for_plotting = np.reshape(np.mean(bright, axis=-3),
                                            (len(self.lifetime_delay_times_ms),
                                            data_array_shape[-2],
                                            data_array_shape[-1])
                                            )

        fig = plt.figure(figsize=(2 * len(self.lifetime_delay_times_ms), 2 ))
        grid = ImageGrid(fig, 111, cbar_mode = 'single', share_all = True,
                        nrows_ncols = (1, len(self.lifetime_delay_times_ms)),
                        )
        clim = (0, 1024)
        for ax, im in zip(grid, bright_imag_for_plotting): 
            im = ax.imshow(im, cmap='GnBu_r', clim=clim)
        ax.cax.colorbar(im)
        fig.suptitle('Bright background signal')
        plt.tight_layout()
        plt.show()

        fig = plt.figure(figsize=(2 * len(self.lifetime_delay_times_ms), 2))
        grid = ImageGrid(fig, 111, cbar_mode = 'single', share_all = True,
                        nrows_ncols = (1, len(self.lifetime_delay_times_ms)),
                        )
        clim = (0, 1024)
        for ax, im in zip(grid, sig_imag_for_plotting): 
            im = ax.imshow(im, cmap='GnBu_r', clim=clim)
        ax.cax.colorbar(im)
        fig.suptitle('Signal images')
        plt.tight_layout()
        plt.show()

        fig = plt.figure(figsize=(2 * len(self.lifetime_delay_times_ms), 2 ))
        grid = ImageGrid(fig, 111, cbar_mode = 'single', share_all = True,
                        nrows_ncols = (1, len(self.lifetime_delay_times_ms)),
                        )
        for ax, im in zip(grid, sig_imag_for_plotting - bright_imag_for_plotting): 
            im = ax.imshow(im, cmap='GnBu_r')
        ax.cax.colorbar(im)
        fig.suptitle('Signal - bright images')
        plt.tight_layout()
        plt.show()

        # self.set_dataset("sig_minus_bright", signal - bright)
        counts = np.sum((signal - bright), axis = (-1,-2))
        counts_mean = np.mean(counts, axis=-1)
        counts_std = np.std(counts, axis=-1)

        print('plotting data')
        fig, ax = plt.subplots(figsize=(6,4))

        mec_list = ['darkslategrey', 'maroon', 'indigo', 'black', 'darkgoldenrod', 'darkolivegreen']
        mfc_list = ['darkturquoise', 'indianred', 'darkorchid', 'grey', 'gold', 'greenyellow']
        markers = ['o','s', 'D','^', '*', 'p']
        # for i, amp in enumerate(self.mot_aom_amps_V):
        #     for j, curr in enumerate(self.mot_currents_amps):
        i = 0
        j = 0

        ax.errorbar([(t + self.imag_t_exp_ms / 2) * ms for t in self.lifetime_delay_times_ms],
                    counts_mean - np.min(counts_mean),
                    xerr = self.imag_t_exp_ms * ms / 2,
                    yerr = counts_std,# / np.max(fluor_counts_mean_zeroed),
                    ls='', marker=markers[0],
                    ecolor=mec_list[0],
                    mec=mec_list[0],
                    mfc=mfc_list[0])#,
                    # label = 'AOM amp=%.2fV,IMOT=%.2fA'%(self.mot_aom_amps_V[0], curr))
        # ax.legend()
        # ax.set_ylim(-0.05, 1.05)
        ax.set_xlabel(r'$t$ (s)')
        ax.set_ylabel(r'Fluor counts - min(Fluor counts) (arb.)')

        plt.tight_layout()
        plt.show()

        # self.set_dataset("mot_pwr_mon", self.mot_pwr_mon)
        # self.set_dataset("mot_pwr_mon_err", self.mot_pwr_mon_err)
        # self.set_dataset("brightfield_pwr_mon", self.brightfield_pwr_mon)
        # self.set_dataset("brightfield_pwr_mon_err", self.brightfield_pwr_mon_err)
        print("Done with analyze section")