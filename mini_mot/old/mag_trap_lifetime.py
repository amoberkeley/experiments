from artiq.experiment import *
from mini_mot import absorption_beam, mot_beams
from experiments import global_parameters
import numpy as np
import sys
sys.path.append("..")

class MagTrapLifetime(EnvExperiment):
    
    def build(self):
        
        ## Device building
        self.setattr_device("core")
        self.setattr_device("ttl2") # coil control
        self.setattr_device("ttl4") # mot camera 4/30/2024
        self.setattr_device("ttl5") # srs optical pumping shutter 4/30/2024
        self.setattr_device("wlm0") # wavemeter
        #self.setattr_device("sampler0") # MOT power monitor, ion pump current monitor, etc
        self.mot_beams = mot_beams.MOTBeams(self) # urukul0_3, ttl_6

        ## Saved/ tunable attribute building
        # self.setattr_argument("mot_current", NumberValue(default=3., ndecimals=5, step=0.1))
        # self.setattr_argument("biasx_current", NumberValue(default=0., ndecimals=5, step=0.1))
        # self.setattr_argument("biasy_current", NumberValue(default=0., ndecimals=5, step=0.1))
        # self.setattr_argument("ti_ball_current", NumberValue(default=0., ndecimals=5, step=0.1))
        self.setattr_argument("mot_frequency", NumberValue(ndecimals=6, default=300.80793))
        #self.setattr_argument("absorption_shifts", Scannable(global_min=-150, global_max=150,
                                                           # ndecimals=6,
                                                           # default=[RangeScan(-80, -20, 6)]))
        #self.setattr_argument('blow_away_pulse_len_us', NumberValue(ndecimals=2, default=1000))
        self.setattr_argument("Nshots", NumberValue(default=1, ndecimals=0, step=1))
        self.setattr_argument("Nt_points", NumberValue(default=100, ndecimals=0, step=1))
        self.setattr_argument("t_exp_ms", NumberValue(default=1, ndecimals=2, step=1))
        self.setattr_argument("delta_t_imag_ms", NumberValue(default=10, ndecimals=2, step=1))
        #self.setattr_argument('imaging_pulses_delay_ms', NumberValue(default=50, ndecimals=2, step=1))


        ## Other attributes, non-saved
        self.a = 1 # random thing that must be set here
        self.camera_trigger_delay = 20 * us
        self.op_shutter_delay = 3*ms
        self.mot_shutter_delay = 2*ms
        self.op_shutter_on_delay = 3*ms
        self.mot_shutter_on_delay = 1.5*ms
        self.shutter_delay_delta = self.op_shutter_delay - self.mot_shutter_delay
        self.mot_aom_freq = 80e-6 #THz
        self.mot_aom_amp = 0.12
        # self.brightfield_frequency = 300.80795 #THz
        self.mot_loading_time = 0.5 * s

    def prepare(self):

        #self.absorption_shifts = np.array([shift for shift in self.absorption_shifts])
        # self.measured_mot_frequencies = np.zeros(len(self.mot_frequencies))
        # self.measured_op_frequencies = np.zeros(len(self.mot_frequencies))
        
        self.delay_times_ms = np.arange(self.delta_t_imag_ms, (self.Nt_points+1)*self.delta_t_imag_ms, self.delta_t_imag_ms)

        # self.mot_pwr_mon = np.zeros(len(self.mot_frequencies))
        # self.mot_pwr_mon_err = np.zeros(len(self.mot_frequencies))
        # self.ion_pump_mon = np.zeros(len(self.mot_frequencies))
        # self.ion_pump_mon_err = np.zeros(len(self.mot_frequencies))

        # self.brightfield_pwr_mon = 0.
        # self.brightfield_pwr_mon_err = 0.


    @kernel(flags={"fast-math"})
    def get_mean_and_std(self, arr):# -> TList(TFloat):
        meanN = 0.0
        stdNS = 0.0
        N = len(arr)

        #mean
        for a in arr:
            meanN += a

        mean = meanN/N

        #std
        for a in arr:
            stdNS += (a-mean)**2

        std = np.sqrt(stdNS/N)

        return mean, std

    @kernel
    def run(self):

        self.a
        self.core.wait_until_mu(now_mu())
        self.core.reset()

        ######## INITIALIZATIONS ########

        ### wrapped initializations
        self.mot_beams.init()

        ### ttl initialization
        self.ttl2.output()
        self.ttl4.output()
        self.ttl5.output()

        delay(1 * ms)
        self.ttl2.off() # MOT coils on
        delay(1 * ms)
        self.ttl4.off() # Camera TTL low
        delay(1 * ms)
        self.ttl5.on() # OP shutter open

        # ### Sampler initialization
        # self.core.break_realtime()
        # self.sampler0.init()
        # self.sampler0.set_gain_mu(0,0)
        # self.sampler0.set_gain_mu(1,0)
        # self.sampler0.set_gain_mu(2,0)

        ##########################
        ### MAIN LOOP

        shift_count = 0
        pulse_count = 0
        
        ### tune mot light onto resonance
        print("Tuning MOT beams...")
        self.wlm0.SetPIDCourseNum(1, self.mot_frequency + self.mot_aom_freq / 2)
        self.core.break_realtime()
        delay(0.5*s)
        self.core.wait_until_mu(now_mu())
        delay(0.5*s)

        # ### record measured laser freq
        # self.measured_mot_frequencies[freq_count] = self.wlm0.GetFrequency(1)
        # self.measured_op_frequencies[freq_count] = self.wlm0.GetFrequency(3)
        self.core.break_realtime()
        delay(10 * ms)
        print('taking signal images')

        # # scan thorugh absorption detunings
        # for absorption_shift in self.absorption_shifts:

        for delay_time_ms in self.delay_times_ms:

            print("At delay time (ms): ", delay_time_ms)

            ### take N shots of MOT
            for i in range(self.Nshots):
                self.core.break_realtime()
                delay(10*ms)

                print("Shot: ", i)

                ### prep other beams and fields
                self.ttl2.off() # MOT coils on
                delay(1 * ms)
                self.ttl5.on() # OP shutter open
                delay(1 * ms)
                self.mot_beams.turn_on() # MOT beams on
                delay(1 * ms)

                ### load MOT
                delay(0.25*s)


                self.ttl5.off() #OP off
                delay(self.op_shutter_delay)
                self.mot_beams.turn_off()

                delay(delay_time_ms*ms-self.mot_beams.pulse_delay)

                self.mot_beams.turn_on()
                delay(20*us) #delay to avoid camera jitter
                self.ttl4.pulse(1*ms) # trigger camera
                delay(1*ms + self.t_exp_ms * ms)
                self.ttl2.on() # coil off to make sure no mag trapped atoms hang around
                delay(10*ms)
                self.core.wait_until_mu(now_mu())


        self.core.break_realtime()
        delay(10*ms)

        ### Take brightfield images
        print('taking brightfield images')
        self.ttl2.on() # turn off coils
        self.mot_beams.turn_on() # MOT beams on
        delay(50 * ms)
        self.ttl5.on() # OP shutter open
        delay(1 * ms)
        for j in range(self.Nshots):
            self.ttl4.pulse(100 * us)
            delay(self.delta_t_imag_ms * ms + 5 * ms)

        ### Take darkfield images
        print('taking darkfield images')
        self.mot_beams.turn_off() #turn off light
        delay(1 * ms)
        self.ttl5.off() # turn off OP light
        delay(100 * ms)
        for j in range(self.Nshots):
            self.ttl4.pulse(100 * us)
            delay(self.delta_t_imag_ms * ms + 5 * ms)

        self.core.break_realtime()
        self.ttl5.off() # op beams off
        delay(1 * ms)
        self.mot_beams.turn_on() # MOT beams on
        delay(1 * ms)
        self.ttl2.off() # MOT coils on
        print("Done with run stage.")

    def analyze(self):
        print("Saving...")
        # self.set_dataset("measured_mot_frequencies", self.measured_mot_frequencies)
        # self.set_dataset("measured_op_frequencies", self.measured_op_frequencies)
        # self.set_dataset("mot_pwr_mon", self.mot_pwr_mon)
        # self.set_dataset("mot_pwr_mon_err", self.mot_pwr_mon_err)
        # self.set_dataset("ion_pump_mon", self.ion_pump_mon)
        # self.set_dataset("ion_pump_mon_err", self.ion_pump_mon_err)
        # self.set_dataset("brightfield_pwr_mon", self.brightfield_pwr_mon)
        # self.set_dataset("brightfield_pwr_mon_err", self.brightfield_pwr_mon_err)
        print("Done with analyze stage.")