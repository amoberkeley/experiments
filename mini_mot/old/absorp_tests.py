from artiq.experiment import *
from mini_mot import absorption_beam, mot_beams
from experiments import global_parameters
import numpy as np
import sys
sys.path.append("..")

class AbsorpTests(EnvExperiment):

	def build(self):

		## Device building
		self.setattr_device("core")
		self.setattr_device("ttl2") # coil control
		self.setattr_device("ttl4") # mot camera 4/30/2024
		self.setattr_device("ttl5") # srs optical pumping shutter 4/30/2024
		self.setattr_device("wlm0") # wavemeter
		self.setattr_device("sampler0") # MOT power monitor
		self.absorption_beam = absorption_beam.AbsorptionBeam(self) # urukul0_0, urukul0_1, ttl_3
		self.mot_beams = mot_beams.MOTBeams(self) # urukul0_3, ttl_6

		## Saved/ tunable attribute building
		# self.setattr_argument("mot_current", NumberValue(default=3., ndecimals=5, step=0.1))
		# self.setattr_argument("biasx_current", NumberValue(default=0., ndecimals=5, step=0.1))
		# self.setattr_argument("biasy_current", NumberValue(default=0., ndecimals=5, step=0.1))
		# self.setattr_argument("ti_ball_current", NumberValue(default=0., ndecimals=5, step=0.1))
		self.setattr_argument("mot_frequency", NumberValue(ndecimals=6, default=300.80793))
		self.setattr_argument("absorption_offsets_MHz", Scannable(global_min=-100, global_max=0,
																	ndecimals=6,
																	default=[RangeScan(-80, -40, 5)]))
		self.camera_trigger_delay = 20 * us
		self.setattr_argument("image_delay_time_us", NumberValue(ndecimals=3, default=200.0))
		self.setattr_argument("Nshots", NumberValue(default=1, ndecimals=0, step=1))
		self.setattr_argument("t_exp_us", NumberValue(default=100, ndecimals=0, step=1))

		## Other attributes, non-saved
		self.a = 1 # random thing that must be set here
		self.op_shutter_delay = 3*ms
		self.mot_shutter_delay = 2*ms
		self.op_shutter_on_delay = 3*ms
		self.mot_shutter_on_delay = 1.5*ms
		self.shutter_delay_delta = self.op_shutter_delay - self.mot_shutter_delay
		self.mot_aom_freq = 80e-6 #THz
		self.mot_aom_amp = 0.12
		self.brightfield_frequency = 300.80795 #THz
		self.mot_loading_time = 0.25 * s

	def prepare(self):

		self.absorption_offsets = np.array([offset * MHz for offset in self.absorption_offsets_MHz])
		# self.mot_frequencies = np.array([freq for freq in self.mot_frequencies])
		# self.measured_mot_frequencies = np.zeros(len(self.mot_frequencies))
		# self.measured_op_frequencies = np.zeros(len(self.mot_frequencies))

		# self.delay_times_us = np.array([delay_time for delay_time in self.delay_times_us])

		# self.mot_pwr_mon = np.zeros(len(self.mot_frequencies))
		# self.mot_pwr_mon_err = np.zeros(len(self.mot_frequencies))
		# self.ion_pump_mon = np.zeros(len(self.mot_frequencies))
		# self.ion_pump_mon_err = np.zeros(len(self.mot_frequencies))

		# self.brightfield_pwr_mon = 0.
		# self.brightfield_pwr_mon_err = 0.


	@kernel(flags={"fast-math"})
	def get_mean_and_std(self, arr):# -> TList(TFloat):
		meanN = 0.0
		stdNS = 0.0
		N = len(arr)

		#mean
		for a in arr:
			meanN += a

			mean = meanN/N

		#std
		for a in arr:
			stdNS += (a-mean)**2

			std = np.sqrt(stdNS/N)

		return mean, std

	@kernel
	def run(self):

		self.a
		self.core.wait_until_mu(now_mu())
		self.core.reset()

		######## INITIALIZATIONS ########

		### wrapped initializations
		self.absorption_beam.init()
		self.mot_beams.init()

		### ttl initialization
		self.ttl2.output()
		self.ttl4.output()
		self.ttl5.output()

		delay(1 * ms)
		self.ttl2.off() # MOT coils on
		delay(1 * ms)
		self.ttl4.off() # Camera TTL low
		delay(1 * ms)
		self.ttl5.on() # OP shutter open

		### Sampler initialization
		self.core.break_realtime()
		self.sampler0.init()
		self.sampler0.set_gain_mu(0,0)
		self.sampler0.set_gain_mu(1,0)
		self.sampler0.set_gain_mu(2,0)

		##########################
		### MAIN LOOP

		### tune mot light onto resonance
		print("Tuning MOT beams...")
		self.wlm0.SetPIDCourseNum(1, self.mot_frequency + self.mot_aom_freq / 2)
		self.core.break_realtime()
		delay(0.5*s)
		self.core.wait_until_mu(now_mu())
		delay(0.5*s)

		### Loop through absoption AOM offsets
		for offset in self.absorption_offsets:

			print("Taking data for absoption offset of ", offset)
			self.core.break_realtime()
			delay(10 * ms)

			self.absorption_beam.set_delta(offset)
			delay(10 * ms)

			### Loop for the number of shots:
			for i in range(self.Nshots):




				self.core.break_realtime()
				delay(10 * ms)

				### Take brightfield background image
				delay(50 * ms)
				with parallel:
					self.absorption_beam.pulse(self.t_exp_us * us) # send absorption pulse
					with sequential:
						delay(self.absorption_beam.pulse_delay - self.camera_trigger_delay)
						self.ttl4.pulse(100*us) # trigger camera

				### Load MOT
				self.ttl2.off() # coil on
				delay(1 * ms)
				self.ttl5.on() # OP shutter open
				delay(1 * ms)
				self.mot_beams.turn_on()
				delay(self.mot_loading_time)

				### Take MOT absorption image

				with parallel:

					#pulse absorption light on
					self.absorption_beam.pulse(self.t_exp_us * us)

					#accounting for absorption pulse delay, pulse MOT off and then back on
					with sequential:
						delay(self.absorption_beam.pulse_delay - self.image_delay_time_us*us)
						# turn fields off
						self.ttl2.on()
						delay(250*us)
						with parallel:
							# turn coils off
							self.mot_beams.turn_off()
							with sequential:
								delay(self.image_delay_time_us*us - self.camera_trigger_delay - 250 * us)
								self.ttl4.pulse(100 * us)
						
				# ### Take brightfield background image
				# delay(50 * ms)
				# with parallel:
				# 	self.absorption_beam.pulse(self.t_exp_us * us) # send absorption pulse
				# 	with sequential:
				# 		delay(self.absorption_beam.pulse_delay - self.camera_trigger_delay)
				# 		self.ttl4.pulse(100*us) # trigger camera

				### Take darkfield background image
				delay(50 * ms)
				self.ttl5.off() # turn off OP
				delay(self.op_shutter_delay)
				self.ttl4.pulse(100*us) # trigger camera
				delay(50 * ms)
				self.core.wait_until_mu(now_mu())

			self.core.wait_until_mu(now_mu())

		delay(10 * ms)
		self.ttl2.off() #leave coils on to stay warm
		
		print('Run stage done.')

	def analyze(self):
		print("Saving...")
		self.set_dataset('absorption_offsets', self.absorption_offsets)
		# self.set_dataset("measured_mot_frequencies", self.measured_mot_frequencies)
		# self.set_dataset("measured_op_frequencies", self.measured_op_frequencies)
		# self.set_dataset("mot_pwr_mon", self.mot_pwr_mon)
		# self.set_dataset("mot_pwr_mon_err", self.mot_pwr_mon_err)
		# self.set_dataset("ion_pump_mon", self.ion_pump_mon)
		# self.set_dataset("ion_pump_mon_err", self.ion_pump_mon_err)
		# self.set_dataset("brightfield_pwr_mon", self.brightfield_pwr_mon)
		# self.set_dataset("brightfield_pwr_mon_err", self.brightfield_pwr_mon_err)
		print("Analyze stage done.")