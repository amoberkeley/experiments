from artiq.experiment import *
from mini_mot import absorption_beam, mot_beams, mot_coils
# from experiments import global_parameters
from global_params import mini_mot_global_params as global_parameters
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid
import sys
sys.path.append("..")

class MagTrapVideo(EnvExperiment):
    
    def build(self):
        
        ## Device building
        self.setattr_device("core")
        # self.setattr_device("zotino0")
        # self.setattr_device("ttl2") # coil control
        self.setattr_device("ttl4") # mot camera 4/30/2024
        self.setattr_device("ttl5") # srs optical pumping shutter 4/30/2024
        self.setattr_device("wlm0") # wavemeter
        self.setattr_device("sampler0") # MOT power monitor, ion pump current monitor, etc
        self.setattr_device("thorcam1")
        self.mot_beams = mot_beams.MOTBeams(self, aom_freq = 200 * MHz) # urukul0_2, ttl_6
        self.mot_coils = mot_coils.MOTCoils(self) # zotino0, ttl2
        self.absorption_beam = absorption_beam.AbsorptionBeam(self)

        ## Saved/ tunable attribute building
        ## Scannable attributes for calibration
        # self.setattr_argument('mag_trap_current_A', Scannable(global_min = 0.0, global_max = 4.5,
        #                                                     ndecimals = 2,
        #                                                     default = [RangeScan(4.0, 4.0, 1)]))
        # self.setattr_argument('mag_trap_ramp_t_ms', Scannable(global_min = 0.0, global_max = 5.0,
        #                                                     ndecimals = 2,
        #                                                     default = [RangeScan(2.0, 2.0, 1)]))

        ## None scanned attributes - tunable to get good signals
        self.setattr_argument("mag_trap_hold_time_ms", NumberValue(default=20., ndecimals=2, step=0.1))
        self.setattr_argument('absorp_pulse_delay_ms', NumberValue(default = 0.5, ndecimals = 2))
        self.setattr_argument('absorp_pulse_len_us', NumberValue(default = 0.0, ndecimals = 1))
        self.setattr_argument("Nshots", NumberValue(default=10, ndecimals=0, step=1))
        self.setattr_argument('isotope', NumberValue(default=48, ndecimals=0, step=1))

        ## Fixed attributes, non-saved
        self.mot_detuning_gamma = -2.0
        self.mot_aom_amp_V = 0.18
        self.mot_current_A = 4.0
        self.imag_detuning_gamma = 0.0
        self.imag_aom_amp_V = 0.18
        self.imag_t_exp_ms = 1.5
        self.imag_pulse_delay_ms = 0.3
        self.absorp_detuning_gamma = 0.0
        self.absorp_aom_amp_V = 0.18
        self.camera_gain_dB = 48
        self.camera_t_exp_ms = 1.8
        self.roi_w_x = 300
        self.roi_w_y = 300
        self.roi_x_0 = 620
        self.roi_y_0 = 450
        self.roi = [self.roi_x_0, self.roi_y_0,
                    self.roi_x_0 + self.roi_w_x,
                    self.roi_y_0 + self.roi_w_y]
        self.camera_trigger_delay = 20 * us
        self.op_shutter_delay = 3 * ms
        self.mot_shutter_delay = 2 * ms
        self.op_shutter_on_delay = 3 * ms
        self.mot_shutter_on_delay = 1.5 * ms
        self.shutter_delay_delta = self.op_shutter_delay - self.mot_shutter_delay
        self.mot_loading_time = 0.2 * s
        self.num_power_samples = 10
        self.a = 1 # random thing that must be set here

    def prepare(self):

        # pull the atomic frequencies from global params
        self.cooling_freq = global_parameters.cooling_line_freq[str(self.isotope)]
        self.op_freq = global_parameters.op_line_freq[str(self.isotope)]

        # calculate the detuning that the lasers should be locked to on the wavemeter
        self.vexlum_freq = (self.cooling_freq # set Vexlum frequency where there will be maximum AOM efficiency at MOT detuning
                             + (self.mot_detuning_gamma - 2 * self.mot_beams.ch2_center_freq) / 10**12)
        self.fluor_imag_aom_freq = (self.mot_beams.ch2_center_freq +
                                    (self.imag_detuning_gamma - self.mot_detuning_gamma) *
                                    global_parameters.cooling_gamma_linear / 2)
        self.absorp_aom_freq = (self.mot_beams.ch2_center_freq +
                                (self.absorp_detuning_gamma - self.mot_detuning_gamma) *
                                global_parameters.cooling_gamma_linear / 2)

        # set up other scans
        # self.mot_currents = np.array([i for i in self.mot_currents_amps])
        # self.mot_aom_amp = np.array([v for v in self.mot_aom_amps_V])

        # set up arrays to save measured sampler data into, fitting it into properly sized arrays
        # self.n_imags = len(self.mot_dets) * len(self.mot_aom_amp) * len(self.mot_currents) * self.Nshots
        # self.data_shape = (len(self.mot_dets), len(self.mot_aom_amp), len(self.mot_currents), self.Nshots)
        # self.measured_imag_power = np.zeros(int(self.n_imags))
        # self.measured_imag_power_err = np.zeros(int(self.n_imags))

        self.mot_aom_freq = self.mot_beams.ch2_center_freq

        self.aom_thermalization_time = 4 * s

    def broadcast_last_image(self):

        signal_imgs, signal_img_errs = self.thorcam1.get_buffer_and_buffer_errs("signal")
        bright_imgs, bright_img_errs = self.thorcam1.get_buffer_and_buffer_errs("bright")
        #dark_imgs, dark_img_errs = self.thorcam1.get_buffer_and_buffer_errs("dark")

        last_image = np.array(signal_imgs[-1]) - np.array(bright_imgs[-1])

        # plt.imshow(last_image)
        # plt.show()
        print(last_image.shape)

        self.set_dataset("last_image", last_image, broadcast=True)



    @kernel(flags={"fast-math"})
    def get_mean_and_std(self, arr):# -> TList(TFloat):
        meanN = 0.0
        stdNS = 0.0
        N = len(arr)

        #mean
        for a in arr:
            meanN += a

        mean = meanN/N

        #std
        for a in arr:
            stdNS += (a-mean)**2

        std = np.sqrt(stdNS/N)

        return mean, std

    @kernel
    def run(self):

        self.a # random thing that must be called here
        self.core.wait_until_mu(now_mu())
        self.core.reset()

        ######## INITIALIZATIONS ########

        ### wrapped initializations
        self.mot_beams.init()
        self.mot_coils.init()
        self.absorption_beam.init()

        ### ttl initialization
        # self.ttl2.output()
        self.ttl4.output()
        self.ttl5.output()
        # self.ttl2.off() # MOT coils on
        delay(1 * ms)
        self.ttl4.off() # Camera TTL low
        delay(1 * ms)
        self.ttl5.on() # OP shutter open

        ### Sampler initialization
        self.core.break_realtime()
        self.sampler0.init()
        self.sampler0.set_gain_mu(0,0) # check this!
        self.sampler0.set_gain_mu(1,0) # check this!
        self.sampler0.set_gain_mu(2,0) # check this!
        self.sampler0.set_gain_mu(3,0) # check this!

        ### thorcam initialization
        self.thorcam1.connect()
        self.thorcam1.initialize(experiment=self,
                                exposure_time_us = self.camera_t_exp_ms * 1000,
                                operation_mode = 1,
                                frames_per_trigger_zero_for_unlimited = 1,
                                image_poll_timeout_ms = 1500,
                                gain = self.camera_gain_dB * 10,
                                roi = self.roi,
                                trigger_polarity = 0, # 0 for rising edge 1 for falling edge
                                wavelength_nm = 498,
                                )
        self.core.break_realtime()

        ##########################
        ### MAIN LOOP

        img_count = 0
        missing_frame_count = 0
        shot_count = 0

        ### Tune wavemeter for vexlum
        print("Tuning MOT beams...")
        self.wlm0.SetPIDCourseNum(3, self.op_freq)
        self.wlm0.SetPIDCourseNum(5, self.vexlum_freq)
        self.core.break_realtime()
        # print(100 * float(img_count) / self.n_imags, ' percent done')
        delay(0.5 * s)
        # print('aom frequency:', aom_freq / MHz, ' MHz')

        ### set aom/ coils appropriately
        self.mot_beams.set_aom_amp(self.mot_aom_amp_V)
        delay(1 * ms)
        self.absorption_beam.set_aom(self.absorp_aom_freq, self.absorp_aom_amp_V)
        self.mot_coils.set_current(self.mot_current_A)
        delay(1 * ms)

        for k in range(10):

            ### Add a check for missing frames - we only will loop a max of 10 times per frame
            missing_frame_check = 0
            while missing_frame_check == 0:
                shot_count += 1
                missing_sig_check = 0
                missing_bright_check = 0
                missing_dark_check = 0
                self.core.break_realtime()

                ### Arm the camera
                self.thorcam1.arm(3)
                self.core.break_realtime()
                delay(0.5 * ms)

                ### Load the MOT
                self.mot_coils.turn_on()
                delay(0.5 * ms)
                self.ttl5.on() # OP shutter open
                delay(0.5 * ms)
                self.mot_beams.turn_on()
                # delay(1 * ms)
                delay(self.mot_loading_time)

                ### Turn off MOT, optically pump atoms
                with parallel:
                    with sequential:
                        delay(self.absorption_beam.pulse_delay - self.absorp_pulse_delay_ms * ms) # bad naming of things!
                        self.mot_coils.turn_off()
                        self.mot_beams.turn_off()
                    self.absorption_beam.absorp_pulse(self.absorp_pulse_len_us * us)

                ### Turn on mag trap, hold for set hold time
                self.mot_coils.turn_on()
                delay(self.mag_trap_hold_time_ms * ms)

                ### Turn off mag trap, take a fluor signal image
                with parallel:
                    # after appropriate delay, send imaging pulse to camera
                    with sequential:
                        delay(self.mot_beams.pulse_delay - self.camera_trigger_delay)
                        self.ttl4.pulse(100 * us)
                    with sequential:
                        delay(self.mot_beams.pulse_delay - self.imag_pulse_delay_ms * ms)
                        self.mot_coils.turn_off()
                    self.mot_beams.fluor_imag_pulse(self.imag_t_exp_ms * ms,
                                                    self.mot_beams.pulse_delay,
                                                    self.fluor_imag_aom_freq,
                                                    self.imag_aom_amp_V)

                ### Take a brightfield image
                delay(50 * ms) # let atoms fly away
                with parallel:
                    # after appropriate delay, send imaging pulse to camera
                    with sequential:
                        delay(self.mot_beams.pulse_delay - self.camera_trigger_delay)
                        self.ttl4.pulse(100 * us)
                    self.mot_beams.fluor_imag_pulse(self.imag_t_exp_ms * ms,
                                                    self.mot_beams.pulse_delay,
                                                    self.fluor_imag_aom_freq,
                                                    self.imag_aom_amp_V)

                ### Power calibration stage
                ### should this be changed to be contemporaneous with the images being taken???
                # self.mot_beams.set_aom(self.fluor_imag_aom_freqs[i], self.imag_aom_amp_V)
                # delay(10 * us)
                # self.mot_beams.turn_on()
                # delay(0.5 * ms)
                # smp_arr = [0.0] * self.num_power_samples
                # delay(0.5 * ms)
                # for n in range(self.num_power_samples):
                #     smp = [0.0] * 8
                #     delay(0.5 * ms)
                #     self.sampler0.sample(smp)
                #     smp_arr[n] = smp[3]
                # delay(0.5 * ms)
                # self.measured_imag_power[img_count], self.measured_imag_power_err[img_count] = self.get_mean_and_std(smp_arr)
                # self.core.break_realtime()
                # # delay(0.1 * s)

                ### Take a darkfield image
                self.mot_beams.turn_off()
                delay(10 * ms)
                self.ttl4.pulse(100 * us)
                self.mot_beams.set_aom(self.mot_beams.ch2_center_freq, self.mot_aom_amp_V)

                # print("Polling camera for images...")
                self.core.break_realtime()
                delay(0.5 * ms)
                self.thorcam1.poll_for_frame_and_append_to_buffer(['signal', 'bright', 'dark'], 3)
                self.core.break_realtime()
                self.thorcam1.disarm()
                self.core.break_realtime()

                ### handle missing frame by comparing against the expected number of images at this point
                if self.thorcam1.get_buffer_len('signal') < img_count + 1:
                    missing_sig_check = 1
                    # print('missing sig frame')
                if self.thorcam1.get_buffer_len('bright') < img_count + 1:
                    missing_bright_check = 1
                    # print('missing bright frame')
                if self.thorcam1.get_buffer_len('dark') < img_count + 1:
                    missing_dark_check = 1
                    # print('missing dark frame')
                if ((missing_sig_check == 0) and
                    (missing_bright_check == 0) and
                    (missing_dark_check == 0)):
                    missing_frame_check = 1
                if missing_frame_check == 0:
                    print('missing frame caught, retaking image')
                    missing_frame_count += 1
                    if missing_sig_check == 0:
                        # print('popping extra sig frame')
                        self.thorcam1.pop_buffer('signal')
                    if missing_bright_check == 0:
                        # print('popping extra bright frame')
                        self.thorcam1.pop_buffer('bright')
                    if missing_dark_check == 0:
                        # print('popping extra dark frame')
                        self.thorcam1.pop_buffer('dark')
                self.core.break_realtime()

            img_count += 1
            # print(img_count,'images taken out of', self.n_imags)

            self.broadcast_last_image()

            self.core.break_realtime()
            delay(0.1*s)

        ### do something here in thorcam to bg subtract/process the most recent image
        ### set that image to a dataset like self.set_dataset("current_image", broadcast=True)
        ### run img applet that displays this dataset.

        self.core.break_realtime()
        self.ttl5.on() # op beams on
        delay(0.5 * ms)
        self.mot_beams.set_aom(self.mot_beams.ch2_center_freq, self.mot_beams.ch2_amp)
        delay(0.5 * ms)
        self.mot_beams.turn_on() # MOT beams on
        delay(0.5 * ms)
        self.mot_coils.turn_on() # MOT coils on
        print("Done with run stage.")
        print('Missing frame count: ', missing_frame_count)
        print('Missing frame frac: ', float(missing_frame_count) / shot_count)

    # def analyze(self):
        # print("Saving...")
        # self.thorcam1.save_all(self)
        # # print(self.thorcam1.get_img_shape())
        # img_shape = self.thorcam1.get_img_shape()
        # data_array_shape = self.data_shape + (img_shape[0] + 1, img_shape[1] + 1)
        # # print(data_array_shape)
        # signal, signal_err = self.thorcam1.get_buffer_and_buffer_errs('signal')
        # # print(np.array(signal).shape)
        # bright, bright_err = self.thorcam1.get_buffer_and_buffer_errs('bright')
        # dark, dark_err = self.thorcam1.get_buffer_and_buffer_errs('dark')
        # signal = np.reshape(np.array(signal), data_array_shape)
        # # print(signal.shape)
        # signal_err = np.reshape(np.array(signal_err), data_array_shape)
        # bright = np.reshape(np.array(bright), data_array_shape)
        # bright_err = np.reshape(np.array(bright_err), data_array_shape)
        # dark = np.reshape(np.array(dark), data_array_shape)
        # dark_err = np.reshape(np.array(dark_err), data_array_shape)

        # meas_imag_pow = np.mean(np.reshape(self.measured_imag_power,
        #                                     self.data_shape),
        #                         axis=-1)
        # meas_imag_err = np.sqrt(np.sum(np.reshape(self.measured_imag_power_err,
        #                                             self.data_shape)**2,
        #                                 axis=-1)) / self.Nshots
        # # print(meas_imag_pow.shape)
        # # print(meas_imag_pow)
        # # print(meas_imag_err)
        # self.set_dataset("measured_imag_power", meas_imag_pow)
        # self.set_dataset("measured_imag_power_err", meas_imag_err)

        # #do background subtraction and calculate error in signal counts - check with Jack
        # sig_bkg_sub = signal - bright # no way to meas. power diff. b/w bright & sig, assuming they are equal
        # sig_bkg_sub_err = np.sqrt(signal_err**2 + bright_err**2)
        # sig_bkg_sub_mean = np.mean(sig_bkg_sub, axis=-3)
        # # print(sig_bkg_sub_mean.shape)
        # sig_bkg_sub_err_mean = np.sqrt(np.sum(sig_bkg_sub_err**2, axis=-3)) / self.Nshots 
        # sig_bkg_sub_std = np.std(sig_bkg_sub, axis=-3)

        # sig_mean = np.mean(signal, axis=-3)
        # sig_std = np.std(signal, axis=-3)
        # sig_err_mean = np.sqrt(np.sum(signal_err**2, axis=-3)) / self.Nshots

        # imag_for_plotting = np.reshape(sig_bkg_sub_mean,
        #                                 (int(self.n_imags / self.Nshots),
        #                                 data_array_shape[-2],
        #                                 data_array_shape[-1])
        #                                 )
        # sig_imag_for_plotting = np.reshape(np.mean(signal, axis=-3),
        #                                     (int(self.n_imags / self.Nshots),
        #                                     data_array_shape[-2],
        #                                     data_array_shape[-1])
        #                                     )
        # bright_imag_for_plotting = np.reshape(np.mean(bright, axis=-3),
        #                                     (int(self.n_imags / self.Nshots),
        #                                     data_array_shape[-2],
        #                                     data_array_shape[-1])
        #                                     )

        # # fig = plt.figure(figsize=(2 * self.data_shape[0], 2 * self.data_shape[1] * self.data_shape[2]))
        # # grid = ImageGrid(fig, 111, cbar_mode = 'single', share_all = True,
        # #                 nrows_ncols = (self.data_shape[1] * self.data_shape[2], self.data_shape[0]),
        # #                 )
        # # clim = (np.min(imag_for_plotting), np.max(imag_for_plotting))
        # # for ax, im in zip(grid, imag_for_plotting): 
        # #     im = ax.imshow(im, cmap='GnBu_r', clim=clim)
        # # ax.cax.colorbar(im)
        # # plt.show()

        # fig = plt.figure(figsize=(2 * self.data_shape[0], 2 * self.data_shape[1] * self.data_shape[2]))
        # grid = ImageGrid(fig, 111, cbar_mode = 'single', 
        #                 nrows_ncols = (self.data_shape[1] * self.data_shape[2], self.data_shape[0]),
        #                 )
        # clim = (np.min(sig_imag_for_plotting), 1048)
        # for ax, im in zip(grid, sig_imag_for_plotting): 
        #     im = ax.imshow(im, cmap='GnBu_r', clim=clim)
        # ax.cax.colorbar(im)
        # plt.show()

        # # fig = plt.figure(figsize=(2 * self.data_shape[0], 2 * self.data_shape[1] * self.data_shape[2]))
        # # grid = ImageGrid(fig, 111, cbar_mode = 'single', 
        # #                 nrows_ncols = (self.data_shape[1] * self.data_shape[2], self.data_shape[0]),
        # #                 )
        # # clim = (np.min(bright_imag_for_plotting), np.max(bright_imag_for_plotting))
        # # for ax, im in zip(grid, bright_imag_for_plotting): 
        # #     im = ax.imshow(im, cmap='GnBu_r', clim=clim)
        # # ax.cax.colorbar(im)
        # # plt.show()

        # # fluor_counts = np.sum(sig_bkg_sub, axis=(-1, -2))
        # # fluor_counts_mean = np.mean(fluor_counts, axis = -1)
        # # fluor_counts_std = np.std(fluor_counts, axis=-1)
        # # fluor_counts_err_from_pixel_err = np.sqrt(np.sum(sig_bkg_sub_err_mean**2, axis=(-1, -2)))
        # # fluor_counts_err_from_pixel_std = np.sqrt(np.sum(sig_bkg_sub_std**2, axis=(-1, -2)))
        # # fluor_counts_err = np.maximum(fluor_counts_std,
        # #                                 np.maximum(fluor_counts_err_from_pixel_err,
        # #                                             fluor_counts_err_from_pixel_std))

        # fluor_counts = np.sum(signal, axis=(-1, -2))
        # fluor_counts_mean = np.mean(fluor_counts, axis = -1)
        # fluor_counts_std = np.std(fluor_counts, axis=-1)
        # fluor_counts_err_from_pixel_err = np.sqrt(np.sum(sig_err_mean**2, axis=(-1, -2)))
        # fluor_counts_err_from_pixel_std = np.sqrt(np.sum(sig_std**2, axis=(-1, -2)))
        # fluor_counts_err = fluor_counts_std #np.maximum(fluor_counts_std,
        #                     #            np.maximum(fluor_counts_err_from_pixel_err,
        #                      #                       fluor_counts_err_from_pixel_std))

        # # print(fluor_counts_mean.shape)

        # detuning_array, aom_amp_array, t_exp_array = np.meshgrid(self.fluor_imag_detunings_gamma,
        #                                                         self.fluor_imag_aom_Vs,
        #                                                         self.fluor_imag_t_exps,
        #                                                         indexing='ij')

        # fig, ax = plt.subplots(figsize=(5,4))

        # mec_list = ['darkslategrey', 'maroon', 'indigo', 'black', 'darkgoldenrod', 'darkolivegreen']
        # mfc_list = ['darkturquoise', 'indianred', 'darkorchid', 'grey', 'gold', 'greenyellow']
        # markers = ['o','s', 'D','^', '*', 'p']
        # for i, amp in enumerate(self.fluor_imag_aom_Vs):
        #     for j, t in enumerate(self.fluor_imag_t_exps):
        #         fluor_counts_mean_zeroed = (fluor_counts_mean[:,i,j] - np.min(fluor_counts_mean[:,i,j])) / meas_imag_pow[:,i,j]
        #         fluor_counts_mean_scaled = fluor_counts_mean_zeroed / np.max(fluor_counts_mean_zeroed)
        #         # print(fluor_counts_mean[:,i,j].shape)
        #         # print(fluor_counts_err[:,i,j].shape)
        #         # print(self.fluor_imag_detunings_gamma.shape)
        #         ax.errorbar(self.fluor_imag_detunings_gamma,
        #                     fluor_counts_mean_scaled,
        #                     yerr = fluor_counts_err[:,i,j] / np.max(fluor_counts_mean_zeroed),
        #                     ls='', marker=markers[i],
        #                     ecolor=mec_list[j],
        #                     mec=mec_list[j],
        #                     mfc=mfc_list[j],
        #                     label = 'amp='+str(amp)+', texp='+str(t*1000))
        # ax.legend()
        # ax.set_ylim(-0.05, 1.05)

        # plt.tight_layout()
        # plt.show()

        # print("Done with analyze stage.")