from artiq.experiment import *
import numpy as np
import sys
sys.path.append("..")

class PGCoolTests(EnvExperiment):
    
    def build(self):
        self.setattr_device("core")
        self.setattr_device("ttl2") # coil control
        self.setattr_device("ttl4") # mot camera 4/30/2024
        self.setattr_device("ttl5") # srs optical pumping shutter 4/30/2024
        self.setattr_device("wlm0")
        self.setattr_device("sampler0")
        self.setattr_device("urukul0_ch3")
        # self.setattr_argument("mot_current", NumberValue(default=3., ndecimals=5, step=0.1))
        # self.setattr_argument("biasx_current", NumberValue(default=0., ndecimals=5, step=0.1))
        # self.setattr_argument("biasy_current", NumberValue(default=0., ndecimals=5, step=0.1))
        # self.setattr_argument("ti_ball_current", NumberValue(default=0., ndecimals=5, step=0.1))
        self.setattr_argument("mot_frequencies", Scannable(global_max=400,
                                                           ndecimals=6,
                                                           default=[RangeScan(300.807905, 300.807945, 10)]))
        self.camera_trigger_delay = 20 * us
        self.setattr_argument("delay_times_us", Scannable(global_min=self.camera_trigger_delay / us,
                                                          global_max=100000,
                                                          ndecimals=3,
                                                          default=[RangeScan(100., 5000., 10)]))
        self.setattr_argument("Nshots", NumberValue(default=1, ndecimals=0, step=1))
        self.setattr_argument("t_exp_us", NumberValue(default=40, ndecimals=0, step=1))
        self.a = 1 # random thing that must be set here
        self.op_shutter_delay = 3*ms
        self.mot_shutter_delay = 2*ms
        self.op_shutter_on_delay = 3*ms
        self.mot_shutter_on_delay = 1.5*ms
        self.shutter_delay_delta = self.op_shutter_delay - self.mot_shutter_delay
        self.mot_aom_freq = 80e-6 #THz
        self.mot_aom_amp = 0.12
        self.brightfield_frequency = 300.80795 #THz
        self.aom_warmup_delay = 8.0 * s

    def prepare(self):

        self.mot_frequencies = np.array([freq for freq in self.mot_frequencies])
        self.measured_mot_frequencies = np.zeros(len(self.mot_frequencies))
        self.measured_op_frequencies = np.zeros(len(self.mot_frequencies))
        
        self.delay_times_us = np.array([delay_time for delay_time in self.delay_times_us])

        self.mot_pwr_mon = np.zeros(len(self.mot_frequencies))
        self.mot_pwr_mon_err = np.zeros(len(self.mot_frequencies))
        self.ion_pump_mon = np.zeros(len(self.mot_frequencies))
        self.ion_pump_mon_err = np.zeros(len(self.mot_frequencies))

        self.brightfield_pwr_mon = 0.
        self.brightfield_pwr_mon_err = 0.


    @kernel(flags={"fast-math"})
    def get_mean_and_std(self, arr):# -> TList(TFloat):
        meanN = 0.0
        stdNS = 0.0
        N = len(arr)

        #mean
        for a in arr:
            meanN += a

        mean = meanN/N

        #std
        for a in arr:
            stdNS += (a-mean)**2

        std = np.sqrt(stdNS/N)

        return mean, std

    @kernel
    def run(self):

        self.a
        self.core.wait_until_mu(now_mu())
        self.core.reset()

        ### ttl initialization
        self.ttl2.output()
        self.ttl4.output()
        self.ttl5.output()

        delay(1 * ms)
        self.ttl2.off()
        delay(1 * ms)
        self.ttl4.off()


        #######################
        ### Sampler initialization
        self.core.break_realtime()
        self.sampler0.init()
        self.sampler0.set_gain_mu(0,0)
        self.sampler0.set_gain_mu(1,0)
        self.sampler0.set_gain_mu(2,0)


        #######################
        ### Urukul initialization
        self.core.break_realtime()
        delay(10 * ms)
        self.urukul0_ch3.cpld.init()
        self.urukul0_ch3.init()

        freq = 80 * MHz#self.mot_aom_freq
        amp = self.mot_aom_amp
        attenuation = 0.0 * dB

        self.urukul0_ch3.set_att(attenuation)
        self.urukul0_ch3.set(freq, amplitude = amp)
        self.core.break_realtime()
        delay(50 * ms)
        

        ##########################
        ### Darkfield img
        print("Taking darkfield bg")
        self.ttl5.off() # op beams off
        delay(self.shutter_delay_delta)
        self.urukul0_ch3.sw.off() # MOT beams off
        delay(10 * ms)

        ### take 10 images
        for i in range(self.Nshots):
            self.ttl4.pulse(100*us) #trigger Nshots times
            delay(100*ms)

        self.core.break_realtime()
        delay(100 * ms)
        self.core.wait_until_mu(now_mu())
        

        ##########################
        ### Brightfield img

        ### tune mot light to out of MOT res
        print("Taking brightfield bg")
        print("Tuning MOT beams...")
        self.wlm0.SetPIDCourseNum(1, self.brightfield_frequency+self.mot_aom_freq/2)
        self.core.break_realtime()
        delay(0.5*s)
        self.core.wait_until_mu(now_mu())
        delay(0.1*s)

        ###################
        ### turn on op and mot beams out of res
        delay(0.5*s)
        self.ttl5.on() # op beams on
        delay(self.shutter_delay_delta)
        self.urukul0_ch3.sw.on() # MOT beams on

        # let aom heat up
        delay(self.aom_warmup_delay)

        ########################
        ### first take brightfield pd cal measurement
        n_samples = 50
        smp_arr = [0.0]*n_samples
        delay(1*ms)
        for j in range(n_samples):
            smp = [0.0]*8
            delay(2*ms)
            self.sampler0.sample(smp)
            smp_arr[j] = smp[0]
        delay(1*ms)
        self.brightfield_pwr_mon, self.brightfield_pwr_mon_err = self.get_mean_and_std(smp_arr)
        self.core.break_realtime()
        delay(1*s)

        self.urukul0_ch3.sw.off()
        delay(1*ms)

        ### take 10 images
        for i in range(self.Nshots):
            with parallel:
                self.ttl4.pulse(100*us) #trigger camera
                with sequential:
                #send pulse of MOT light
                    delay(self.camera_trigger_delay)
                    self.urukul0_ch3.sw.on()
                    delay(self.t_exp_us * us)
                    self.urukul0_ch3.sw.off()
            delay(100*ms)

        self.core.wait_until_mu(now_mu())

        delay(10*ms)
        self.urukul0_ch3.sw.on()
        delay(self.aom_warmup_delay)


        ####################
        ##### MAIN loop

        freq_count = 0
        for mot_frequency in self.mot_frequencies:
            
            print("Freq: ", mot_frequency)
            ### Set probe laser frequency
            print("Tuning MOT beams...")
            self.wlm0.SetPIDCourseNum(1, mot_frequency+self.mot_aom_freq/2)
            
            ### wait for laser control to settle before measuring laser freq
            self.core.break_realtime()
            delay(0.5*s)
            self.core.wait_until_mu(now_mu())

            ### record measured laser freq
            self.measured_mot_frequencies[freq_count] = self.wlm0.GetFrequency(1)
            self.measured_op_frequencies[freq_count] = self.wlm0.GetFrequency(3)
            self.core.break_realtime()

            ### taking a pd cal measurement
            n_samples = 50
            mot_smp_arr = [0.0]*n_samples
            ion_pump_smp_arr = [0.0]*n_samples

            delay(1*ms)
            for j in range(n_samples):
                smp = [0.0]*8
                delay(2*ms)
                self.sampler0.sample(smp)
                mot_smp_arr[j] = smp[0]
                ion_pump_smp_arr[j] = smp[1]
            delay(1*ms)

            self.mot_pwr_mon[freq_count], self.mot_pwr_mon_err[freq_count] = self.get_mean_and_std(mot_smp_arr)
            self.ion_pump_mon[freq_count], self.ion_pump_mon_err[freq_count] = self.get_mean_and_std(ion_pump_smp_arr)
            

            ### fresh time cursor for next block
            self.core.break_realtime()
            delay(1*s)

            ######################################
            ### loop through delay times
            for d in self.delay_times_us:

                ####################################
                ### loop through Nshots images to take per detuning
                for i in range(self.Nshots):
                    #self.core.break_realtime()
                    delay(50*ms)
                    print("MOT TOF measurement... delay: ", d, "us")

                    # self.ttl5.on() # op beams on
                    delay(self.shutter_delay_delta)
                    with parallel:
                        self.urukul0_ch3.sw.on() # MOT beams on
                        self.ttl2.off()

                    delay(0.5*s) # load MOT

                    ##### MOT OFF
                    with parallel:
                        self.ttl2.on() ### MOT COILS OFF
                        self.urukul0_ch3.sw.off()

                    #### TOF delay
                    delay(d * us - self.camera_trigger_delay)

                    with parallel:
                        self.ttl4.pulse(100*us) ## trigger camera
                        ### imaging pulse
                        with sequential:
                            delay(self.camera_trigger_delay)
                            self.urukul0_ch3.sw.on()
                            delay(self.t_exp_us * us)
                            self.urukul0_ch3.sw.off()

                    self.core.wait_until_mu(now_mu())

                self.core.wait_until_mu(now_mu())

            
            self.core.wait_until_mu(now_mu())
            print("Done with current freq.")
            freq_count += 1

        self.core.wait_until_mu(now_mu())
        delay(0.25 * s)
        self.ttl2.off()
        delay(10*ms)
        self.urukul0_ch3.sw.on()
        print("Done.")

    def analyze(self):
        print("Saving...")
        self.set_dataset("measured_mot_frequencies", self.measured_mot_frequencies)
        self.set_dataset("measured_op_frequencies", self.measured_op_frequencies)
        self.set_dataset("mot_pwr_mon", self.mot_pwr_mon)
        self.set_dataset("mot_pwr_mon_err", self.mot_pwr_mon_err)
        self.set_dataset("ion_pump_mon", self.ion_pump_mon)
        self.set_dataset("ion_pump_mon_err", self.ion_pump_mon_err)
        self.set_dataset("brightfield_pwr_mon", self.brightfield_pwr_mon)
        self.set_dataset("brightfield_pwr_mon_err", self.brightfield_pwr_mon_err)
        print("Done.")