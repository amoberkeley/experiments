from artiq.experiment import *
import numpy as np

class NarrowLineSearch(EnvExperiment):
    
    def build(self):
        self.setattr_device("core")
        self.setattr_device("ttl4") # camera trigger
        self.setattr_device("thorcam1")
        self.setattr_device("wlm0_old")
        #self.setattr_argument("probe_frequencies", Scannable(global_max=400, ndecimals=6, default=[RangeScan(300.807945 - 0.0002 - 0.001, 300.807945 - 0.0002 + 0.001, 20)]))
        self.wlm_channel = 6
        self.loop_count = 0
        self.a = 1

    def prepare(self):

        self.measured_frequencies_before = np.zeros(1000)
        self.measured_frequencies_after = np.zeros(1000)
        # self.measured_frequencies_after = np.zeros(len(self.probe_frequencies))

        # self.set_dataset("measured_frequencies_before", [])
        # self.set_dataset("measured_frequencies_after", [])
        # self.set_dataset("counts", [], broadcast=True)

    @kernel(flags={"fast-math"})
    def get_mean_and_std(self, arr):
        meanN = 0.0
        stdNS = 0.0
        N = len(arr)

        #mean
        for a in arr:
            meanN += a

        mean = meanN/N

        #std
        for a in arr:
            stdNS += (a-mean)**2

        std = np.sqrt(stdNS/N)

        return mean, std


    @kernel
    def run(self):

        #######################
        ### core initialization
        self.core.wait_until_mu(now_mu())
        self.core.reset() #Clear RTIO FIFOs, release RTIO PHY reset, and set the time cursor at the current value of the hardware RTIO counter plus a margin of 125000 machine units.
        self.a

        #######################
        ### ttl initialization
        self.ttl4.output()


        #######################
        ### Thorcam initialization
        self.thorcam1.connect()
        self.thorcam1.initialize(self, exposure_time_us=1000, image_poll_timeout=2000, roi=[750,340,850,440], wavelength_nm=498, gain=200)
        self.thorcam1.arm(1000)

        #######################
        ### WLM initialization
        self.wlm0_old.connect()
        #self.wlm0.SetPIDCourseNum(self.wlm_channel, self.probe_frequencies[0])
        ##rint(self.wlm0.GetRegulationStatus())
        ##self.wlm0.SetRegulationStatus(True)

        print("Here")
        ####################################
        ### loop through probe frequencies
        frequency = 1.0
        self.core.break_realtime()
        while frequency<288.269813:

            delay(0.05*s)
            self.core.wait_until_mu(now_mu())

            self.measured_frequencies_before[self.loop_count] = self.wlm0_old.GetFrequency(6)
            #self.append_to_dataset("measured_frequencies_before", self.wlm0_old.GetFrequency(6))
            self.core.break_realtime()

            ### trigger camera
            self.ttl4.pulse(100*us)

            ### wait until the image has been taken
            delay(0.05*s)
            self.core.wait_until_mu(now_mu())

            # ### get the counts
            # self.thorcam1.poll_for_frame_and_append_counts_to_dataset(self, "counts")

            frequency = self.wlm0_old.GetFrequency(6)
            print("Frequency: ", frequency)
            self.measured_frequencies_after[self.loop_count] = frequency


            self.loop_count += 1

                
        print("Loop broken.")

           

    def analyze(self):
        print("Saving...")

        self.thorcam1.poll_for_frame_and_append_to_buffer('signal', Nframes=self.loop_count)
        self.thorcam1.show_img_buffer('signal')
        
        signal, signal_err = self.thorcam1.get_buffer_and_buffer_errs('signal')
        signal_frame_numbers = np.array(self.thorcam1.frame_count_buffer_dict['signal_frame_count'])

        self.measured_frequencies_before = np.take(self.measured_frequencies_before, signal_frame_numbers-1)
        self.measured_frequencies_after = np.take(self.measured_frequencies_after, signal_frame_numbers-1)


        self.set_dataset("measured_frequencies_before", self.measured_frequencies_before, broadcast=True)
        self.set_dataset("measured_frequencies_after", self.measured_frequencies_after, broadcast=True)


        counts = np.sum(signal, axis=(-1,-2))
        print("array lengths")
        print(len(self.measured_frequencies_before[self.measured_frequencies_before!=0]))
        print(len(counts))
        count_errs = np.sqrt(np.sum(signal_err**2, axis=(-1,-2)))
        self.set_dataset("counts", counts, broadcast=True)
        self.set_dataset("count_errs", count_errs)

        print("Done.")