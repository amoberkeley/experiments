from artiq.experiment import *
from mini_mot import absorption_beam, mot_beams, ext_mot_coils, mot_coils
# from experiments import global_parameters
from global_params import mini_mot_global_params as global_parameters
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid

class FluorExtCoilMOTParamScan(EnvExperiment):
    
    def build(self):
        
        ## Device building
        self.setattr_device("core")
        # self.setattr_device("zotino0")
        # self.setattr_device("ttl2") # coil control
        self.setattr_device("ttl4") # mot camera 4/30/2024
        self.setattr_device("ttl5") # srs optical pumping shutter 4/30/2024
        self.setattr_device("wlm0") # wavemeter
        self.setattr_device("sampler0") # MOT power monitor, ion pump current monitor, etc
        self.setattr_device("thorcam1")
        self.mot_beams = mot_beams.MOTBeams(self, aom_freq = 200 * MHz) # urukul0_2, ttl_6
        self.inner_mot_coils = mot_coils.MOTCoils(self, default_curr = 0.0) #zotino0, ttl2
        self.mot_coils = ext_mot_coils.ExtMOTCoils(self) # zotino1
        self.absorption_beam = absorption_beam.AbsorptionBeam(self) # urukul0_0, urukul0_1, ttl_3

        ## Saved/ tunable attribute building
        ## Scannable attributes for calibration
        self.setattr_argument("mot_detunings_gamma", Scannable(global_min=-15, global_max=5,
                                                                ndecimals=3,
                                                                default=[RangeScan(-6, -1, 6)]))
        self.setattr_argument("mot_aom_amps_V", Scannable(global_min=0.0, global_max=1.0,
                                                        ndecimals=3,
                                                        default=[RangeScan(0.06, 0.18, 5)]))
        self.setattr_argument("mot_currents_amps", Scannable(global_min=0.0, global_max=240,
                                                            ndecimals=3,
                                                            default=[RangeScan(20, 100, 4)]))

        ## None scanned attributes - tunable to get good signals
        self.setattr_argument("Nshots", NumberValue(default=10, ndecimals=0, step=1))
        self.setattr_argument('isotope', NumberValue(default=48, ndecimals=0, step=1))

        self.setattr_argument("imag_detuning_gamma", NumberValue(default=0, ndecimals=2, step=0.5))
        self.setattr_argument("imag_aom_amp_V", NumberValue(default=0.18, ndecimals=3, step=0.01))
        self.setattr_argument("t_exp_us", NumberValue(default=100, ndecimals=0, step=1))
        # self.setattr_argument("camera_gain_dB", NumberValue(default=20, ndecimals=0, step=1))

        self.setattr_argument("imag_pulse_delay_ms", NumberValue(ndecimals=3, default=0.1))
        self.setattr_argument("Nshots", NumberValue(default=5, ndecimals=0, step=1))
        self.setattr_argument('camera_gain_dB', NumberValue(default=20, ndecimals=0, step=1))
        self.setattr_argument('camera_t_exp_ms', NumberValue(default=2, ndecimals=2, step=1))
        self.setattr_argument('cam_roi_x_0', NumberValue(default=704, ndecimals=0, step=1))
        self.setattr_argument('cam_roi_y_0', NumberValue(default=550, ndecimals=0, step=1))
        self.setattr_argument('cam_roi_w_x', NumberValue(default=80, ndecimals=0, step=1))
        self.setattr_argument('cam_roi_w_y', NumberValue(default=80, ndecimals=0, step=1))
        self.setattr_argument('isotope', NumberValue(default=48, ndecimals=0, step=1))

        ## Fixed attributes, non-saved

        self.camera_trigger_delay = 20 * us
        self.mot_loading_time = 2 * s
        self.aom_thermalization_time = 4 * s
        self.num_power_samples = 50
        self.mot_sampler_channel = 0
        self.a = 1 # random thing that must be set here

    def prepare(self):

        self.roi = [int(self.cam_roi_x_0), int(self.cam_roi_y_0),
                    int(self.cam_roi_x_0 + self.cam_roi_w_x),
                    int(self.cam_roi_y_0 + self.cam_roi_w_y)]

        # pull the atomic frequencies from global params
        self.cooling_freq = global_parameters.cooling_line_freq[str(self.isotope)]
        self.op_freq = global_parameters.op_line_freq[str(self.isotope)]

        # calculate the detuning that the lasers should be locked to on the wavemeter
        self.mot_dets = np.array([d for d in self.mot_detunings_gamma]) * global_parameters.cooling_gamma_linear
        self.vexlum_freqs = (self.cooling_freq # set Vexlum freq where there will be max AOM efficiency at MOT dets
                             + (self.mot_dets - 2 * self.mot_beams.ch2_center_freq) / 10**12)
        self.imag_aom_freqs = (self.mot_beams.ch2_center_freq +
                                    (self.imag_detuning_gamma * global_parameters.cooling_gamma_linear
                                     - self.mot_dets) / 2)

        # set up other scans
        self.mot_currents = np.array([i for i in self.mot_currents_amps])
        self.mot_aom_amps = np.array([v for v in self.mot_aom_amps_V])

        # set up arrays to save measured data into, fitting it into properly sized arrays
        self.n_imags = len(self.mot_dets) * len(self.mot_aom_amps) * len(self.mot_currents) * self.Nshots
        self.data_shape = (len(self.mot_dets), len(self.mot_aom_amps), len(self.mot_currents), self.Nshots)
        self.meas_mot_power = np.zeros(self.data_shape)
        self.meas_mot_power_err = np.zeros(self.data_shape)
        # self.meas_imag_power = np.zeros(self.data_shape)
        # self.meas_imag_power_err = np.zeros(self.data_shape)

        self.mot_aom_freq = self.mot_beams.ch2_center_freq

    @kernel(flags={"fast-math"})
    def get_mean_and_std(self, arr):# -> TList(TFloat):
        meanN = 0.0
        stdNS = 0.0
        N = len(arr)

        #mean
        for a in arr:
            meanN += a

        mean = meanN/N

        #std
        for a in arr:
            stdNS += (a-mean)**2

        std = np.sqrt(stdNS/N)

        return mean, std

    @kernel
    def run(self):

        self.a # random thing that must be called here
        self.core.wait_until_mu(now_mu())
        self.core.reset()

        ######## INITIALIZATIONS ########

        ### wrapped initializations
        self.mot_beams.init()
        self.mot_coils.init()
        self.absorption_beam.init()
        self.inner_mot_coils.init()
        self.inner_mot_coils.turn_off()

        ### ttl initialization
        # self.ttl2.output()
        self.ttl4.output()
        self.ttl5.output()

        # delay(1 * ms)
        # self.ttl2.off() # MOT coils on
        delay(1 * ms)
        self.ttl4.off() # Camera TTL low
        delay(1 * ms)
        self.ttl5.on() # OP shutter open

        ### Sampler initialization
        self.core.break_realtime()
        self.sampler0.init()
        self.sampler0.set_gain_mu(0,0) # check this!
        self.sampler0.set_gain_mu(1,0) # check this!
        self.sampler0.set_gain_mu(2,0) # check this!
        self.sampler0.set_gain_mu(3,0) # check this!

        ### thorcam initialization
        self.thorcam1.connect()
        self.thorcam1.initialize(experiment=self,
                                exposure_time_us = self.camera_t_exp_ms * 1000,
                                operation_mode = 1,
                                frames_per_trigger_zero_for_unlimited = 1,
                                image_poll_timeout_ms = 3000,
                                gain = self.camera_gain_dB * 10,
                                roi = self.roi,
                                trigger_polarity = 0, # 0 for rising edge 1 for falling edge
                                wavelength_nm = 498,
                                )
        self.core.break_realtime()

        ##########################
        ### MAIN LOOP

        img_count = 0
        missing_frame_count = 0
        shot_count = 0

        ### Loop thorugh MOT freqs
        for i in range(len(self.vexlum_freqs)):
            det = self.mot_dets[i]
            ### Tune wavemeter for vexlum
            print("Tuning MOT beams...")
            self.wlm0.SetPIDCourseNum(3, self.op_freq)
            self.wlm0.SetPIDCourseNum(5, self.vexlum_freqs[i])
            self.core.break_realtime()
            delay(0.5 * s)
            # print('aom frequency:', aom_freq / MHz, ' MHz')

            ### Loop through MOT powers
            for j in range(len(self.mot_aom_amps)):
                aom_amp = self.mot_aom_amps[j]
                self.mot_beams.set_aom_amp(aom_amp)
                # delay(self.aom_thermalization_time)
                
                ### Loop through MOT currents times
                for k in range(len(self.mot_currents)):
                    print(100 * float(img_count) / self.n_imags, ' percent done')
                    mot_curr = self.mot_currents[k]
                    self.mot_coils.set_current(mot_curr)
                    delay(1 * ms)
                    # print('t_exp:', t_exp / ms, ' ms')

                    ### Take Nshots images
                    for m in range(self.Nshots):

                        ### Add a check for missing frames - we only will loop a max of 10 times per frame
                        missing_frame_check = 0
                        while missing_frame_check == 0:
                            shot_count += 1
                            missing_sig_check = 0
                            missing_bright_check = 0
                            missing_dark_check = 0
                            self.core.break_realtime()

                            ### Arm the camera
                            self.thorcam1.arm(3)
                            self.core.break_realtime()
                            delay(0.5 * ms)

                            ### Load the MOT
                            self.mot_coils.turn_on()
                            delay(0.5 * ms)
                            self.ttl5.off() # OP shutter open
                            delay(0.5 * ms)
                            self.mot_beams.turn_on()
                            delay(0.5 * ms)
                            self.absorption_beam.turn_off()
                            delay(0.5 * ms)
                            with parallel:
                                delay(self.mot_loading_time)
                                ### MOT power measurement
                                with sequential:
                                    mot_smp_arr = [0.0] * self.num_power_samples
                                    delay(0.5 * ms)
                                    for n in range(self.num_power_samples):
                                        smp = [0.0] * 8
                                        delay(0.5 * ms)
                                        self.sampler0.sample(smp)
                                        mot_smp_arr[n] = smp[self.mot_sampler_channel]
                                    delay(0.5 * ms)
                                    self.meas_mot_power[i, j, k, m], self.meas_mot_power_err[i, j, k, m] = self.get_mean_and_std(mot_smp_arr)

                            ### Take a signal image
                            with parallel:
                                # after appropriate delay, send imaging pulse to camera
                                with sequential:
                                    delay(self.imag_pulse_delay_ms * ms - self.camera_trigger_delay)
                                    self.ttl4.pulse(100 * us)
                                self.mot_coils.turn_off() # MOT coils off
                                self.mot_beams.fluor_imag_pulse(self.t_exp_us * us,
                                                                self.imag_pulse_delay_ms * ms,
                                                                self.imag_aom_freqs[i],
                                                                self.imag_aom_amp_V)
                            delay(0.5 * ms)

                            ### Take a brightfield image
                            with parallel:
                                smp_arr = [0.0] * self.num_power_samples
                                smp_arr_2 = [[0.0] * 8] * self.num_power_samples
                                delay(50 * ms) # let atoms fly away
                            with parallel:
                                # after appropriate delay, send imaging pulse to camera
                                with sequential:
                                    delay(self.mot_beams.pulse_delay + self.imag_pulse_delay_ms * ms - self.camera_trigger_delay)
                                    self.ttl4.pulse(100 * us)
                                self.mot_beams.fluor_imag_pulse(self.t_exp_us * us,
                                                                self.imag_pulse_delay_ms * ms,
                                                                self.imag_aom_freqs[i],
                                                                self.imag_aom_amp_V)
                            ### Take a darkfield image
                            self.mot_beams.turn_off()
                            delay(.5 * ms)
                            self.absorption_beam.turn_off()
                            delay(50 * ms)
                            self.ttl4.pulse(100 * us)
                            self.mot_beams.set_aom(self.mot_beams.ch2_center_freq, aom_amp)

                            # print("Polling camera for images...")
                            self.core.break_realtime()
                            delay(0.5 * ms)
                            self.core.wait_until_mu(now_mu())
                            self.thorcam1.poll_for_frame_and_append_to_buffer(['signal', 'bright', 'dark'], 3)
                            self.core.break_realtime()
                            self.thorcam1.disarm()
                            self.core.break_realtime()

                            ### handle missing frame by comparing against the expected number of images at this point
                            if self.thorcam1.get_buffer_len('signal') < img_count + 1:
                                missing_sig_check = 1
                                # print('missing sig frame')
                            if self.thorcam1.get_buffer_len('bright') < img_count + 1:
                                missing_bright_check = 1
                                # print('missing bright frame')
                            if self.thorcam1.get_buffer_len('dark') < img_count + 1:
                                missing_dark_check = 1
                                # print('missing dark frame')
                            if ((missing_sig_check == 0) and
                                (missing_bright_check == 0) and
                                (missing_dark_check == 0)):
                                missing_frame_check = 1
                            if missing_frame_check == 0:
                                print('missing frame caught, retaking image')
                                missing_frame_count += 1
                                if missing_sig_check == 0:
                                    # print('popping extra sig frame')
                                    self.thorcam1.pop_buffer('signal')
                                if missing_bright_check == 0:
                                    # print('popping extra bright frame')
                                    self.thorcam1.pop_buffer('bright')
                                if missing_dark_check == 0:
                                    # print('popping extra dark frame')
                                    self.thorcam1.pop_buffer('dark')
                            self.core.break_realtime()

                        img_count += 1
                        # print(img_count,'images taken out of', self.n_imags)

        self.core.break_realtime()
        self.ttl5.on() # op beams on
        delay(0.5 * ms)
        self.mot_beams.set_aom(self.mot_beams.ch2_center_freq, self.mot_beams.ch2_amp)
        delay(0.5 * ms)
        self.mot_beams.turn_on() # MOT beams on
        delay(0.5 * ms)
        self.mot_coils.turn_on() # MOT coils on
        delay(0.5 * ms)
        self.absorption_beam.turn_off()
        print("Done with run stage.")
        print('Missing frame count: ', missing_frame_count)
        print('Missing frame frac: ', float(missing_frame_count) / shot_count)

    def analyze(self):
        print("Saving...")

        self.thorcam1.save_all(self)
        # print(self.thorcam1.get_img_shape())
        img_shape = self.thorcam1.get_img_shape()
        print(img_shape)
        data_array_shape = self.data_shape + (img_shape[0], img_shape[1])
        # print(data_array_shape)
        print('reading signals')
        signal, signal_err = self.thorcam1.get_buffer_and_buffer_errs('signal')
        # print(np.array(signal).shape)
        print('reading brights')
        bright, bright_err = self.thorcam1.get_buffer_and_buffer_errs('bright')
        print('reading darks')
        dark, dark_err = self.thorcam1.get_buffer_and_buffer_errs('dark')
        print('reshaping data')
        signal = np.reshape(np.array(signal), data_array_shape)
        print(signal.shape)
        signal_err = np.reshape(np.array(signal_err), data_array_shape)
        bright = np.reshape(np.array(bright), data_array_shape)
        bright_err = np.reshape(np.array(bright_err), data_array_shape)
        dark = np.reshape(np.array(dark), data_array_shape)
        dark_err = np.reshape(np.array(dark_err), data_array_shape)

        n_points = int(self.n_imags / self.Nshots)

        sig_imag_for_plotting = np.reshape(np.mean(signal, axis=-3),
                                            (n_points,
                                            data_array_shape[-2],
                                            data_array_shape[-1])
                                            )
        bright_imag_for_plotting = np.reshape(np.mean(bright, axis=-3),
                                            (n_points,
                                            data_array_shape[-2],
                                            data_array_shape[-1])
                                            )

        fig = plt.figure(figsize=(2 * n_points, 2 ))
        grid = ImageGrid(fig, 111, cbar_mode = 'single', share_all = True,
                        nrows_ncols = (1, n_points),
                        )
        clim = (0, 1024)
        for ax, im in zip(grid, bright_imag_for_plotting): 
            im = ax.imshow(im, cmap='GnBu_r', clim=clim)
        ax.cax.colorbar(im)
        fig.suptitle('Bright background signal')
        plt.tight_layout()
        plt.show()

        fig = plt.figure(figsize=(2 * n_points, 2))
        grid = ImageGrid(fig, 111, cbar_mode = 'single', share_all = True,
                        nrows_ncols = (1, n_points),
                        )
        clim = (0, 1024)
        for ax, im in zip(grid, sig_imag_for_plotting): 
            im = ax.imshow(im, cmap='GnBu_r', clim=clim)
        ax.cax.colorbar(im)
        fig.suptitle('Signal images')
        plt.tight_layout()
        plt.show()

        fig = plt.figure(figsize=(2 * n_points, 2 ))
        grid = ImageGrid(fig, 111, cbar_mode = 'single', share_all = True,
                        nrows_ncols = (1, n_points),
                        )
        for ax, im in zip(grid, sig_imag_for_plotting - bright_imag_for_plotting): 
            im = ax.imshow(im, cmap='GnBu_r', vmin=0, vmax=250)
        ax.cax.colorbar(im)
        fig.suptitle('Signal - bright images')
        plt.tight_layout()
        plt.show()

        # self.set_dataset("sig_minus_bright", signal - bright)
        bg_subtracted_imgs = signal - bright
        counts = np.sum((signal - bright), axis = (-1,-2))
        #offset = np.sum(counts[70:-1,])
        counts_mean = np.mean(counts, axis=-1)
        counts_std = np.std(counts, axis=-1)

        print('plotting data')
        fig, ax = plt.subplots(figsize=(6,4))

        mec_list = ['darkslategrey', 'maroon', 'indigo', 'black', 'darkgoldenrod', 'darkolivegreen']
        mfc_list = ['darkturquoise', 'indianred', 'darkorchid', 'grey', 'gold', 'greenyellow']
        markers = ['o','s', 'D','^', '*', 'p']
        # for i, amp in enumerate(self.mot_aom_amps_V):
        #     for j, curr in enumerate(self.mot_currents_amps):
        i = 0
        j = 0


        print(self.mot_currents_amps)
        print(counts_mean.shape)
        ax.errorbar([i for i in self.mot_currents_amps],
                    counts_mean[0,0,:],
                    # xerr =,
                    yerr = counts_std[0,0,:],# / np.max(fluor_counts_mean_zeroed),
                    ls='', marker=markers[0],
                    ecolor=mec_list[0],
                    mec=mec_list[0],
                    mfc=mfc_list[0])#,
                    # label = 'AOM amp=%.2fV,IMOT=%.2fA'%(self.mot_aom_amps_V[0], curr))
        # ax.legend()
        # ax.set_ylim(-0.05, 1.05)
        ax.set_xlabel(r'$I_{\mathrm{ext}}$ (s)')
        ax.set_ylabel(r'Fluor counts - min(Fluor counts) (arb.)')

        plt.tight_layout()
        plt.show()

        # self.set_dataset("mot_pwr_mon", self.mot_pwr_mon)
        # self.set_dataset("mot_pwr_mon_err", self.mot_pwr_mon_err)
        # self.set_dataset("brightfield_pwr_mon", self.brightfield_pwr_mon)
        # self.set_dataset("brightfield_pwr_mon_err", self.brightfield_pwr_mon_err)
        print("Done with analyze section")