import sys
sys.path.insert(1, '../../analysis-tools/')
from fit_functions import eliptical_gaussian

from scipy.optimize import curve_fit
import numpy as np
import matplotlib.pyplot as plt
import h5py

filename="/home/eurydice/artiq-master/results/2024-09-06/17/000008721-AbsorpParamScan.h5"

f = h5py.File(filename, 'r')

for key in f.keys():
    print('key:')
    print(key) #Names of the root level object names in HDF5 file - can be groups or datasets.
    print('type:')
    print(type(f[key])) # get the object type: usually group or dataset
    print("=====")
#Get the HDF5 group; key needs to be a group name from above
group = f['datasets']

# #Checkout what keys are inside that group.
for key in group.keys():
    print(key)


# This assumes group[some_key_inside_the_group] is a dataset, 
# and returns a np.array:
# data = group[some_key_inside_the_group][()]
#Do whatever you want with data

#After you are done
f.close()