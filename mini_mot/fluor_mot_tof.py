from artiq.experiment import *
from mini_mot import absorption_beam, mot_beams, mot_coils 
# from experiments import global_parameters
from global_params import mini_mot_global_params as global_parameters
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid
from scipy import constants as cnst

class FluorMOTTOF(EnvExperiment):
    ### Meausres MOT temperature via TOF fluorescence imaging - this is only done at a certain stage
    
    ### I have not implemented a method to take MOT power calibrations during the absorption process - seems to throw errors
    
    def build(self):

        ## Device building
        self.setattr_device("core")
        # self.setattr_device("ttl2") # coil control
        self.setattr_device("ttl4") # mot camera 4/30/2024
        self.setattr_device("ttl5") # srs optical pumping shutter 4/30/2024
        self.setattr_device("wlm0") # wavemeter
        self.setattr_device("sampler0") # MOT power monitor
        self.setattr_device("thorcam1")
        self.absorption_beam = absorption_beam.AbsorptionBeam(self) # urukul0_0, urukul0_1, ttl_3
        self.mot_beams = mot_beams.MOTBeams(self) # urukul0_3, ttl_6
        self.mot_coils = mot_coils.MOTCoils(self) # ttl_2,  zotino_0

        ## Saved/ tunable attribute building
        ## Scannable attributes for calibration

        # image timing/ beam params
        self.setattr_argument("imag_delay_times_ms", Scannable(global_min=1,
                                                                global_max=100,
                                                                ndecimals=3,
                                                                default=[RangeScan(1., 10., 10)]))
        self.setattr_argument("imag_t_exps_ms", Scannable(global_min=0.05,
                                                            global_max=3,
                                                            ndecimals=3,
                                                            default=[NoScan(1.0)]))
        self.setattr_argument("imag_aom_amps_V", Scannable(global_min=0.00,
                                                            global_max=0.18,
                                                            ndecimals=4,
                                                            default=[NoScan(0.18)]))
        self.setattr_argument("imag_detunings_gamma", Scannable(global_min=-10.0,
                                                                global_max=10.0,
                                                                ndecimals=2,
                                                                default=[NoScan(0.0)]))


        ## Single attributes, not scanned
        
        # MOT params
        self.setattr_argument("mot_current_A", NumberValue(default=4., ndecimals=5, step=0.1))
        self.setattr_argument("mot_detuning_gamma", NumberValue(default=-3., ndecimals=5, step=0.1))
        self.setattr_argument("mot_aom_amp_V", NumberValue(default=0.18, ndecimals=5, step=0.1))
        self.setattr_argument('mot_loading_time_s', NumberValue(default=0.5, ndecimals=2, step=0.1))

        # 2nd stage MOT params
        self.setattr_argument("sec_stage_mot_current_A", NumberValue(default=4., ndecimals=5, step=0.1))
        self.setattr_argument("sec_stage_mot_detuning_gamma", NumberValue(default=-3., ndecimals=5, step=0.1))
        self.setattr_argument("sec_stage_mot_aom_amp_V", NumberValue(default=0.18, ndecimals=5, step=0.1))
        self.setattr_argument('sec_stage_mot_loading_time_ms', NumberValue(default=10, ndecimals=2, step=1))
        self.setattr_argument("sec_stage_mot_bool", BooleanValue(default=False))
        
        # camera params
        self.setattr_argument('camera_gain_dB', NumberValue(default=5, ndecimals=0, step=1))
        self.setattr_argument('camera_t_exp_ms', NumberValue(default=3, ndecimals=3, step=1))
        self.setattr_argument('cam_roi_x_0', NumberValue(default=704, ndecimals=0, step=1))
        self.setattr_argument('cam_roi_y_0', NumberValue(default=550, ndecimals=0, step=1))
        self.setattr_argument('cam_roi_w_x', NumberValue(default=80, ndecimals=0, step=1))
        self.setattr_argument('cam_roi_w_y', NumberValue(default=80, ndecimals=0, step=1))

        # other params
        self.setattr_argument("Nshots", NumberValue(default=5, ndecimals=0, step=1))
        self.setattr_argument('isotope', NumberValue(default=48, ndecimals=0, step=1))
        self.setattr_argument('num_power_samples', NumberValue(default=50, ndecimals=0, step=1))

        ## Other attributes, non-saved
        self.a = 1 # random thing that must be set here
        self.camera_trigger_delay = 20 * us
        self.op_shutter_delay = 3*ms
        self.mot_shutter_delay = 2*ms
        self.op_shutter_on_delay = 3*ms
        self.mot_shutter_on_delay = 1.5*ms
        self.shutter_delay_delta = self.op_shutter_delay - self.mot_shutter_delay
        self.mot_sampler_channel = 0

    def prepare(self):

        ## set up scans
        self.imag_delay_times = np.array([t * ms for t in self.imag_delay_times_ms])
        self.imag_t_exps = np.array([t * ms for t in self.imag_t_exps_ms])
        self.imag_aom_amps = np.array([v for v in self.imag_aom_amps_V])
        self.imag_detunings_gamma = np.array([d for d in self.imag_detunings_gamma])

        ## set up camera ROI
        self.roi = [int(self.cam_roi_x_0), int(self.cam_roi_y_0),
                    int(self.cam_roi_x_0 + self.cam_roi_w_x),
                    int(self.cam_roi_y_0 + self.cam_roi_w_y)]

        # pull the atomic frequencies from global params
        self.cooling_freq = global_parameters.cooling_line_freq[str(self.isotope)]
        self.op_freq = global_parameters.op_line_freq[str(self.isotope)]

        # calculate the detuning that the lasers should be locked to on the wavemeter
        self.vexlum_freq = (self.cooling_freq # set Vexlum freq where there will be max AOM efficiency at MOT dets
                            + (self.mot_detuning_gamma * global_parameters.cooling_gamma_linear
                                - 2 * self.mot_beams.ch2_center_freq) / 10**12) 
        self.sec_stage_mot_aom_freq = (((self.sec_stage_mot_detuning_gamma - self.mot_detuning_gamma) *
                                        (global_parameters.cooling_gamma_linear / 2)) + 
                                        self.mot_beams.ch2_center_freq)
        self.imag_aom_freqs = (((self.imag_detunings_gamma - self.mot_detuning_gamma) *
                                (global_parameters.cooling_gamma_linear / 2)) + 
                                self.mot_beams.ch2_center_freq)

        # prep data shape and arrays
        self.data_shape = (len(self.imag_delay_times), self.Nshots)
        self.n_imags = len(self.imag_delay_times) * self.Nshots

        # prep power monitor arrays
        self.meas_sig_power = np.zeros(self.data_shape)
        self.meas_sig_power_err = np.zeros(self.data_shape)
        self.meas_bright_power = np.zeros(self.data_shape)
        self.meas_bright_power_err = np.zeros(self.data_shape)

    @kernel(flags={"fast-math"})
    def get_mean_and_std(self, arr):# -> TList(TFloat):
        meanN = 0.0
        stdNS = 0.0
        N = len(arr)

        #mean
        for a in arr:
            meanN += a

        mean = meanN/N

        #std
        for a in arr:
            stdNS += (a-mean)**2

        std = np.sqrt(stdNS/N)

        return mean, std

    @kernel
    def run(self):

        self.a
        self.core.wait_until_mu(now_mu())
        self.core.reset()

        ######## INITIALIZATIONS ########

        ### wrapped initializations
        self.mot_beams.init()
        self.mot_coils.init()
        self.absorption_beam.init()

        ### ttl initialization
        self.ttl4.output()
        self.ttl5.output()
        delay(0.5 * ms)
        self.ttl4.off() # Camera TTL low
        delay(0.5 * ms)
        self.ttl5.on() # OP shutter open

        ### Sampler initialization
        self.core.break_realtime()
        self.sampler0.init()
        self.sampler0.set_gain_mu(0,0) # check this!
        self.sampler0.set_gain_mu(1,0) # check this!
        self.sampler0.set_gain_mu(2,0) # check this!
        self.sampler0.set_gain_mu(3,0) # check this!

        ### thorcam initialization
        self.thorcam1.connect()
        self.thorcam1.initialize(experiment=self,
                                exposure_time_us = self.camera_t_exp_ms * 1000,
                                operation_mode = 1,
                                frames_per_trigger_zero_for_unlimited = 1,
                                image_poll_timeout_ms = 1500,
                                gain = self.camera_gain_dB * 10,
                                roi = self.roi,
                                trigger_polarity = 0, # 0 for rising edge 1 for falling edge
                                wavelength_nm = 498,
                                )
        self.core.break_realtime()

        ##########################
        ### MAIN LOOP

        img_count = 0
        missing_frame_count = 0
        shot_count = 0

        print("Tuning MOT beams...")
        self.wlm0.SetPIDCourseNum(3, self.op_freq)
        self.wlm0.SetPIDCourseNum(5, self.vexlum_freq)
        self.core.break_realtime()
        delay(0.5 * s)

        ### Loop through imaging delay times
        for i in range(len(self.imag_delay_times)):
            print(100 * float(img_count) / self.n_imags, ' percent done')
            t_delay = self.imag_delay_times[i]
            
            # if other imaging params are scanned with the delay times, set those
            t_exp = self.imag_t_exps[0]
            imag_aom_amp = self.imag_aom_amps[0]
            imag_aom_freq = self.imag_aom_freqs[0]
            if len(self.imag_t_exps) == len(self.imag_delay_times):
                t_exp = self.imag_t_exps[i]
            if len(self.imag_aom_amps) == len(self.imag_delay_times):
                imag_aom_amp = self.imag_aom_amps[i]
            if len(self.imag_aom_freqs) == len(self.imag_delay_times):
                imag_aom_freq = self.imag_aom_freqs[i]

            ### take N shots for each delay time
            for j in range(self.Nshots):

            ### Add a check for missing frames - we only will loop a max of 10 times per frame
                missing_frame_check = 0
                while missing_frame_check == 0:
                    shot_count += 1
                    missing_sig_check = 0
                    missing_bright_check = 0
                    missing_dark_check = 0
                    self.core.break_realtime()

                    ### Arm the camera
                    self.thorcam1.arm(3)
                    self.core.break_realtime()
                    delay(0.2 * ms)

                    ### Load the MOT
                    self.mot_coils.set_current(self.mot_current_A)
                    delay(0.2 * ms)
                    self.mot_coils.turn_on()
                    delay(0.2 * ms)
                    self.ttl5.on() # OP shutter open
                    delay(0.2 * ms)
                    self.mot_beams.turn_on()
                    delay(0.2 * ms)
                    self.absorption_beam.turn_off()
                    with parallel:
                        smp_arr = [0.0] * self.num_power_samples
                        smp_arr_2 = [[0.0] * 8] * self.num_power_samples
                        delay(self.mot_loading_time_s)

                    ### if running a second MOT stage, do that
                    if self.sec_stage_mot_bool:
                        with parallel:
                            self.mot_beams.set_aom(self.sec_stage_mot_aom_freq,
                                                    self.sec_stage_mot_aom_amp_V)
                            self.mot_coils.set_current(self.sec_stage_mot_current_A)
                            delay(self.sec_stage_mot_loading_time_ms * ms)

                    ### Take a signal image
                    with parallel:
                        # after appropriate delay, send imaging pulse to camera
                        with sequential:
                            delay(t_delay - self.camera_trigger_delay)
                            self.ttl4.pulse(100 * us)
                        self.mot_coils.turn_off() # MOT coils off
                        self.mot_beams.fluor_imag_pulse(t_exp,
                                                        t_delay,
                                                        imag_aom_freq,
                                                        imag_aom_amp)
                        # with sequential: # Power calibration stage
                        #     delay(t_delay)
                        #     delay(t_exp / 10) # ensure samples are taken during pulse
                        #     if self.num_power_samples == 1:
                        #         self.sampler0.sample(smp_arr_2[0])
                        #     else:
                        #         for n in range(self.num_power_samples):
                        #             smp = [0.0] * 8
                        #             self.sampler0.sample(smp)
                        #             if n < self.num_power_samples - 1:
                        #                 delay(0.7 * t_exp / (self.num_power_samples - 1))
                        #             smp_arr_2[n] = smp[self.mot_sampler_channel]
                        #     delay(t_exp / 10)
                    delay(0.5 * ms)
                    # for n in range(self.num_power_samples):
                    #     smp_arr[n] = smp_arr_2[n][self.mot_sampler_channel]
                    # self.meas_sig_power[i,j], self.meas_sig_power_err[i,j] = self.get_mean_and_std(smp_arr)
                    # self.core.break_realtime()


                    ### Take a brightfield image
                    with parallel:
                        smp_arr = [0.0] * self.num_power_samples
                        smp_arr_2 = [[0.0] * 8] * self.num_power_samples
                        delay(50 * ms) # let atoms fly away
                    with parallel:
                        # after appropriate delay, send imaging pulse to camera
                        with sequential:
                            delay(self.mot_beams.pulse_delay + t_delay - self.camera_trigger_delay)
                            self.ttl4.pulse(100 * us)
                        self.mot_beams.fluor_imag_pulse(t_exp,
                                                        self.mot_beams.pulse_delay + t_delay,
                                                        imag_aom_freq,
                                                        imag_aom_amp)
                        # with sequential: # Power calibration stage
                        #     delay(self.mot_beams.pulse_delay + t_delay)
                        #     delay(t_exp / 10) # ensure samples are taken during pulse
                        #     if self.num_power_samples == 1:
                        #         self.sampler0.sample(smp_arr_2[0])
                        #     else:
                        #         for n in range(self.num_power_samples):
                        #             smp = [0.0] * 8
                        #             self.sampler0.sample(smp)
                        #             if n < self.num_power_samples - 1:
                        #                 delay(0.7 * t_exp / (self.num_power_samples - 1))
                        #             smp_arr_2[n] = smp[self.mot_sampler_channel]
                        #     delay(t_exp / 10)
                    delay(5 * ms)
                    # for n in range(self.num_power_samples):
                        # smp_arr[n] = smp_arr_2[n][self.mot_sampler_channel]
                    # self.meas_bright_power[i,j], self.meas_bright_power_err[i,j] = self.get_mean_and_std(smp_arr)
                    self.core.break_realtime()

                    ### Take a darkfield image
                    self.mot_beams.turn_off()
                    delay(10 * ms)
                    self.ttl4.pulse(100 * us)
                    self.mot_beams.set_aom(self.mot_beams.ch2_center_freq, self.mot_beams.ch2_amp)

                    # print("Polling camera for images...")
                    self.core.break_realtime()
                    delay(0.5 * ms)
                    self.thorcam1.poll_for_frame_and_append_to_buffer(['signal', 'bright', 'dark'], 3)
                    self.core.break_realtime()
                    self.thorcam1.disarm()
                    self.core.break_realtime()

                    ### handle missing frame by comparing against the expected number of images at this point
                    if self.thorcam1.get_buffer_len('signal') < img_count + 1:
                        missing_sig_check = 1
                        # print('missing sig frame')
                    if self.thorcam1.get_buffer_len('bright') < img_count + 1:
                        missing_bright_check = 1
                        # print('missing bright frame')
                    if self.thorcam1.get_buffer_len('dark') < img_count + 1:
                        missing_dark_check = 1
                        # print('missing dark frame')
                    if ((missing_sig_check == 0) and
                        (missing_bright_check == 0) and
                        (missing_dark_check == 0)):
                        missing_frame_check = 1
                    if missing_frame_check == 0:
                        print('missing frame caught, retaking image')
                        missing_frame_count += 1
                        if missing_sig_check == 0:
                            # print('popping extra sig frame')
                            self.thorcam1.pop_buffer('signal')
                        if missing_bright_check == 0:
                            # print('popping extra bright frame')
                            self.thorcam1.pop_buffer('bright')
                        if missing_dark_check == 0:
                            # print('popping extra dark frame')
                            self.thorcam1.pop_buffer('dark')
                    self.core.break_realtime()

                img_count += 1

        print("Done with run stage.")
        print('Missing frame count: ', missing_frame_count)
        print('Missing frame frac: ', float(missing_frame_count) / shot_count)

    def analyze(self):
        print("Saving...")

        # self.thorcam1.save_all(self)
        # print(self.thorcam1.get_img_shape())
        img_shape = self.thorcam1.get_img_shape()
        data_array_shape = self.data_shape + (img_shape[0], img_shape[1])
        # print(data_array_shape)
        print('reading signals')
        signal, signal_err = self.thorcam1.get_buffer_and_buffer_errs('signal')
        # print(np.array(signal).shape)
        print('reading brights')
        bright, bright_err = self.thorcam1.get_buffer_and_buffer_errs('bright')
        print('reading darks')
        dark, dark_err = self.thorcam1.get_buffer_and_buffer_errs('dark')
        print('reshaping data')
        signal = np.reshape(np.array(signal), data_array_shape)
        print(signal.shape)
        signal_err = np.reshape(np.array(signal_err), data_array_shape)
        bright = np.reshape(np.array(bright), data_array_shape)
        bright_err = np.reshape(np.array(bright_err), data_array_shape)
        dark = np.reshape(np.array(dark), data_array_shape)
        dark_err = np.reshape(np.array(dark_err), data_array_shape)

        self.set_dataset("sig_minus_bright", signal - bright)


        sig_imag_for_plotting = np.reshape(np.mean(signal, axis=-3),
                                            (len(self.imag_delay_times),
                                            data_array_shape[-2],
                                            data_array_shape[-1])
                                            )
        sig_imag_for_plotting_2 = np.reshape(signal,
                                            (len(self.imag_delay_times) * self.Nshots,
                                            data_array_shape[-2],
                                            data_array_shape[-1])
                                            )
        bright_imag_for_plotting = np.reshape(np.mean(bright, axis=-3),
                                            (len(self.imag_delay_times),
                                            data_array_shape[-2],
                                            data_array_shape[-1])
                                            )

        fig = plt.figure(figsize=(2 * len(self.imag_delay_times), 2 ))
        grid = ImageGrid(fig, 111, cbar_mode = 'single', share_all = True,
                        nrows_ncols = (1, len(self.imag_delay_times)),
                        )
        clim = (0, 1024)
        for ax, im in zip(grid, bright_imag_for_plotting): 
            im = ax.imshow(im, cmap='GnBu_r', clim=clim)
        ax.cax.colorbar(im)
        fig.suptitle('Bright background signal')
        plt.tight_layout()
        plt.show()

        # fig = plt.figure(figsize=(2 * len(self.imag_delay_times), 2 * self.Nshots))
        # grid = ImageGrid(fig, 111, cbar_mode = 'single', share_all = True,
        #                 nrows_ncols = (self.Nshots, len(self.imag_delay_times)),
        #                 )
        # clim = (0, 1024)
        # for ax, im in zip(grid, sig_imag_for_plotting_2): 
        #     im = ax.imshow(im, cmap='GnBu_r', clim=clim)
        # ax.cax.colorbar(im)
        # fig.suptitle('Signal images non averaged')
        # plt.tight_layout()
        # plt.show()

        fig = plt.figure(figsize=(2 * len(self.imag_delay_times), 2 ))
        grid = ImageGrid(fig, 111, cbar_mode = 'single', share_all = True,
                        nrows_ncols = (1, len(self.imag_delay_times)),
                        )
        clim = (0, 1024)
        for ax, im in zip(grid, sig_imag_for_plotting): 
            im = ax.imshow(im, cmap='GnBu_r', clim=clim)
        ax.cax.colorbar(im)
        fig.suptitle('Signal images')
        plt.tight_layout()
        plt.show()

        fig = plt.figure(figsize=(2 * len(self.imag_delay_times), 2 ))
        grid = ImageGrid(fig, 111, cbar_mode = 'single', share_all = True,
                        nrows_ncols = (1, len(self.imag_delay_times)),
                        )
        # clim = (0, 1024)
        for ax, im in zip(grid, sig_imag_for_plotting - bright_imag_for_plotting): 
            im = ax.imshow(im, cmap='GnBu_r', clim=clim)
        ax.cax.colorbar(im)
        fig.suptitle('Signal - bright images')
        plt.tight_layout()
        plt.show()

        fig, axs  = plt.subplots(ncols = len(self.imag_delay_times), nrows = 2,
                                figsize=(2 * len(self.imag_delay_times), 4 ),
                                sharex='all', sharey='col')
        for i, im in enumerate(sig_imag_for_plotting - bright_imag_for_plotting): 
            axs[0,i].plot(np.sum(im, axis=0))
            axs[1,i].plot(np.sum(im, axis=1))

        fig.suptitle('Signal - bright images')
        plt.tight_layout()
        plt.show()

        # self.set_dataset("mot_pwr_mon", self.mot_pwr_mon)
        # self.set_dataset("mot_pwr_mon_err", self.mot_pwr_mon_err)
        # self.set_dataset("brightfield_pwr_mon", self.brightfield_pwr_mon)
        # self.set_dataset("brightfield_pwr_mon_err", self.brightfield_pwr_mon_err)
        print("Done with analyze section")