from artiq.experiment import *
from mini_mot import absorption_beam, mot_beams, mot_coils
# from experiments import global_parameters
from global_params import mini_mot_global_params as global_parameters
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid
from scipy import constants as cnst

class FluorLoadRateAndLifetimeVideo(EnvExperiment):
    ### Meausres MOT loading rate via fluorescence imaging - this is nominally more sensitive than
    ### absorption, abd thus better suited to measureing small atom numbers near the start of a
    ### MOT's loading. There are two ways to do write this script:
    ### 1) a bunch of images can be taken in series (eg a video from the camera) while using
    ###    the MOT light to serve as the source of fluorescence. This can give an accurate measure
    ###    of the MOT loading itself, but it is hard to infer the true atom number from such
    ###    a measurement, since the light is detuned a lot and there may be optical pumping effects.
    ### 2) a MOT can be loaded, the OP can be turned off, and then a fluorescence image can be
    ###    taken after some delay time, which the light accurately jumped to resonance - there the
    ###    atom number can be measured more accurately. This script will take signficantly more time
    ###    to run, which can be bad of course.
    ### Since we have absorption scripts now working to accurately calculate the atom number,
    ### it seems less valuable to do option 2 - we can rescale things with a single data point of
    ### absorption data. And reducing the amount of time to take data is good!

    ### upon trying the above, option 1 didn't seem to work that well - there were many dropped
    ### frames, so we will have to go with option 2 even if it takes longer - 2024.10.11
    
    ### modifying a lifetime script to become this loading rate script. Also includes a
    ### boolean value for whether OP light should be present or not, to compare it's effect - 2024.10.30

    def build(self):

        ## Device building
        self.setattr_device("core")
        self.setattr_device("ttl2") # coil control
        self.setattr_device("ttl4") # mot camera 4/30/2024
        self.setattr_device("ttl5") # srs optical pumping shutter 4/30/2024
        self.setattr_device("wlm0") # wavemeter
        self.setattr_device("sampler0") # MOT power monitor
        self.setattr_device("thorcam1")
        self.absorption_beam = absorption_beam.AbsorptionBeam(self) # urukul0_0, urukul0_1, ttl_3
        self.mot_beams = mot_beams.MOTBeams(self) # urukul0_3, ttl_6
        self.mot_coils = mot_coils.MOTCoils(self) # ttl_2,  zotino_0

        ## Saved/ tunable attribute building

        # image timing params
        self.setattr_argument("loading_delay_times_ms", Scannable(global_min=0.02,
                                                                global_max=10000,
                                                                ndecimals=3,
                                                                default=[RangeScan(1., 100., 10)]))


        ## Single attributes, not scanned

        # image params
        self.setattr_argument("imag_t_exp_ms", NumberValue(default=3, ndecimals=1, step=10))
        self.setattr_argument("imag_delay_time_ms", NumberValue(default=0.1, ndecimals=1, step=0.1))
        self.setattr_argument("imag_aom_amp_V", NumberValue(default=0.18, ndecimals=3, step=0.01))
        self.setattr_argument("imag_detuning_gamma", NumberValue(default=-3.5, ndecimals=2, step=0.1))
        
        # MOT params
        self.setattr_argument("mot_current_A", NumberValue(default=4.5, ndecimals=5, step=0.1))
        self.setattr_argument("mot_detuning_gamma", NumberValue(default=-5., ndecimals=5, step=0.1))
        self.setattr_argument("mot_aom_amp_V", NumberValue(default=0.18, ndecimals=5, step=0.1))
        self.setattr_argument('mot_loading_time_s', NumberValue(default=2, ndecimals=2, step=0.1))
        
        # camera params
        self.setattr_argument('camera_gain_dB', NumberValue(default=48, ndecimals=0, step=1))
        self.setattr_argument('camera_t_exp_ms', NumberValue(default=5, ndecimals=3, step=0.1))
        self.setattr_argument('cam_roi_x_0', NumberValue(default=715, ndecimals=0, step=1))
        self.setattr_argument('cam_roi_y_0', NumberValue(default=535, ndecimals=0, step=1))
        self.setattr_argument('cam_roi_w_x', NumberValue(default=80, ndecimals=0, step=1))
        self.setattr_argument('cam_roi_w_y', NumberValue(default=80, ndecimals=0, step=1))

        # other params
        self.setattr_argument("op_bool", BooleanValue(default=True))
        self.setattr_argument("Nshots", NumberValue(default=5, ndecimals=0, step=1))
        self.setattr_argument('isotope', NumberValue(default=48, ndecimals=0, step=1))
        self.setattr_argument('num_power_samples', NumberValue(default=50, ndecimals=0, step=1))

        ## Other attributes, non-saved
        self.a = 1 # random thing that must be set here
        self.camera_trigger_delay = 20 * us
        self.op_shutter_delay = 3*ms
        self.mot_shutter_delay = 2*ms
        self.op_shutter_on_delay = 3*ms
        self.mot_shutter_on_delay = 1.5*ms
        self.shutter_delay_delta = self.op_shutter_delay - self.mot_shutter_delay
        # self.mot_aom_freq = 80e-6 #THz
        # self.mot_aom_amp = 0.12
        # self.brightfield_frequency = 300.80795 #THz
        self.mot_loading_time = 0.5 * s
        self.n_video_frames = 100

    def prepare(self):

        ## set up camera ROI
        self.roi = [int(self.cam_roi_x_0), int(self.cam_roi_y_0),
                    int(self.cam_roi_x_0 + self.cam_roi_w_x),
                    int(self.cam_roi_y_0 + self.cam_roi_w_y)]

        # pull the atomic frequencies from global params
        self.cooling_freq = global_parameters.cooling_line_freq[str(self.isotope)]
        self.op_freq = global_parameters.op_line_freq[str(self.isotope)]

        # calculate the detuning that the lasers should be locked to on the wavemeter
        self.vexlum_freq = (self.cooling_freq # set Vexlum freq where there will be max AOM efficiency at MOT dets
                            + (self.mot_detuning_gamma * global_parameters.cooling_gamma_linear
                                - 2 * self.mot_beams.ch2_center_freq) / 10**12) 
        self.imag_aom_freq = (((self.imag_detuning_gamma - self.mot_detuning_gamma) *
                                (global_parameters.cooling_gamma_linear / 2)) + 
                                self.mot_beams.ch2_center_freq)
        print(self.imag_aom_freq*1e-6)

        # prep data shape and arrays
        self.data_shape = (len(self.loading_delay_times_ms), self.Nshots)
        self.n_imags = len(self.loading_delay_times_ms) * self.Nshots

        # prep power monitor arrays
        self.meas_sig_power = np.zeros(self.data_shape)
        self.meas_sig_power_err = np.zeros(self.data_shape)
        self.meas_bright_power = np.zeros(self.data_shape)
        self.meas_bright_power_err = np.zeros(self.data_shape)


    @kernel(flags={"fast-math"})
    def get_mean_and_std(self, arr):# -> TList(TFloat):
        meanN = 0.0
        stdNS = 0.0
        N = len(arr)

        #mean
        for a in arr:
            meanN += a

            mean = meanN/N

        #std
        for a in arr:
            stdNS += (a-mean)**2

            std = np.sqrt(stdNS/N)

        return mean, std


    @kernel
    def run(self):

        self.a # random thing that must be called here
        self.core.wait_until_mu(now_mu())
        self.core.reset()

        ######## INITIALIZATIONS ########

        ### wrapped initializations
        self.mot_beams.init()
        self.mot_coils.init()
        delay(1*s)
        # self.absorption_beam.init()

        ### ttl initialization
        # self.ttl2.output()
        self.ttl4.output()
        self.ttl5.output()

        # delay(1 * ms)
        # self.ttl2.off() # MOT coils on
        delay(1 * ms)
        self.ttl4.off() # Camera TTL low
        delay(1 * ms)
        self.ttl5.on() # OP shutter open

        ### Sampler initialization
        self.core.break_realtime()
        self.sampler0.init()
        self.sampler0.set_gain_mu(0,0) # check this!
        self.sampler0.set_gain_mu(1,0) # check this!
        self.sampler0.set_gain_mu(2,0) # check this!
        self.sampler0.set_gain_mu(3,0) # check this!

        ### thorcam initialization
        self.thorcam1.connect()
        self.thorcam1.initialize(experiment=self,
                                exposure_time_us = self.camera_t_exp_ms * 1000,
                                operation_mode = 1,
                                frames_per_trigger_zero_for_unlimited = self.n_video_frames,
                                image_poll_timeout_ms = 1500,
                                gain = self.camera_gain_dB * 10,
                                roi = self.roi,
                                trigger_polarity = 0, # 0 for rising edge 1 for falling edge
                                wavelength_nm = 498,
                                )

        self.thorcam1.arm(self.n_video_frames)

        self.core.break_realtime()
        self.ttl5.pulse(100*us) ## 100 shots for background
        delay(1*ms)
        self.core.wait_until_mu(now_mu())
        print(self.thorcam1.get_measured_frame_rate_fps())
        # self.core.break_realtime()

        # img_count = 0
        # missing_frame_count = 0
        # shot_count = 0

        # print("Tuning MOT beams...")
        # self.wlm0.SetPIDCourseNum(3, self.op_freq)
        # self.wlm0.SetPIDCourseNum(5, self.vexlum_freq)
        # self.core.break_realtime()
        # delay(0.5 * s)


        # ### take darkfield bg
        # print("taking bg")
        # self.ttl5.pulse(100*us) ## 100 shots for background
        # delay(self.frame_rate * self.n_video_frames)

        # self.core.wait_until_mu(now_mu())
        # self.thorcam3.poll_for_frame_and_append_to_buffer('dark')

        # self.core.break_realtime()


        # #######################
        # # wlm GetFrequency call. fyi can be slow since its an rpc
        # channel = 1
        # cooling_freq_THz = self.wlm0_old.GetFrequency(channel)


        # ## Save metadata
        # self.set_dataset("cooling_freq_THz", cooling_freq_THz)
        # self.set_dataset("mot_coil_current_A", self.mot_coil_current_A)

        # #######################

        
       
        # ## Run mott 10 times
        # for i in range(10):
            
        #     print(i)
        #     self.core.break_realtime()
            
        #     ### OP on            
        #     self.ttl4.on()
            
        #     ### load MOT for 1s
        #     delay(1*s)
            
        #     ### OP off
        #     self.ttl4.off()

        #     ## trigger 100 frames on camera
        #     self.ttl5.pulse(50*us)
        #     delay(1*s)
        #     for i in range(100):
        #         self.thorcam3.poll_for_frame_and_append_to_buffer('sig')


        # print("Reading out...")
        # self.save_imgs()
        # self.core.break_realtime()
        # self.ttl4.on()

        # print("Finished.")
