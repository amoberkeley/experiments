from artiq.experiment import *
from mini_mot import absorption_beam, mot_beams, mot_coils
# from experiments import global_parameters
from global_params import mini_mot_global_params as global_parameters
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid

class FluorParamScan(EnvExperiment):
    
    def build(self):
        
        ## Device building
        self.setattr_device("core")
        # self.setattr_device("ttl2") # coil control
        self.setattr_device("ttl4") # mot camera 4/30/2024
        self.setattr_device("ttl5") # srs optical pumping shutter 4/30/2024
        self.setattr_device("wlm0") # wavemeter
        self.setattr_device("sampler0") # MOT power monitor, ion pump current monitor, etc
        self.setattr_device("thorcam1")
        self.mot_coils = mot_coils.MOTCoils(self) #ttl_2, zotino_0
        self.mot_beams = mot_beams.MOTBeams(self, aom_freq = 200 * MHz) # urukul0_2, ttl_6

        ## Saved/ tunable attribute building
        ## Scannable attributes for calibration
        self.setattr_argument("imag_detunings_gamma", Scannable(global_min=-10, global_max=10,
                                                                ndecimals=3,
                                                                default=[RangeScan(-2.0, 2.0, 5)]))
        self.setattr_argument("imag_aom_amp_V", Scannable(global_min=0.0, global_max=2.0,
                                                            ndecimals=3,
                                                            default=[RangeScan(0.02, 0.1, 5)]))
        self.setattr_argument("t_exp_ms", Scannable(global_min=0.0, global_max=10.0,
                                                    ndecimals=3,
                                                    default=[RangeScan(0.2, 1.0, 5)]))

        ## None scanned attributes - tunable to get good signals
        self.setattr_argument("mot_current", NumberValue(default=4., ndecimals=5, step=0.1))
        self.setattr_argument("mot_detuning_gamma", NumberValue(default=-3., ndecimals=5, step=0.1))
        self.setattr_argument("mot_aom_amp_V", NumberValue(default=0.18, ndecimals=5, step=0.1))
        # self.setattr_argument("biasx_current", NumberValue(default=0., ndecimals=5, step=0.1))
        # self.setattr_argument("biasy_current", NumberValue(default=0., ndecimals=5, step=0.1))
        # self.setattr_argument("ti_ball_current", NumberValue(default=0., ndecimals=5, step=0.1))
        self.setattr_argument("Nshots", NumberValue(default=10, ndecimals=0, step=1))
        self.setattr_argument('imag_pulse_delay_ms', NumberValue(default=0.5, ndecimals=2, step=1))
        self.setattr_argument('isotope', NumberValue(default=48, ndecimals=0, step=1))
        self.setattr_argument('camera_gain_dB', NumberValue(default=48, ndecimals=0, step=1))
        self.setattr_argument('camera_t_exp_ms', NumberValue(default=1.5, ndecimals=3, step=1))
        self.setattr_argument('num_power_samples', NumberValue(default=5, ndecimals=0, step=1))

        ## Fixed attributes, non-saved
        self.a = 1 # random thing that must be set here
        self.roi_w_x = 200
        self.roi_w_y = 200
        self.roi_x_0 = 670
        self.roi_y_0 = 500
        self.roi = [self.roi_x_0, self.roi_y_0,
                    self.roi_x_0 + self.roi_w_x,
                    self.roi_y_0 + self.roi_w_y]
        self.camera_trigger_delay = 20 * us
        self.op_shutter_delay = 3 * ms
        self.mot_shutter_delay = 2 * ms
        self.op_shutter_on_delay = 3 * ms
        self.mot_shutter_on_delay = 1.5 * ms
        self.shutter_delay_delta = self.op_shutter_delay - self.mot_shutter_delay
        # self.mot_aom_freq = 80e-6 #THz
        # self.mot_aom_amp = 0.12
        # self.brightfield_frequency = 300.80795 #THz
        self.mot_loading_time = 0.2 * s
        self.mot_sampler_channel = 0

    def prepare(self):

        # pull the atomic frequencies from global params
        self.cooling_freq = global_parameters.cooling_line_freq[str(self.isotope)]
        self.op_freq = global_parameters.op_line_freq[str(self.isotope)]

        # calculate the detuning that the lasers should be locked to on the wavemeter
        self.vexlum_freq = (self.cooling_freq # set Vexlum frequency where there will be maximum AOM efficiency at nom MOT detuning
                            + (self.mot_detuning_gamma * global_parameters.cooling_gamma_linear
                                -  2 * self.mot_beams.ch2_center_freq) / 10**12)

        # set up scan lists - what to do if they aren't scans???
        self.fluor_imag_detunings_gamma = np.array([d for d in self.imag_detunings_gamma])
        self.fluor_imag_aom_freqs = (self.mot_beams.ch2_center_freq +
                                        (self.fluor_imag_detunings_gamma - self.mot_detuning_gamma)
                                        * global_parameters.cooling_gamma_linear / 2)
        self.fluor_imag_aom_Vs = np.array([v for v in self.imag_aom_amp_V])
        self.fluor_imag_t_exps = np.array([t for t in self.t_exp_ms]) * ms

        # set up arrays to save measured sampler data into, fitting it into properly sized arrays
        self.n_imags = len(self.fluor_imag_aom_freqs) * len(self.fluor_imag_aom_Vs) * len(self.fluor_imag_t_exps) * self.Nshots
        self.data_shape = (len(self.fluor_imag_aom_freqs), len(self.fluor_imag_aom_Vs), len(self.fluor_imag_t_exps), self.Nshots)
        self.meas_sig_power = np.zeros(int(self.n_imags))
        self.meas_sig_power_err = np.zeros(int(self.n_imags))
        self.meas_bright_power = np.zeros(int(self.n_imags))
        self.meas_bright_power_err = np.zeros(int(self.n_imags))

        self.mot_aom_freq = self.mot_beams.ch2_center_freq

    @kernel(flags={"fast-math"})
    def get_mean_and_std(self, arr):# -> TList(TFloat):
        meanN = 0.0
        stdNS = 0.0
        N = len(arr)

        #mean
        for a in arr:
            meanN += a

        mean = meanN/N

        #std
        for a in arr:
            stdNS += (a-mean)**2

        std = np.sqrt(stdNS/N)

        return mean, std

    @kernel
    def run(self):

        self.a # random thing that must be called here
        self.core.wait_until_mu(now_mu())
        self.core.reset()

        ######## INITIALIZATIONS ########

        ### wrapped initializations
        self.mot_beams.init()
        self.mot_coils.init()
        self.mot_coils.set_current(self.mot_current)

        ### ttl initialization
        # self.ttl2.output()
        self.ttl4.output()
        self.ttl5.output()

        # delay(1 * ms)
        # self.ttl2.off() # MOT coils on
        delay(1 * ms)
        self.ttl4.off() # Camera TTL low
        delay(1 * ms)
        self.ttl5.on() # OP shutter open

        ### Sampler initialization
        self.core.break_realtime()
        self.sampler0.init()
        self.sampler0.set_gain_mu(0,0) # check this!
        self.sampler0.set_gain_mu(1,0) # check this!
        self.sampler0.set_gain_mu(2,0) # check this!
        self.sampler0.set_gain_mu(3,0) # check this!

        ### thorcam initialization
        self.thorcam1.connect()
        self.thorcam1.initialize(experiment=self,
                                exposure_time_us = self.camera_t_exp_ms * 1000,
                                operation_mode = 1,
                                frames_per_trigger_zero_for_unlimited = 1,
                                image_poll_timeout_ms = 1500,
                                gain = self.camera_gain_dB * 10,
                                roi = self.roi,
                                trigger_polarity = 0, # 0 for rising edge 1 for falling edge
                                wavelength_nm = 498,
                                )
        self.core.break_realtime()
        # delay(100 * ms)
        # self.thorcam1.arm(self.n_imags * 3) # times however many things you're doing here...

        ##########################
        ### MAIN LOOP

        ### Tune wavemeter for vexlum
        print("Tuning MOT beams...")
        self.wlm0.SetPIDCourseNum(3, self.op_freq)
        self.wlm0.SetPIDCourseNum(5, self.vexlum_freq)
        self.core.break_realtime()
        delay(0.5 * s)
        # self.core.wait_until_mu(now_mu())
        # delay(0.25 * s)

        img_count = 0
        missing_frame_count = 0
        shot_count = 0

        ### Loop thorugh imaging freqs
        for aom_freq in self.fluor_imag_aom_freqs:
            # print(100 * float(img_count) / self.n_imags, ' percent done')
            # print('aom frequency:', aom_freq / MHz, ' MHz')

            ### Loop through imaging powers
            for aom_amp in self.fluor_imag_aom_Vs:
                # print('aom amp:', aom_amp)

                ### Loop through imaging exposure times
                for t_exp in self.fluor_imag_t_exps:
                    print(100 * float(img_count) / self.n_imags, ' percent done')
                    # print('t_exp:', t_exp / ms, ' ms')

                    ### Take Nshots images
                    for m in range(self.Nshots):

                        ### Add a check for missing frames
                        missing_frame_check = 0
                        while missing_frame_check == 0:
                            shot_count += 1
                            missing_sig_check = 0
                            missing_bright_check = 0
                            missing_dark_check = 0
                            self.core.break_realtime()

                            ### Arm the camera
                            self.thorcam1.arm(3)
                            self.core.break_realtime()
                            delay(0.5 * ms)

                            ### Load the MOT
                            self.mot_coils.turn_on() # MOT coils on
                            delay(0.5 * ms)
                            self.ttl5.on() # OP shutter open
                            delay(0.5 * ms)
                            self.mot_beams.turn_on() # MOT beams on
                            # delay(1 * ms)
                            
                            with parallel:
                                smp_arr = [0.0] * self.num_power_samples
                                smp_arr_2 = [[0.0] * 8] * self.num_power_samples
                                delay(self.mot_loading_time)

                            ### Take a signal image
                            with parallel:
                                # after appropriate delay, send imaging pulse to camera
                                with sequential:
                                    delay(self.imag_pulse_delay_ms * ms - self.camera_trigger_delay)
                                    self.ttl4.pulse(100 * us)
                                self.mot_coils.turn_off() # MOT coils off
                                self.mot_beams.fluor_imag_pulse(t_exp,
                                                                self.imag_pulse_delay_ms * ms,
                                                                aom_freq,
                                                                aom_amp)
                                with sequential: # Power calibration stage
                                    delay(self.imag_pulse_delay_ms * ms)
                                    delay(t_exp / 10) # ensure samples are taken during pulse
                                    if self.num_power_samples == 1:
                                        self.sampler0.sample(smp_arr_2[0])
                                    else:
                                        for n in range(self.num_power_samples):
                                            # smp = [0.0] * 8
                                            self.sampler0.sample(smp_arr_2[n])
                                            delay(0.8 * t_exp / (self.num_power_samples - 1))
                                            # smp_arr_2[n] = smp[self.mot_sampler_channel]
                                    delay(t_exp / 10)
                            delay(0.5 * ms)
                            for n in range(self.num_power_samples):
                                smp_arr[n] = smp_arr_2[n][self.mot_sampler_channel]
                            self.meas_sig_power[img_count], self.meas_sig_power_err[img_count] = self.get_mean_and_std(smp_arr)
                            self.core.break_realtime()

                            ### Take a brightfield image
                            with parallel:
                                smp_arr = [0.0] * self.num_power_samples
                                smp_arr_2 = [[0.0] * 8] * self.num_power_samples
                                delay(50 * ms) # let atoms fly away
                            with parallel:
                                # after appropriate delay, send imaging pulse to camera
                                with sequential:
                                    delay(self.mot_beams.pulse_delay + self.imag_pulse_delay_ms * ms - self.camera_trigger_delay)
                                    self.ttl4.pulse(100 * us)
                                self.mot_beams.fluor_imag_pulse(t_exp,
                                                                self.mot_beams.pulse_delay + self.imag_pulse_delay_ms * ms,
                                                                aom_freq,
                                                                aom_amp)
                                with sequential: # Power calibration stage
                                    delay(self.mot_beams.pulse_delay + self.imag_pulse_delay_ms * ms)
                                    delay(t_exp / 10) # ensure samples are taken during pulse
                                    if self.num_power_samples == 1:
                                        self.sampler0.sample(smp_arr_2[0])
                                    else:
                                        for n in range(self.num_power_samples):
                                            # smp = [0.0] * 8
                                            self.sampler0.sample(smp_arr_2[n])
                                            delay(0.8 * t_exp / (self.num_power_samples - 1))
                                            # smp_arr_2[n] = smp[self.mot_sampler_channel]
                                    delay(t_exp / 10)
                            delay(0.5 * ms)
                            for n in range(self.num_power_samples):
                                smp_arr[n] = smp_arr_2[n][self.mot_sampler_channel]
                            self.meas_bright_power[img_count], self.meas_bright_power_err[img_count] = self.get_mean_and_std(smp_arr)
                            self.core.break_realtime()

                            ### Power calibration stage
                            ### should this be changed to be contemporaneous with the images being taken???
                            # self.mot_beams.set_aom(aom_freq, aom_amp)
                            # delay(10 * us)
                            # self.mot_beams.turn_on()
                            # delay(self.mot_beams.pulse_delay)
                            # for n in range(self.num_power_samples):
                            #     smp = [0.0] * 8
                            #     delay(0.5 * ms)
                            #     self.sampler0.sample(smp)
                            #     smp_arr[n] = smp[3]
                            # delay(0.5 * ms)
                            # self.measured_imag_power[img_count], self.measured_imag_power_err[img_count] = self.get_mean_and_std(smp_arr)
                            # self.core.break_realtime()
                            # delay(0.1 * s)

                            ### Take a darkfield image
                            self.mot_beams.turn_off()
                            delay(10 * ms)
                            self.ttl4.pulse(100 * us)
                            self.mot_beams.set_aom(self.mot_beams.ch2_center_freq, self.mot_beams.ch2_amp)

                            # print("Polling camera for images...")
                            self.core.break_realtime()
                            delay(0.5 * ms)
                            self.thorcam1.poll_for_frame_and_append_to_buffer(['signal', 'bright', 'dark'], 3)
                            self.core.break_realtime()
                            self.thorcam1.disarm()
                            self.core.break_realtime()

                            ### handle missing frame by comparing against the expected number of images at this point
                            if self.thorcam1.get_buffer_len('signal') < img_count + 1:
                                missing_sig_check = 1
                                # print('missing sig frame')
                            if self.thorcam1.get_buffer_len('bright') < img_count + 1:
                                missing_bright_check = 1
                                # print('missing bright frame')
                            if self.thorcam1.get_buffer_len('dark') < img_count + 1:
                                missing_dark_check = 1
                                # print('missing dark frame')
                            if ((missing_sig_check == 0) and
                                (missing_bright_check == 0) and
                                (missing_dark_check == 0)):
                                missing_frame_check = 1
                            if missing_frame_check == 0:
                                print('missing frame caught, retaking image')
                                missing_frame_count += 1
                                if missing_sig_check == 0:
                                    # print('popping extra sig frame')
                                    self.thorcam1.pop_buffer('signal')
                                if missing_bright_check == 0:
                                    # print('popping extra bright frame')
                                    self.thorcam1.pop_buffer('bright')
                                if missing_dark_check == 0:
                                    # print('popping extra dark frame')
                                    self.thorcam1.pop_buffer('dark')
                            self.core.break_realtime()

                        img_count += 1
                        # print(img_count,'images taken out of', self.n_imags)

        self.core.break_realtime()
        self.ttl5.on() # op beams on
        delay(0.5 * ms)
        self.mot_beams.set_aom(self.mot_beams.ch2_center_freq, self.mot_beams.ch2_amp)
        delay(0.5 * ms)
        self.mot_beams.turn_on() # MOT beams on
        delay(0.5 * ms)
        self.mot_coils.turn_on() # MOT coils on
        print("Done with run stage.")
        print('Missing frame count: ', missing_frame_count)
        print('Missing frame frac: ', float(missing_frame_count) / shot_count)

    def analyze(self):
        print("Saving...")
        self.thorcam1.save_all(self)
        # print(self.thorcam1.get_img_shape())
        img_shape = self.thorcam1.get_img_shape()
        data_array_shape = self.data_shape + (img_shape[0] + 1, img_shape[1] + 1)
        # print(data_array_shape)
        print('reading signals')
        signal, signal_err = self.thorcam1.get_buffer_and_buffer_errs('signal')
        # print(np.array(signal).shape)
        print('reading brights')
        bright, bright_err = self.thorcam1.get_buffer_and_buffer_errs('bright')
        print('reading darks')
        dark, dark_err = self.thorcam1.get_buffer_and_buffer_errs('dark')
        print('reshaping data')
        signal = np.reshape(np.array(signal), data_array_shape)
        # print(signal.shape)
        signal_err = np.reshape(np.array(signal_err), data_array_shape)
        bright = np.reshape(np.array(bright), data_array_shape)
        bright_err = np.reshape(np.array(bright_err), data_array_shape)
        dark = np.reshape(np.array(dark), data_array_shape)
        dark_err = np.reshape(np.array(dark_err), data_array_shape)

        print('reading power samples')
        meas_sig_pow = np.mean(np.reshape(self.meas_sig_power,
                                        self.data_shape),
                                axis=-1)
        meas_sig_pow_err = np.sqrt(np.sum(np.reshape(self.meas_sig_power_err,
                                                    self.data_shape)**2,
                                        axis=-1)) / self.Nshots
        meas_bright_pow = np.mean(np.reshape(self.meas_bright_power,
                                            self.data_shape),
                                    axis=-1)
        meas_bright_pow_err = np.sqrt(np.sum(np.reshape(self.meas_bright_power_err,
                                                        self.data_shape)**2,
                                            axis=-1)) / self.Nshots
        # print(meas_imag_pow.shape)
        # print(meas_imag_pow)
        # print(meas_imag_err)
        self.set_dataset("meas_sig_pow", meas_sig_pow)
        self.set_dataset("meas_sig_pow_err", meas_sig_pow_err)
        self.set_dataset("meas_bright_pow", meas_bright_pow)
        self.set_dataset("meas_bright_pow_err", meas_bright_pow_err)

        #do background subtraction and calculate error in signal counts - check with Jack
        print('performing background subtraction')
        sig_bkg_sub = signal - bright # no way to meas. power diff. b/w bright & sig, assuming they are equal
        sig_bkg_sub_err = np.sqrt(signal_err**2 + bright_err**2)
        sig_bkg_sub_mean = np.mean(sig_bkg_sub, axis=-3)
        # print(sig_bkg_sub_mean.shape)
        sig_bkg_sub_err_mean = np.sqrt(np.sum(sig_bkg_sub_err**2, axis=-3) / self.Nshots) 
        sig_bkg_sub_std = np.std(sig_bkg_sub, axis=-3)

        sig_mean = np.mean(signal, axis=-3)
        sig_std = np.std(signal, axis=-3)
        sig_err_mean = np.sqrt(np.sum(signal_err**2, axis=-3)) / self.Nshots

        # imag_for_plotting = np.reshape(sig_bkg_sub,
        #                                 (int(self.n_imags),# / self.Nshots),
        #                                 data_array_shape[-2],
        #                                 data_array_shape[-1])
        #                                 )
        # sig_imag_for_plotting = np.reshape(signal,#np.mean(signal, axis=-3),
        #                                     (int(self.n_imags),# / self.Nshots),
        #                                     data_array_shape[-2],
        #                                     data_array_shape[-1])
        #                                     )
        # bright_imag_for_plotting = np.reshape(bright,#np.mean(bright, axis=-3),
        #                                     (int(self.n_imags),# / self.Nshots),
        #                                     data_array_shape[-2],
        #                                     data_array_shape[-1])
        #                                     )

        # fig = plt.figure(figsize=(2 * self.data_shape[0], 2 * self.data_shape[1] * self.data_shape[2] * self.data_shape[3]))
        # grid = ImageGrid(fig, 111, cbar_mode = 'single', share_all = True,
        #                 nrows_ncols = (self.data_shape[1] * self.data_shape[2] * self.data_shape[3], self.data_shape[0]),
        #                 )
        # clim = (np.min(imag_for_plotting), np.max(imag_for_plotting))
        # for ax, im in zip(grid, imag_for_plotting): 
        #     im = ax.imshow(im, cmap='GnBu_r', clim=clim)
        # ax.cax.colorbar(im)
        # plt.show()

        # fig = plt.figure(figsize=(2 * self.data_shape[0], 2 * self.data_shape[1] * self.data_shape[2] * self.data_shape[3]))
        # grid = ImageGrid(fig, 111, cbar_mode = 'single', 
        #                 nrows_ncols = (self.data_shape[1] * self.data_shape[2]* self.data_shape[3], self.data_shape[0]),
        #                 )
        # clim = (np.min(sig_imag_for_plotting), 1048)
        # for ax, im in zip(grid, sig_imag_for_plotting): 
        #     im = ax.imshow(im, cmap='GnBu_r', clim=clim)
        # ax.cax.colorbar(im)
        # plt.show()

        # fig = plt.figure(figsize=(2 * self.data_shape[0], 2 * self.data_shape[1] * self.data_shape[2]* self.data_shape[3]))
        # grid = ImageGrid(fig, 111, cbar_mode = 'single', 
        #                 nrows_ncols = (self.data_shape[1] * self.data_shape[2]* self.data_shape[3], self.data_shape[0]),
        #                 )
        # clim = (np.min(bright_imag_for_plotting), np.max(bright_imag_for_plotting))
        # for ax, im in zip(grid, bright_imag_for_plotting): 
        #     im = ax.imshow(im, cmap='GnBu_r', clim=clim)
        # ax.cax.colorbar(im)
        # plt.show()

        # fluor_counts = np.sum(sig_bkg_sub, axis=(-1, -2))
        # fluor_counts_mean = np.mean(fluor_counts, axis = -1)
        # fluor_counts_std = np.std(fluor_counts, axis=-1)
        # fluor_counts_err_from_pixel_err = np.sqrt(np.sum(sig_bkg_sub_err_mean**2, axis=(-1, -2)))
        # fluor_counts_err_from_pixel_std = np.sqrt(np.sum(sig_bkg_sub_std**2, axis=(-1, -2)))
        # fluor_counts_err = np.maximum(fluor_counts_std,
        #                                 np.maximum(fluor_counts_err_from_pixel_err,
        #                                             fluor_counts_err_from_pixel_std))

        fluor_counts = np.sum(sig_bkg_sub, axis=(-1, -2))
        fluor_counts_mean = np.mean(fluor_counts, axis = -1)
        fluor_counts_std = np.std(fluor_counts, axis=-1)
        # fluor_counts_err_from_pixel_err = np.sqrt(np.sum(sig_err_mean**2, axis=(-1, -2)))
        # fluor_counts_err_from_pixel_std = np.sqrt(np.sum(sig_std**2, axis=(-1, -2)))
        fluor_counts_err = fluor_counts_std #np.maximum(fluor_counts_std,
                            #            np.maximum(fluor_counts_err_from_pixel_err,
                             #                       fluor_counts_err_from_pixel_std))

        # print(fluor_counts_mean.shape)

        detuning_array, aom_amp_array, t_exp_array = np.meshgrid(self.fluor_imag_detunings_gamma,
                                                                self.fluor_imag_aom_Vs,
                                                                self.fluor_imag_t_exps,
                                                                indexing='ij')

        print('plotting data')
        fig, ax = plt.subplots(figsize=(6,4))

        mec_list = ['darkslategrey', 'maroon', 'indigo', 'black', 'darkgoldenrod', 'darkolivegreen']
        mfc_list = ['darkturquoise', 'indianred', 'darkorchid', 'grey', 'gold', 'greenyellow']
        markers = ['o','s', 'D','^', '*', 'p']
        for i, amp in enumerate(self.fluor_imag_aom_Vs):
            for j, t in enumerate(self.fluor_imag_t_exps):
                fluor_counts_mean_zeroed = (fluor_counts_mean[:,i,j])#/ meas_imag_pow[:,i,j]
                fluor_counts_mean_scaled = fluor_counts_mean_zeroed #/ np.max(fluor_counts_mean_zeroed)
                # print(fluor_counts_mean[:,i,j].shape)
                # print(fluor_counts_err[:,i,j].shape)
                # print(self.fluor_imag_detunings_gamma.shape)
                ax.errorbar(self.fluor_imag_detunings_gamma,
                            fluor_counts_mean_scaled,
                            yerr = fluor_counts_err[:,i,j],# / np.max(fluor_counts_mean_zeroed),
                            ls='', marker=markers[i],
                            ecolor=mec_list[j],
                            mec=mec_list[j],
                            mfc=mfc_list[j],
                            label = 'A=%.2fV,t=%.2fms'%(amp, t*1000))
        ax.legend()
        # ax.set_ylim(-0.05, 1.05)
        ax.set_xlabel(r'$\delta [\Gamma]$')
        ax.set_ylabel(r'$Fluor. counts$')

        plt.tight_layout()
        plt.show()

        # fig, ax = plt.subplots(figsize=(6,4))

        # mec_list = ['darkslategrey', 'maroon', 'indigo', 'black', 'darkgoldenrod', 'darkolivegreen']
        # mfc_list = ['darkturquoise', 'indianred', 'darkorchid', 'grey', 'gold', 'greenyellow']
        # markers = ['o','s', 'D','^', '*', 'p']
        # for i, amp in enumerate(self.fluor_imag_detunings_gamma):
        #     for j, t in enumerate(self.fluor_imag_t_exps):
        #         fluor_counts_mean_zeroed = fluor_counts_mean[i,:,j]#/ meas_imag_pow[:,i,j]
        #         fluor_counts_mean_scaled = fluor_counts_mean_zeroed #/ np.max(fluor_counts_mean_zeroed)
        #         # print(fluor_counts_mean[:,i,j].shape)
        #         # print(fluor_counts_err[:,i,j].shape)
        #         # print(self.fluor_imag_detunings_gamma.shape)
        #         ax.errorbar(meas_sig_pow[i,:,j],
        #                     fluor_counts_mean_scaled,
        #                     xerr = meas_sig_pow_err[i,:,j],
        #                     yerr = fluor_counts_err[i,:,j],# / np.max(fluor_counts_mean_zeroed),
        #                     ls='', marker=markers[i],
        #                     ecolor=mec_list[j],
        #                     mec=mec_list[j],
        #                     mfc=mfc_list[j],
        #                     label = 'delta=%.2fGamma,t=%.2fms'%(amp, t*1000))
        # ax.legend()
        # # ax.set_ylim(-0.05, 1.05)
        # ax.set_xlim(0.0, np.max(meas_sig_pow)*1.05)
        # ax.set_ylim(0.0, np.max(fluor_counts_mean + fluor_counts_err)*1.05)
        # ax.set_xlabel(r'$V_{PD} [V]$')
        # ax.set_ylabel(r'$Fluor. counts$')
        # # ax.set_yscale('log')

        # plt.tight_layout()
        # plt.show()

        # fig, ax = plt.subplots(figsize=(6,4))

        # mec_list = ['darkslategrey', 'maroon', 'indigo', 'black', 'darkgoldenrod', 'darkolivegreen']
        # mfc_list = ['darkturquoise', 'indianred', 'darkorchid', 'grey', 'gold', 'greenyellow']
        # markers = ['o','s', 'D','^', '*', 'p']
        # for i, det in enumerate(self.fluor_imag_detunings_gamma):
        #     for j, amp in enumerate(self.fluor_imag_aom_Vs):
        #         fluor_counts_mean_zeroed = fluor_counts_mean[i,j,:]#/ meas_imag_pow[:,i,j]
        #         fluor_counts_mean_scaled = fluor_counts_mean_zeroed #/ np.max(fluor_counts_mean_zeroed)
        #         # print(fluor_counts_mean[:,i,j].shape)
        #         # print(fluor_counts_err[:,i,j].shape)
        #         # print(self.fluor_imag_detunings_gamma.shape)
        #         ax.errorbar(self.fluor_imag_t_exps * 1000,
        #                     fluor_counts_mean_scaled,
        #                     xerr = meas_sig_pow_err[i,j,:],
        #                     yerr = fluor_counts_err[i,j,:],# / np.max(fluor_counts_mean_zeroed),
        #                     ls='', marker=markers[i],
        #                     ecolor=mec_list[j],
        #                     mec=mec_list[j],
        #                     mfc=mfc_list[j],
        #                     label = 'delta=%.2fGamma,t=%.2fms'%(det, amp))
        # ax.legend()
        # # ax.set_ylim(-0.05, 1.05)
        # ax.set_xlim(0.0, np.max(self.fluor_imag_t_exps * 1000)*1.05)
        # ax.set_ylim(0.0, np.max(fluor_counts_mean + fluor_counts_err)*1.05)
        # ax.set_xlabel(r'$t_{\mathrm{exp}} [ms]$')
        # ax.set_ylabel(r'$Fluor. counts$')
        # # ax.set_yscale('log')

        # plt.tight_layout()
        # plt.show()


        print("Done with analyze stage.")