from artiq.experiment import *
from mini_mot import absorption_beam, mot_beams, mot_coils 
# from experiments import global_parameters
from global_params import mini_mot_global_params as global_parameters
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid
from scipy import constants as cnst

class FluorSecStageMOTLifetime(EnvExperiment):
    ### Meausres MOT lifetime for a second stage MOT via fluorescence imaging - this is
    ### more sensitive than absorption, abd thus better suited to measureing small atom numbers
    ### near the end of a MOT's life. This script takes a bunch of images in series
    ### (eg a video from the camera) while using the MOT light to serve as the source of fluorescence.
    ### This can give an accurate measure of the MOT lifetime/ loading time itself, but it is hard
    ### to infer the true atom number since the light is detuned a lot and there may be
    ### optical pumping effects. But, since we have absorption scripts now working, we can rescale
    ### things with a single data point of absorption data that quantifies the total atom number!
    ### This will run a second stage MOT such that we can measure the lifetime of the MOT at
    ### different detunings/ powers that might change relative to the originally loaded MOT.

    ### I have not implemented a check to make sure that the images are properly read out
    ### (eg the while loop in other scripts). If we run into that issue, we can try to do it.

    def build(self):

        ## Device building
        self.setattr_device("core")
        # self.setattr_device("ttl2") # coil control
        self.setattr_device("ttl4") # mot camera 4/30/2024
        self.setattr_device("ttl5") # srs optical pumping shutter 4/30/2024
        self.setattr_device("wlm0") # wavemeter
        self.setattr_device("sampler0") # MOT power monitor
        self.setattr_device("thorcam1")
        self.absorption_beam = absorption_beam.AbsorptionBeam(self) # urukul0_0, urukul0_1, ttl_3
        self.mot_beams = mot_beams.MOTBeams(self) # urukul0_3, ttl_6
        self.mot_coils = mot_coils.MOTCoils(self) # ttl_2,  zotino_0

        ## Saved/ tunable attribute building
        ## Scannable attributes for calibration

        ## Single attributes, not scanned
        
        # MOT params
        self.setattr_argument("mot_current", NumberValue(default=4., ndecimals=5, step=0.1))
        self.setattr_argument("mot_detuning_gamma", NumberValue(default=-3., ndecimals=5, step=0.1))
        self.setattr_argument("mot_aom_amp_V", NumberValue(default=0.18, ndecimals=5, step=0.1))
        self.setattr_argument('mot_loading_time_s', NumberValue(default=0.5, ndecimals=2, step=0.1))

        # 2nd stage MOT params
        self.setattr_argument("sec_stage_mot_current_A", NumberValue(default=4., ndecimals=5, step=0.1))
        self.setattr_argument("sec_stage_mot_detuning_gamma", NumberValue(default=-3., ndecimals=5, step=0.1))
        self.setattr_argument("sec_stage_mot_aom_amp_V", NumberValue(default=0.18, ndecimals=5, step=0.1))
        self.setattr_argument('sec_stage_mot_loading_time_ms', NumberValue(default=10, ndecimals=2, step=1))
        

        # image timing params
        self.setattr_argument('imag_pulse_shutter_delay_ms', NumberValue(default=20, ndecimals=1, step=1))
        self.setattr_argument('N_t_points', NumberValue(default=50, ndecimals=0, step=1))

        # camera imaging params
        self.setattr_argument('camera_gain_dB', NumberValue(default=5, ndecimals=0, step=1))
        self.setattr_argument('camera_t_exp_ms', NumberValue(default=1.5, ndecimals=3, step=1))
        self.setattr_argument('cam_roi_x_0', NumberValue(default=704, ndecimals=0, step=1))
        self.setattr_argument('cam_roi_y_0', NumberValue(default=550, ndecimals=0, step=1))
        self.setattr_argument('cam_roi_w_x', NumberValue(default=80, ndecimals=0, step=1))
        self.setattr_argument('cam_roi_w_y', NumberValue(default=80, ndecimals=0, step=1))

        # other params
        self.setattr_argument("Nshots", NumberValue(default=5, ndecimals=0, step=1))
        self.setattr_argument('isotope', NumberValue(default=48, ndecimals=0, step=1))
        self.setattr_argument('N_power_samples', NumberValue(default=50, ndecimals=0, step=1))

        ## Other attributes, non-saved
        self.a = 1 # random thing that must be set here
        self.camera_trigger_delay = 20 * us
        self.op_shutter_delay = 3*ms
        self.mot_shutter_delay = 2*ms
        self.op_shutter_on_delay = 3*ms
        self.mot_shutter_on_delay = 1.5*ms
        self.shutter_delay_delta = self.op_shutter_delay - self.mot_shutter_delay

    def prepare(self):
        #set up imaging ROI
        self.roi = [int(self.cam_roi_x_0), int(self.cam_roi_y_0),
                    int(self.cam_roi_x_0) + int(self.cam_roi_w_x),
                    int(self.cam_roi_y_0) + int(self.cam_roi_w_y)]

        # pull the atomic frequencies from global params
        self.cooling_freq = global_parameters.cooling_line_freq[str(self.isotope)]
        self.op_freq = global_parameters.op_line_freq[str(self.isotope)]

        # calculate the detuning that the lasers should be locked to on the wavemeter
        self.vexlum_freq_mot = (self.cooling_freq # set Vexlum frequency where there will be maximum AOM efficiency at nom MOT detuning
                                + (self.mot_detuning_gamma * global_parameters.cooling_gamma_linear
                                    -  2 * self.mot_beams.ch2_center_freq) / 10**12)
        self.sec_stage_aom_freq = ((self.mot_detuning_gamma - self.sec_stage_mot_detuning_gamma) * 
                                    (global_parameters.cooling_gamma_linear / 2) + 
                                    self.mot_beams.ch2_center_freq)

        # track shape and size of expected data arrays
        self.n_imags = self.Nshots * self.N_t_points
        self.data_shape = (self.Nshots, self.N_t_points)

        # set up arrays to save measured sampler data into, fitting it into properly sized arrays
        self.measured_mot_power = np.zeros((int(self.Nshots), int(self.N_power_samples)))
        self.measured_mot_power_err = np.zeros((int(self.Nshots), int(self.N_power_samples)))
        self.measured_bright_power = np.zeros(int(self.N_power_samples))
        self.measured_bright_power_err = np.zeros(int(self.N_power_samples))

    @kernel(flags={"fast-math"})
    def get_mean_and_std(self, arr):# -> TList(TFloat):
        meanN = 0.0
        stdNS = 0.0
        N = len(arr)

        #mean
        for a in arr:
            meanN += a

            mean = meanN/N

        #std
        for a in arr:
            stdNS += (a-mean)**2

            std = np.sqrt(stdNS/N)

        return mean, std

    @kernel
    def run(self):

        self.a
        self.core.wait_until_mu(now_mu())
        self.core.reset()

        ######## INITIALIZATIONS ########

        ### wrapped initializations
        self.absorption_beam.init()
        self.mot_beams.init()
        self.mot_coils.init()
        self.mot_coils.set_current(self.mot_current)

        ### ttl initialization
        self.ttl4.output()
        self.ttl5.output()

        delay(1 * ms)
        self.ttl4.off() # Camera TTL low
        delay(1 * ms)
        self.ttl5.on() # OP shutter open

        ### Sampler initialization
        self.core.break_realtime()
        self.sampler0.init()
        self.sampler0.set_gain_mu(0,0)
        self.sampler0.set_gain_mu(1,0)
        self.sampler0.set_gain_mu(2,0)

        ### thorcam initialization
        self.thorcam1.connect()
        self.thorcam1.initialize(experiment=self,
                                exposure_time_us = int(self.camera_t_exp_ms) * 1000,
                                operation_mode = 1,
                                frames_per_trigger_zero_for_unlimited = int(self.N_t_points),
                                image_poll_timeout_ms = 1500,
                                gain = int(self.camera_gain_dB) * 10,
                                roi = self.roi,
                                trigger_polarity = 0, # 0 for rising edge 1 for falling edge
                                wavelength_nm = 498,
                                )
        self.core.break_realtime()


        ##########################
        ### MAIN LOOP

        ### tune wavelength locks on wavemeter
        print("Tuning lasers for bright background...")
        self.wlm0.SetPIDCourseNum(5, self.vexlum_freq_mot)
        self.wlm0.SetPIDCourseNum(3, self.op_freq)
        self.core.break_realtime()
        self.core.wait_until_mu(now_mu())

        self.thorcam1.arm(int(self.N_t_points * (self.Nshots + 2)))
        self.core.break_realtime()
        delay(5 * ms)
        # Should above line be done in a while loop to check the aom freq?

        ##########################
        ### Darkfield img
        print("Taking darkfield bg")
        self.ttl5.off() # op beams off
        delay(self.shutter_delay_delta)
        self.mot_beams.turn_off() # MOT beams off
        delay(1 * ms)
        self.mot_coils.turn_off() # MOT coils off

        ### take N_t_points images
        self.ttl4.pulse(100 * us) 
        delay(self.mot_loading_time_s * s)

        ##########################
        ### Brightfield img

        print("Taking brightfield bg")
        
        ### turn on op and mot beams, with B field off
        self.mot_beams.set_aom(self.sec_stage_aom_freq, # tune to second stage MOT freq/ amp
                                self.sec_stage_mot_aom_amp_V)
        delay(0.2 * ms)
        self.ttl5.on() # op beams on
        delay(self.shutter_delay_delta)
        self.mot_beams.turn_on()
        delay(self.mot_shutter_delay)

        ### take brightfield pd cal measurement
        # smp_arr = [0.0] * int(self.N_power_samples)
        # delay(1*ms)
        # for j in range(int(self.N_power_samples)):
        #     smp = [0.0] * 8
        #     delay(2 * ms)
        #     self.sampler0.sample(smp)
        #     smp_arr[j] = smp[0]
        # delay(1 * ms)
        # self.brightfield_pwr_mon, self.brightfield_pwr_mon_err = self.get_mean_and_std(smp_arr)
        # self.core.break_realtime()

        ### take N_t_points images for brightfield
        self.ttl4.pulse(100 * us)
        delay(self.mot_loading_time_s * s)

        img_count = 0
        ####################
        ##### Signal images

        ### take mot pd cal measurement
        # self.mot_beams.turn_on()
        # smp_arr = [0.0] * int(self.N_power_samples)
        # delay(1 * ms)
        # for j in range(int(self.N_power_samples)):
        #     smp = [0.0] * 8
        #     delay(2 * ms)
        #     self.sampler0.sample(smp)
        #     smp_arr[j] = smp[0]
        # delay(1 * ms)
        # self.mot_pwr_mon, self.mot_pwr_mon_err = self.get_mean_and_std(smp_arr)
        # self.core.break_realtime()

        ### Load N_shots MOTs and record their loading and decay
        #scott wrote up to here
        for j in range(int(self.Nshots)):

            # load the first stage MOT
            self.mot_beams.set_aom(self.mot_beams.ch2_center_freq, # tune to first stage MOT freq/ amp
                                    self.mot_aom_amp_V)
            delay(0.5 * ms)
            self.mot_beams.turn_on()
            delay(0.5 * ms)
            self.mot_coils.turn_on()
            delay(0.5 * ms)
            # self.ttl4.pulse(100*us) ## trigger camera for loading rate
            # delay(self.imag_pulse_shutter_delay_ms * ms) # wait to set a baseline
            self.ttl5.on() # op beams on
            delay(self.mot_loading_time_s * s) # load MOT

            # Jump to second stage MOT and take images
            self.ttl4.pulse(100 * us) ## trigger camera for lifetime
            delay(self.imag_pulse_shutter_delay_ms * ms) # wait to set a baseline
            with parallel:
                self.ttl5.off() # op beams off
                self.mot_beams.set_aom(self.sec_stage_aom_freq, # tune to second stage MOT freq/ amp
                                        self.sec_stage_mot_aom_amp_V)
                delay(self.mot_loading_time_s * s) # load MOT

        ### Readout from the camera
        self.thorcam1.poll_for_frame_and_append_to_buffer(['bright'], self.N_t_points)
        self.thorcam1.poll_for_frame_and_append_to_buffer(['dark'], self.N_t_points)
        for j in range(self.Nshots):
            self.thorcam1.poll_for_frame_and_append_to_buffer(['signal'], self.N_t_points)

        ### Reset hardware for end of run
        self.core.break_realtime()
        self.ttl5.on() # op beams on
        delay(1 * ms)
        self.mot_beams.set_aom(self.mot_beams.ch2_center_freq, self.mot_beams.ch2_amp)
        delay(1 * ms)
        self.mot_beams.turn_on() # MOT beams on
        delay(1 * ms)
        self.mot_coils.turn_on() # MOT coils on
        print("Done with run stage.")

    def analyze(self):
        print("Saving...")
        # save images and read them into arrays
        self.thorcam1.save_all(self)
        img_shape = self.thorcam1.get_img_shape()
        img_shape = (img_shape[0] + 1, img_shape[1] + 1)

        print('reading dark')
        dark, dark_err = self.thorcam1.get_buffer_and_buffer_errs('dark')
        print('reading bright')
        bright, bright_err = self.thorcam1.get_buffer_and_buffer_errs('bright')
        print('reading lifetime signal')
        signal, signal_err = self.thorcam1.get_buffer_and_buffer_errs("signal")

        # save samples
        # self.set_dataset("mot_pwr_mon", self.mot_pwr_mon)
        # self.set_dataset("mot_pwr_mon_err", self.mot_pwr_mon_err)
        # self.set_dataset("brightfield_pwr_mon", self.brightfield_pwr_mon)
        # self.set_dataset("brightfield_pwr_mon_err", self.brightfield_pwr_mon_err)

        # average images for plotting/ analysis
        dark_mean = np.mean(np.array(dark), axis=0)
        dark_mean_err = np.sqrt(np.sum(np.array(dark_err)**2, axis=0) / int(self.Nshots))
        dark_std = np.std(np.array(dark), axis=0)
        
        bright_mean = np.mean(np.array(bright), axis=0)
        bright_mean_err = np.sqrt(np.sum(np.array(bright_err)**2, axis=0) / int(self.Nshots))
        bright_std = np.std(np.array(bright), axis=0)

        singal_array = np.reshape(signal,
                                    (int(self.Nshots), int(self.N_t_points), img_shape[0], img_shape[1]))
        signal_err_array = np.reshape(signal_err,
                                        (int(self.Nshots), int(self.N_t_points), img_shape[0], img_shape[1]))
        signal_mean = np.mean(np.array(signal_array), axis=0)
        signal_mean_err = np.sqrt(np.sum(np.array(signal_err_array)**2, axis=0) / int(self.Nshots))
        signal_std = np.std(np.array(signal_array), axis=0)

        # Plot images for spot checking
        fig = plt.figure(figsize=(2 * 3, 2))
        grid = ImageGrid(fig, 111, cbar_mode = 'single', share_all = True,
                        nrows_ncols = (1 , 3),
                        )
        clim = (0, 1024)
        for ax, im in zip(grid, [dark_mean, bright_mean, signal_mean[0,:,:]]): 
            im = ax.imshow(im, cmap='GnBu_r', clim=clim)
        ax.cax.colorbar(im)
        fig.suptitle('Average Dark, Bright, and Initial signal')
        plt.tight_layout()
        plt.show()

        # Process images in a basic manner
        bg_sub_sig = signal_mean - np.tile(bright_mean, (int(self.N_t_points, 1, 1)))
        bg_sub_sig_err = np.sqrt(signal_err**2 + np.tile(bright_err**2, (int(self.N_t_points, 1, 1))))

        ### scott wrote up to here

        lifetime_counts = np.sum(bg_sub_lifetime_sig, axis=(-2, -1))
        lifetime_counts_err = np.sqrt(np.sum(bg_sub_lifetime_sig_err**2, axis=(-2, -1)))

        # plot loading and lifetime fluor counts
        ### FIX TIME AXIS!
        t_ax = np.arage(-int(self.imag_pulse_shutter_delay_ms),
                        int(self.N_t_points) * self.camera_t_exp_ms - int(self.imag_pulse_shutter_delay_ms),
                        int(self.N_t_points))

        fig, axs = plt.subplots(ncols=2,figsize=(10,4), sharey=True)

        mec_list = ['darkslategrey', 'maroon', 'indigo', 'black', 'darkgoldenrod', 'darkolivegreen']
        mfc_list = ['darkturquoise', 'indianred', 'darkorchid', 'grey', 'gold', 'greenyellow']
        markers = ['o','s', 'D','^', '*', 'p']

        ax[0].errorbar(t_ax,
                    load_counts,
                    yerr = load_counts_err,
                    ls='', marker=markers[0],
                    ecolor=mec_list[0],
                    mec=mec_list[0],
                    mfc=mfc_list[0])
        ax[1].errorbar(t_ax,
                    lifetime_counts,
                    yerr = lifetime_counts_err,
                    ls='', marker=markers[0],
                    ecolor=mec_list[0],
                    mec=mec_list[0],
                    mfc=mfc_list[0])
        ax[0].set_xlabel(r'$t_{\mathrm{OP shutter open}}}$ [ms]')
        ax[1].set_xlabel(r'$t_{\mathrm{OP shutter closed}}}$ [ms]')
        ax[0].set_ylabel('Fluor. counts [arb.]')
        
        plt.tight_layout()
        plt.show()


        print("Analyze stage done.")