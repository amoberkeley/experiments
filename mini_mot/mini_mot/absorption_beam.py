from artiq.experiment import *
import numpy as np

class AbsorptionBeam(HasEnvironment):

	def build(self, aom_amp = 0.18, aom_freq = 200 * MHz):
		self.setattr_device('core')
		self.setattr_device('ttl3') #abs beam SRS shutter. 
		self.setattr_device("urukul0_ch1") #double pass AOM +1 order

		self.ch1_amp = aom_amp #higher power at 0.7
		self.ch1_center_freq = aom_freq
		self.ch1_attenuation = 0.0 * dB

		self.aom_off_delay = 50 * us # measure this!!!
		self.aom_on_delay = 50 * us # measure this!!!

		self.shutter_closing_delay = 5 * ms
		self.shutter_opening_delay = 5 * ms

		self.shutter_min_exposure = 5 * ms

		self.pulse_delay = self.aom_off_delay + self.shutter_opening_delay

	@kernel
	def init(self):

		self.core.break_realtime()
		delay(10 * ms)
		self.urukul0_ch1.cpld.init()
		self.core.break_realtime()
		delay(10 * ms)
		self.urukul0_ch1.init()
		self.core.break_realtime()
		delay(10 * ms)

		self.ttl3.output()

		self.urukul0_ch1.set_att(self.ch1_attenuation)
		self.urukul0_ch1.set(self.ch1_center_freq, amplitude = self.ch1_amp)

		## turn on to thermalize, with shutter closed
		self.ttl3.off()
		delay(self.shutter_closing_delay)
		self.urukul0_ch1.sw.on()

	# @kernel
	# def fm(self, data):
	# 	self.urukul0_ch0.frequency_to_ram(frequency: list(elt=float), ram: list(elt=numpy.int32))
	# 	self.urukul0_ch0.set_phase_mode(phase_mode: numpy.int32)
	# 	self.urukul0_ch0.set_profile_ram(start: numpy.int32, end: numpy.int32, step: numpy.int32 = 1, profile: numpy.int32 = 0, nodwell_high: numpy.int32 = 0, zero_crossing: numpy.int32 = 0, mode: numpy.int32 = 1)

	@kernel
	def get_freq(self):
		return self.urukul0_ch1.get_frequency()

	@kernel
	def get_amp(self):
		return self.urukul0_ch1.get_amplitude()

	@kernel
	def set_aom(self, freq, amp):
		'''
		takes freq in Hz and amplitude in volts and sets the AOM parameters
		'''
		self.urukul0_ch1.set(frequency = freq, amplitude = amp)

	@kernel
	def set_aom_freq(self, freq):
		'''
		takes the frequency in Hz and sets the AOM
		'''
		a = self.get_amp()
		delay(1 * ms)
		self.set_aom(freq, a)

	@kernel
	def set_aom_amp(self, amp):
		'''
		takes the frequency in Hz and sets the AOM
		'''
		f = self.get_freq()
		delay(50 * ms)
		self.set_aom(f, amp)

	# @kernel
	# def open_shutter(self):
	# 	'''
	# 	Opens the shutter (beware the delay time!)
	# 	'''
	# 	self.urukul0_ch1.set(freq, amp)

	# @kernel
	# def close_shutter(self):
	# 	'''
	# 	takes freq in Hz and amplitude in volts and sets the AOM parameters
	# 	'''
	# 	self.urukul0_ch1.set(freq, amp)

	# @kernel
	# def set_delta(self, delta):
	# 	'''
	# 	takes delta in MHz, tunes the two AOMs so that the net shift
	# 	on the fundemental after going thru the daisy chained double passes
	# 	is delta in MHz.
	# 	'''
	# 	# print("test set delta: ", delta)
	# 	ch1_freq = self.ch1_center_freq + delta * MHz / 2
	# 	# print('got here')
	# 	delay(10*ms)
	# 	self.urukul0_ch1.set(ch1_freq, amplitude = self.ch1_amp)

	@kernel
	def turn_off(self):
		'''
		basic function for turning off the imaging beam in a way
		that preserves the AOM RF power for thermal management
		'''
		self.urukul0_ch1.sw.off()
		delay(self.aom_off_delay)
		self.ttl3.off()
		delay(self.shutter_closing_delay)
		self.urukul0_ch1.sw.on()

	@kernel
	def turn_on(self):
		'''
		basic function for turning on the imaging beam in a way
		that preserves the AOM RF power for thermal management
		'''
		self.urukul0_ch1.sw.off()
		delay(self.aom_off_delay)
		self.ttl3.on()
		delay(self.shutter_opening_delay)
		self.urukul0_ch1.sw.on()		

	@kernel
	def pulse(self, pulse_len, pulse_delay = 0.0 * us):
		'''
		function that pulses on the imaging light for a set length of time,
		with a optional delay time. 
		NOTE: this function has a delay of self.aom_off_delay + self.shutter_opening_delay
				before the AOM comes on. This delay can be called from self.pulse_delay,
				but you must call this function that length of time BEFORE you want the light
				to actually turn on
		'''
		if pulse_len > 2.0 * self.shutter_min_exposure:
			self.turn_on()
			delay(pulse_len)
			self.turn_off()
		else:
			self.urukul0_ch1.sw.off()
			delay(self.aom_off_delay)
			self.ttl3.on()
			with parallel:
				with sequential:
					delay(self.shutter_opening_delay)
					self.urukul0_ch1.sw.on()
					delay(pulse_len)
					self.urukul0_ch1.sw.off()
					delay(self.aom_off_delay)
				with sequential:
					delay(self.shutter_min_exposure * 2.0)
					self.ttl3.off()
					delay(self.shutter_closing_delay)
			self.urukul0_ch1.sw.off()

	@kernel
	def absorp_pulse(self, pulse_len, pulse_delay, pulse_freq, pulse_amp):
		'''
		function that pulses on the imaging light for a set length of time,
		with a optional delay time. 
		NOTE: this function has a delay of self.aom_off_delay + self.shutter_opening_delay
				before the AOM comes on. This delay can be called from self.pulse_delay,
				but you must call this function that length of time BEFORE you want the light
				to actually turn on
		'''

		#set up the AOM for the imag pulse
		with parallel:
			with sequential:
				self.urukul0_ch1.sw.off()# turn off imaging beam
				delay(self.aom_off_delay)
				self.ttl3.on() # open the shutter
			with sequential:
				delay(5 * us)
				self.set_aom(pulse_freq, pulse_amp)
				delay(5 * us)


		with parallel:
			#perform imaging pulse
			with sequential:
				delay(self.shutter_opening_delay + pulse_delay)
				self.urukul0_ch1.sw.on()
				delay(pulse_len)
				self.urukul0_ch1.sw.off()
				delay(self.aom_off_delay)

			#properly handle closing shutter and reseting AOM for thermal management
			with sequential:
				delay(self.shutter_opening_delay + pulse_delay
						+ self.shutter_min_exposure + pulse_len)
				self.ttl3.off()
				delay(self.shutter_closing_delay)
				delay(10 * us)
				self.set_aom(self.ch1_center_freq, self.ch1_amp)
				delay(10 * us)
				self.urukul0_ch1.sw.on()


	@kernel
	def op_then_absorp_pulse(self, op_pulse_len, op_pulse_delay, op_pulse_freq, op_pulse_amp,
							pulse_len, pulse_delay, pulse_freq, pulse_amp):
		'''
		function that pulses on the imaging light for a set length of time twice,
		with two delay times. The first pulse is used for optical pumping, while the
		second should be done with the correct imaging parameters. 
		NOTE: this function has a delay of self.aom_off_delay + self.shutter_opening_delay
				before the AOM comes on. This delay can be called from self.pulse_delay,
				but you must call this function that length of time BEFORE you want the light
				to actually turn on
		'''
		self.urukul0_ch1.sw.off() # turn off imaging beam
		
		#set up the AOM for the OP pulse
		with parallel:
			delay(self.aom_off_delay)
			with sequential:
				delay(10 * us)
				self.set_aom(op_pulse_freq, op_pulse_amp)
				delay(10 * us)
		self.ttl3.on() # open the shutter
		with parallel:
			
			#perform AOM pulses
			with sequential:
				#perform OP pulse
				delay(self.shutter_opening_delay + op_pulse_delay)
				self.urukul0_ch1.sw.on()
				delay(op_pulse_len)
				self.urukul0_ch1.sw.off()

				#set up the AOM for the imaging pulse
				with parallel:
					delay(pulse_delay)
					with sequential:
						delay(10 * us)
						self.set_aom(pulse_freq, pulse_amp)
						delay(10 * us)

				#perform imaging pulse
				self.urukul0_ch1.sw.on()
				delay(pulse_len)
				self.urukul0_ch1.sw.off()

			#properly handle closing shutter and reseting AOM for thermal management
			with sequential:
				delay(self.shutter_min_exposure * 2.0)
				self.ttl3.off()
				with parallel:
					delay(self.shutter_closing_delay)
					with sequential:
						delay(10 * us)
						self.set_aom(self.ch1_center_freq, self.ch1_amp)
						delay(10 * us)
		self.urukul0_ch1.sw.on()
