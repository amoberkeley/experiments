from artiq.experiment import *
import numpy as np

class MOTCoils(HasEnvironment):

    def build(self, default_curr = 4.0):
        self.setattr_device("core")
        self.setattr_device("zotino0")
        self.setattr_device('ttl2')
        self.zotino0_ch_number = 0 # Hard coded here, make sure zotino0 ch0 is controlling coils.
        self.amps_to_volts = 0.1 # convert between desired current on coils to voltage required on zotino
        self.default_curr = default_curr
    
    @kernel
    def set_current(self, current):
        voltage = self.amps_to_volts*current

        ## clamping the coil current to between 0 and 4.5 amps
        if voltage < 0:
            voltage = 0.0
        if voltage > 0.45:
            voltage = 0.45
        delay(5 * ms)
        self.zotino0.set_dac([voltage], [self.zotino0_ch_number])
        delay(5 * ms)

    @kernel
    def init(self):
        # self.core.reset()
        with parallel:
            with sequential:
                self.zotino0.init()
                # delay(5 * ms)
                self.set_current(self.default_curr)
                # delay(5 * ms)
            with sequential:
                delay(1 * ms)
                self.ttl2.output()
                delay(1 * ms)
                self.ttl2.off() # MOT coils on
        self.core.break_realtime()


    @kernel
    def turn_on(self):
        self.ttl2.off()

    @kernel
    def turn_off(self):
        self.ttl2.on()

    # @kernel
    # def record_dma(self, dma_handle_name):
    #   with self.core_dma.record(dma_handle_name):
    #       for i in range(100):
    #           delay(1.5*us)
    #           self.dac.set_dac([i/10],[self.zotino0_ch_number])
    #       self.dac.set_dac_mu([0],[7])

    # @kernel
    # def dma_current_ramp(self, dma_handle_name):

    #   dma_handle = self.core_dma.get_handle(dma_handle_name)

    # @kernel
    # def ramp(self):
    #   '''from https://forum.m-labs.hk/d/721-driving-a-ramp-wave-into-multiple-channels/2
    #   '''
    #   n_steps = 100
    #   voltages_mu = [((1<<16)//n_steps)*i for in in range(n_steps)]

    #   while(True):
    #       for vmu in voltages_mu:
    #           self.zotino0.write_dac_mu(0, voltage)
    #           self.zotino0.load()
    #           delay(7*us)