from artiq.experiment import *

class TimestampSingleton(HasEnvironment):
    _sing = None  # type: __Singleton
    _rid = None
 
    class __Singleton:
        def __init__(self):
            self.start_times_mu = {}
            self.start_timestamps = {}
 
    def build(self):
        self.setattr_device("core")
 
    def __init__(self, mgr, rid):
 
        super().__init__(mgr)
        TimestampSingleton._sing = TimestampSingleton.__Singleton()
        self._rid = rid
 
    @host_only
    def sync(self):
 
        TimestampSingleton._sing.start_times_mu[self._rid] = self.core.get_rtio_counter_mu()
        TimestampSingleton._sing.start_timestamps[self._rid] = time.time()