from artiq.experiment import *
import numpy as np

class MOTBeams(HasEnvironment):

	def build(self, aom_amp = 0.18, aom_freq = 200 * MHz):
		self.setattr_device('core')
		self.setattr_device('ttl6') #MOT Uniblitz shutter. Channel 1 of experiment table SRS box
		self.setattr_device("urukul0_ch2") #MOT AOM
		self.setattr_device("sampler0") #Sampler initialization
		self.sampler_channel = 0

		self.ch2_amp = aom_amp
		self.ch2_center_freq = aom_freq
		self.ch2_attenuation = 0.0 * dB

		self.aom_off_delay = 50 * us #measure this!!
		self.aom_on_delay = 50 * us #measure this!!

		self.shutter_closing_delay = 5 * ms # measure this!!
		self.shutter_opening_delay = 5 * ms # measure this!!
		self.shutter_min_exposure = 3 * 2.5 * ms # measure this!!

		self.pulse_delay = self.aom_off_delay + self.shutter_opening_delay

	@kernel
	def init(self):

		self.core.break_realtime()
		
		self.urukul0_ch2.cpld.init()
		self.urukul0_ch2.init()

		self.ttl6.output()

		self.urukul0_ch2.set_att(self.ch2_attenuation)
		self.urukul0_ch2.set(self.ch2_center_freq, amplitude = self.ch2_amp)

		self.core.break_realtime()
		## turn on to thermalize, with shutter closed
		self.ttl6.off()
		delay(self.shutter_closing_delay)
		self.urukul0_ch2.sw.on()

	# @kernel
	# def fm(self, data):
	# 	self.urukul0_ch0.frequency_to_ram(frequency: list(elt=float), ram: list(elt=numpy.int32))
	# 	self.urukul0_ch0.set_phase_mode(phase_mode: numpy.int32)
		# self.urukul0_ch0.set_profile_ram(start: numpy.int32,
		# 								end: numpy.int32,
		# 								step: numpy.int32(1),
		# 								profile: numpy.int32 = 0,
		# 								nodwell_high: numpy.int32 = 0,
		# 								zero_crossing: numpy.int32 = 0,
		# 								mode: numpy.int32 = 1)

	@kernel
	def get_freq(self):
		return self.urukul0_ch2.get_frequency()

	@kernel
	def get_amp(self):
		return self.urukul0_ch2.get_amplitude()

	@kernel
	def set_aom(self, freq, amp):
		'''
		takes freq in Hz and amplitude in volts and sets the AOM parameters
		'''
		self.urukul0_ch2.set(frequency = freq,
							amplitude = amp)

	@kernel
	def set_aom_freq(self, freq):
		'''
		takes the frequency in Hz and sets the AOM
		'''
		a = self.get_amp()
		delay(1 * ms)
		self.set_aom(freq, a)

	@kernel
	def set_aom_amp(self, amp):
		'''
		takes the frequency in Hz and sets the AOM
		'''
		f = self.get_freq()
		delay(1 * ms)
		self.set_aom(f, amp)

	# @kernel
	# def set_delta(self, delta):
	# 	'''
	# 	takes delta in MHz, tunes the AOM so that the net shift
	# 	on the fundemental after going thru the daisy chained double passes
	# 	is delta in MHz.
	# 	'''
	# 	# print("test set delta: ", delta)
	# 	ch2_freq = self.ch2_center_freq + delta * MHz / 2
	# 	self.urukul0_ch2.set(ch2_freq, amplitude = self.ch0_amp)

	@kernel
	def turn_off(self):
		'''
		basic function for turning off the imaging beam in a way
		that preserves the AOM RF power for thermal management
		'''
		self.urukul0_ch2.sw.off()
		delay(self.aom_off_delay)
		self.ttl6.off()
		delay(self.shutter_closing_delay)
		self.urukul0_ch2.sw.on()

	@kernel
	def turn_on(self):
		'''
		basic function for turning on the imaging beam in a way
		that preserves the AOM RF power for thermal management
		'''
		self.urukul0_ch2.sw.off()
		delay(self.aom_off_delay)
		self.ttl6.on()
		delay(self.shutter_opening_delay)
		self.urukul0_ch2.sw.on()	

	@kernel
	def pulse_on(self, pulse_len):
		'''
		function that pulses on the imaging light for a set length of time
		NOTE: this function has a delay of self.aom_off_delay + self.shutter_opening_delay
				before the AOM comes on. This delay can be called from self.pulse_delay,
				but you must call this function that length of time BEFORE you want the light
				to actually turn on
		'''
		if pulse_len > 2.0 * self.shutter_min_exposure:
			self.turn_on()
			delay(pulse_len)
			self.turn_off()
		else:
			self.urukul0_ch2.sw.off()
			delay(self.aom_off_delay)
			self.ttl6.on()
			with parallel:
				with sequential:
					delay(self.shutter_opening_delay)
					self.urukul0_ch2.sw.on()
					delay(pulse_len)
					self.urukul0_ch2.sw.off()
					delay(self.aom_off_delay)
				with sequential:
					delay(self.shutter_min_exposure * 2.0)
					self.ttl6.off()
					delay(self.shutter_closing_delay)
			self.urukul0_ch2.sw.on()


	@kernel
	def pulse_off(self, pulse_len):
		'''
		function that pulses on the imaging light for a set length of time
		NOTE: this function has a delay of self.aom_off_delay + self.shutter_opening_delay
				before the AOM comes on. This delay can be called from self.pulse_delay,
				but you must call this function that length of time BEFORE you want the light
				to actually turn on
		'''
		if pulse_len > 2.0 * self.shutter_min_exposure:
			self.turn_off()
			delay(pulse_len - 2.0 * self.pulse_delay)
			self.turn_on()
		else:
			self.urukul0_ch2.sw.off()
			delay(pulse_len - self.aom_off_delay + self.aom_on_delay)
			self.urukul0_ch2.sw.on()

	@kernel
	def fluor_imag_pulse(self, pulse_len, pulse_delay, pulse_freq, pulse_amp):
		'''
		Function that takes a fluorescence imaging pulse in the following steps:
			1. Turns MOT light off
			2. Switches AOM freq and amplitude from the MOT settings to the imaging settings
			3. Waits a set delay time
			4. performs a pulse of a given length
			5. switches back to the MOT AOM settings
			6. closes the shutter and turns back on the AOM for thermal management
		'''

		### comment from jack and anke: shouldnt we use self.pulse_delay here as it should be inate to the uniblitz being used? Why is it an argument in this function?
		with parallel:
			self.ttl6.on()
			with sequential:
				self.urukul0_ch2.sw.off()
				delay(10 * us)
				self.set_aom(pulse_freq, pulse_amp)
				delay(10 * us)
			delay(pulse_delay) #pulse delay must be greater than self.shutter_opening_delay?

		self.urukul0_ch2.sw.on()
		delay(pulse_len)
		self.urukul0_ch2.sw.off()
		
		delay(10 * us)
		with parallel:
			self.ttl6.off()
			with sequential:
				self.set_aom(self.ch2_center_freq, self.ch2_amp)
				delay(self.shutter_closing_delay)
				self.urukul0_ch2.sw.on()
