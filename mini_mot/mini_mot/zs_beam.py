from artiq.experiment import *
import numpy as np

class ZSBeam(HasEnvironment):

	def build(self, snglp_aom_amp = 0.18, snglp_aom_freq = 80 * MHz,
			  dblp_aom_amp = 0.18, dblp_aom_freq = 200 * MHz):
		self.setattr_device('core')
		self.setattr_device('ttl7') # ZS Uniblitz shutter. Channel 1 of near-Vexlum Uniblitz box
		self.setattr_device("urukul0_ch0") # ZS single pass AOM
		self.setattr_device("urukul0_ch3") # ZS double pass AOM
		# self.setattr_device("sampler0") #Sampler initialization
		# self.sampler_channel = 0

		self.ch0_amp = snglp_aom_amp
		self.ch0_center_freq = snglp_aom_freq
		self.ch0_attenuation = 0.0 * dB
		self.ch3_amp = dblp_aom_amp
		self.ch3_center_freq = dblp_aom_freq
		self.ch3_attenuation = 0.0 * dB

		self.aom_off_delay = 50 * us #measure this!!
		self.aom_on_delay = 50 * us #measure this!!

		self.shutter_closing_delay = 5 * ms # measure this!!
		self.shutter_opening_delay = 5 * ms # measure this!!
		self.shutter_min_exposure = 3 * 2.5 * ms # measure this!!

		self.pulse_delay = self.aom_off_delay + self.shutter_opening_delay

	@kernel
	def init(self):

		self.core.break_realtime()
		
		self.urukul0_ch0.cpld.init()
		self.urukul0_ch0.init()

		self.urukul0_ch3.cpld.init()
		self.urukul0_ch3.init()

		self.ttl7.output()

		self.urukul0_ch0.set_att(self.ch0_attenuation)
		self.urukul0_ch0.set(self.ch0_center_freq, 
							 amplitude = self.ch0_amp)

		self.urukul0_ch3.set_att(self.ch3_attenuation)
		self.urukul0_ch3.set(self.ch3_center_freq,
							 amplitude = self.ch3_amp)

		self.core.break_realtime()
		## turn on to thermalize, with shutter closed
		self.ttl7.off()
		delay(self.shutter_closing_delay)
		self.urukul0_ch0.sw.on()
		self.urukul0_ch3.sw.on()

	# @kernel
	# def fm(self, data):
	# 	self.urukul0_ch0.frequency_to_ram(frequency: list(elt=float), ram: list(elt=numpy.int32))
	# 	self.urukul0_ch0.set_phase_mode(phase_mode: numpy.int32)
		# self.urukul0_ch0.set_profile_ram(start: numpy.int32,
		# 								end: numpy.int32,
		# 								step: numpy.int32(1),
		# 								profile: numpy.int32 = 0,
		# 								nodwell_high: numpy.int32 = 0,
		# 								zero_crossing: numpy.int32 = 0,
		# 								mode: numpy.int32 = 1)

	@kernel
	def get_snglp_freq(self):
		return self.urukul0_ch0.get_frequency()

	@kernel
	def get_snglp_amp(self):
		return self.urukul0_ch0.get_amplitude()

	@kernel
	def set_snglp_aom(self, freq, amp):
		'''
		takes freq in Hz and amplitude in volts and sets the AOM parameters
		'''
		self.urukul0_ch0.set(frequency = freq,
							 amplitude = amp)

	@kernel
	def set_snglp_aom_freq(self, freq):
		'''
		takes the frequency in Hz and sets the AOM
		'''
		a = self.get_snglp_amp()
		delay(1 * ms)
		self.set_singlp_aom(freq, a)

	@kernel
	def set_snglp_aom_amp(self, amp):
		'''
		takes the frequency in Hz and sets the AOM
		'''
		f = self.get_snglp_freq()
		delay(1 * ms)
		self.set_singlp_aom(f, amp)

	@kernel
	def get_dblp_freq(self):
		return self.urukul0_ch3.get_frequency()

	@kernel
	def get_dblp_amp(self):
		return self.urukul0_ch3.get_amplitude()

	@kernel
	def set_dblp_aom(self, freq, amp):
		'''
		takes freq in Hz and amplitude in volts and sets the AOM parameters
		'''
		self.urukul0_ch3.set(frequency = freq,
							 amplitude = amp)

	@kernel
	def set_dblp_aom_freq(self, freq):
		'''
		takes the frequency in Hz and sets the AOM
		'''
		a = self.get_dblp_amp()
		delay(1 * ms)
		self.set_dblp_aom(freq, a)

	@kernel
	def set_dblp_aom_amp(self, amp):
		'''
		takes the frequency in Hz and sets the AOM
		'''
		f = self.get_dblp_freq()
		delay(1 * ms)
		self.set_dblp_aom(f, amp)

	# @kernel
	# def set_delta(self, delta):
	# 	'''
	# 	takes delta in MHz, tunes the AOM so that the net shift
	# 	on the fundemental after going thru the daisy chained double passes
	# 	is delta in MHz.
	# 	'''
	# 	# print("test set delta: ", delta)
	# 	ch2_freq = self.ch2_center_freq + delta * MHz / 2
	# 	self.urukul0_ch2.set(ch2_freq, amplitude = self.ch0_amp)

	@kernel
	def turn_off(self):
		'''
		basic function for turning off the imaging beam in a way
		that preserves the AOM RF power for thermal management
		'''
		with parallel:
			self.urukul0_ch0.sw.off()
			self.urukul0_ch3.sw.off()
		delay(self.aom_off_delay)
		self.ttl7.off()
		delay(self.shutter_closing_delay)
		with parallel:
			self.urukul0_ch0.sw.on()
			self.urukul0_ch3.sw.on()

	@kernel
	def turn_on(self):
		'''
		basic function for turning on the imaging beam in a way
		that preserves the AOM RF power for thermal management
		'''
		with parallel:
			self.urukul0_ch0.sw.off()
			self.urukul0_ch3.sw.off()
		delay(self.aom_off_delay)
		self.ttl7.on()
		delay(self.shutter_opening_delay)
		with parallel:
			self.urukul0_ch0.sw.on()
			self.urukul0_ch3.sw.on()

	@kernel
	def pulse_on(self, pulse_len):
		'''
		function that pulses on the imaging light for a set length of time
		NOTE: this function has a delay of self.aom_off_delay + self.shutter_opening_delay
				before the AOM comes on. This delay can be called from self.pulse_delay,
				but you must call this function that length of time BEFORE you want the light
				to actually turn on
		'''
		if pulse_len > 2.0 * self.shutter_min_exposure:
			self.turn_on()
			delay(pulse_len)
			self.turn_off()
		else:
			self.urukul0_ch3.sw.off()
			delay(self.aom_off_delay)
			self.ttl7.on()
			with parallel:
				with sequential:
					delay(self.shutter_opening_delay)
					self.urukul0_ch3.sw.on()
					delay(pulse_len)
					self.urukul0_ch3.sw.off()
					delay(self.aom_off_delay)
				with sequential:
					delay(self.shutter_min_exposure * 2.0)
					self.ttl7.off()
					delay(self.shutter_closing_delay)
			self.urukul0_ch3.sw.on()


	@kernel
	def pulse_off(self, pulse_len):
		'''
		function that pulses on the imaging light for a set length of time
		NOTE: this function has a delay of self.aom_off_delay + self.shutter_opening_delay
				before the AOM comes on. This delay can be called from self.pulse_delay,
				but you must call this function that length of time BEFORE you want the light
				to actually turn on
		'''
		if pulse_len > 2.0 * self.shutter_min_exposure:
			self.turn_off()
			delay(pulse_len - 2.0 * self.pulse_delay)
			self.turn_on()
		else:
			with parallel:
				self.urukul0_ch0.sw.off()
				self.urukul0_ch3.sw.off()
			delay(pulse_len - self.aom_off_delay + self.aom_on_delay)
			with parallel:
				self.urukul0_ch0.sw.on()
				self.urukul0_ch3.sw.on()

	@kernel
	def fluor_imag_pulse(self, pulse_len, pulse_delay, pulse_freq, pulse_amp):
		'''
		Function that takes a fluorescence imaging pulse in the following steps:
			1. Turns MOT light off
			2. Switches AOM freq and amplitude from the MOT settings to the imaging settings
			3. Waits a set delay time
			4. performs a pulse of a given length
			5. switches back to the MOT AOM settings
			6. closes the shutter and turns back on the AOM for thermal management
		'''

		### comment from jack and anke: shouldnt we use self.pulse_delay here as it should be inate to the uniblitz being used? Why is it an argument in this function?
		with parallel:
			self.ttl7.on()
			with sequential:
				with parallel:
					self.urukul0_ch0.sw.off()
					self.urukul0_ch3.sw.off()
				delay(10 * us)
				self.set_dblp_aom(pulse_freq, pulse_amp)
				delay(10 * us)
			delay(pulse_delay) #pulse delay must be greater than self.shutter_opening_delay?

		with parallel:
			self.urukul0_ch0.sw.on()
			self.urukul0_ch3.sw.on()
		delay(pulse_len)
		with parallel:
			self.urukul0_ch0.sw.off()
			self.urukul0_ch3.sw.off()
		
		delay(10 * us)
		with parallel:
			self.ttl7.off()
			with sequential:
				self.set_aom(self.ch3_center_freq, self.ch3_amp)
				delay(self.shutter_closing_delay)
				with parallel:
					self.urukul0_ch0.sw.on()
					self.urukul0_ch3.sw.on()
