from artiq.experiment import *
from mini_mot import absorption_beam, mot_beams, mot_coils
# from experiments import global_parameters
from global_params import mini_mot_global_params as global_parameters
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid

class AbsorpMOTLifetime(EnvExperiment):
    ### Meausres MOT lifetime with absorption imaging. Can also extract the density during the decay process
    
    ###
    
    def build(self):
        
        ## Device building
        self.setattr_device("core")
        # self.setattr_device("ttl2") # coil control
        self.setattr_device("ttl4") # mot camera 4/30/2024
        self.setattr_device("ttl5") # srs optical pumping shutter 4/30/2024
        self.setattr_device("wlm0") # wavemeter
        self.setattr_device("sampler0") # MOT power monitor
        self.setattr_device("thorcam1")
        self.absorption_beam = absorption_beam.AbsorptionBeam(self) # urukul0_0, urukul0_1, ttl_3
        self.mot_beams = mot_beams.MOTBeams(self) # urukul0_3, ttl_6
        self.mot_coils = mot_coils.MOTCoils(self) # ttl_2,  zotino_0

        ## Saved/ tunable attribute building
        ## Scannable attributes for calibration

        # image timing/ beam params
        self.setattr_argument("imag_delay_times_ms", Scannable(global_min=0.02,
                                                                global_max=1000,
                                                                ndecimals=3,
                                                                default=[RangeScan(1., 100., 10)]))


        ## Single attributes, not scanned

        # image params
        self.setattr_argument("imag_t_exp_us", NumberValue(default=5, ndecimals=1, step=1))
        self.setattr_argument("imag_aom_amp_V", NumberValue(default=0.18, ndecimals=3, step=0.01))
        self.setattr_argument("imag_detuning_gamma", NumberValue(default=0.0, ndecimals=2, step=0.1))
        
        # MOT params
        self.setattr_argument("mot_current_A", NumberValue(default=4., ndecimals=5, step=0.1))
        self.setattr_argument("mot_detuning_gamma", NumberValue(default=-3., ndecimals=5, step=0.1))
        self.setattr_argument("mot_aom_amp_V", NumberValue(default=0.18, ndecimals=5, step=0.1))
        self.setattr_argument('mot_loading_time_s', NumberValue(default=0.5, ndecimals=2, step=0.1))

        # 2nd stage MOT params
        self.setattr_argument("sec_stage_mot_current_A", NumberValue(default=4., ndecimals=5, step=0.1))
        self.setattr_argument("sec_stage_mot_detuning_gamma", NumberValue(default=-3., ndecimals=5, step=0.1))
        self.setattr_argument("sec_stage_mot_aom_amp_V", NumberValue(default=0.18, ndecimals=5, step=0.1))
        self.setattr_argument('sec_stage_mot_loading_time_ms', NumberValue(default=10, ndecimals=2, step=1))
        self.setattr_argument("sec_stage_mot_bool", BooleanValue(default=False))
        
        # camera params
        self.setattr_argument('camera_gain_dB', NumberValue(default=15, ndecimals=0, step=1))
        self.setattr_argument('camera_t_exp_ms', NumberValue(default=0.04, ndecimals=3, step=0.1))
        self.setattr_argument('cam_roi_x_0', NumberValue(default=740, ndecimals=0, step=1))
        self.setattr_argument('cam_roi_y_0', NumberValue(default=540, ndecimals=0, step=1))
        self.setattr_argument('cam_roi_w_x', NumberValue(default=80, ndecimals=0, step=1))
        self.setattr_argument('cam_roi_w_y', NumberValue(default=80, ndecimals=0, step=1))

        # other params
        self.setattr_argument("Nshots", NumberValue(default=5, ndecimals=0, step=1))
        self.setattr_argument('isotope', NumberValue(default=48, ndecimals=0, step=1))
        self.setattr_argument('num_power_samples', NumberValue(default=50, ndecimals=0, step=1))

        ## Fixed attributes, non-saved

        self.camera_trigger_delay = 20 * us
        self.mot_loading_time = 0.3 * s
        self.aom_thermalization_time = 4 * s
        self.num_power_samples = 50
        self.mot_sampler_channel = 0
        self.mot_coil_rampdown_time = 40 * us # how long to give the MOT coils and beams to turn off
                                                # before an absorption image is taken.
        self.a = 1 # random thing that must be set here

    def prepare(self):

         ## set up scan
        self.imag_delay_times = np.array([t * ms for t in self.imag_delay_times_ms])

        ## set up camera ROI
        self.roi = [int(self.cam_roi_x_0), int(self.cam_roi_y_0),
                    int(self.cam_roi_x_0 + self.cam_roi_w_x),
                    int(self.cam_roi_y_0 + self.cam_roi_w_y)]

        # pull the atomic frequencies from global params
        self.cooling_freq = global_parameters.cooling_line_freq[str(self.isotope)]
        self.op_freq = global_parameters.op_line_freq[str(self.isotope)]

        # calculate the detuning that the lasers should be locked to on the wavemeter
        self.vexlum_freq = (self.cooling_freq # set Vexlum freq where there will be max AOM efficiency at MOT dets
                            + (self.mot_detuning_gamma * global_parameters.cooling_gamma_linear
                                - 2 * self.mot_beams.ch2_center_freq) / 10**12) 
        self.sec_stage_mot_aom_freq = (((self.sec_stage_mot_detuning_gamma - self.mot_detuning_gamma) *
                                        (global_parameters.cooling_gamma_linear / 2)) + 
                                        self.mot_beams.ch2_center_freq)
        self.imag_aom_freq = (((self.imag_detuning_gamma - self.mot_detuning_gamma) *
                                (global_parameters.cooling_gamma_linear / 2)) + 
                                self.mot_beams.ch2_center_freq)
        print(self.imag_aom_freq*1e-6)

        # prep data shape and arrays
        self.data_shape = (len(self.imag_delay_times), self.Nshots)
        self.n_imags = len(self.imag_delay_times) * self.Nshots

    @kernel(flags={"fast-math"})
    def get_mean_and_std(self, arr):# -> TList(TFloat):
        meanN = 0.0
        stdNS = 0.0
        N = len(arr)

        #mean
        for a in arr:
            meanN += a

        mean = meanN/N

        #std
        for a in arr:
            stdNS += (a-mean)**2

        std = np.sqrt(stdNS/N)

        return mean, std

    @kernel
    def run(self):

        self.a # random thing that must be called here
        self.core.wait_until_mu(now_mu())
        self.core.reset()

        ######## INITIALIZATIONS ########

        ### wrapped initializations
        self.mot_beams.init()
        self.mot_coils.init()
        self.absorption_beam.init()

        ### ttl initialization
        # self.ttl2.output()
        self.ttl4.output()
        self.ttl5.output()

        # delay(1 * ms)
        # self.ttl2.off() # MOT coils on
        delay(1 * ms)
        self.ttl4.off() # Camera TTL low
        delay(1 * ms)
        self.ttl5.on() # OP shutter open

        ### Sampler initialization
        self.core.break_realtime()
        self.sampler0.init()
        self.sampler0.set_gain_mu(0,0) # check this!
        self.sampler0.set_gain_mu(1,0) # check this!
        self.sampler0.set_gain_mu(2,0) # check this!
        self.sampler0.set_gain_mu(3,0) # check this!

        ### thorcam initialization
        self.thorcam1.connect()
        self.thorcam1.initialize(experiment=self,
                                exposure_time_us = self.camera_t_exp_ms * 1000,
                                operation_mode = 1,
                                frames_per_trigger_zero_for_unlimited = 1,
                                image_poll_timeout_ms = 1500,
                                gain = self.camera_gain_dB * 10,
                                roi = self.roi,
                                trigger_polarity = 0, # 0 for rising edge 1 for falling edge
                                wavelength_nm = 498,
                                )
        self.core.break_realtime()

        img_count = 0
        missing_frame_count = 0
        shot_count = 0

        print("Tuning MOT beams...")
        self.wlm0.SetPIDCourseNum(3, self.op_freq)
        self.wlm0.SetPIDCourseNum(5, self.vexlum_freq)
        self.core.break_realtime()
        delay(0.5 * s)

        ### Loop through imaging delay times
        for t_delay in self.imag_delay_times:
            print(100 * float(img_count) / self.n_imags, ' percent done')
            
            ### take N shots for each delay time
            for i in range(self.Nshots):

            ### Add a check for missing frames - we only will loop a max of 10 times per frame
                missing_frame_check = 0
                while missing_frame_check == 0:
                    shot_count += 1
                    missing_sig_check = 0
                    missing_bright_check = 0
                    missing_dark_check = 0
                    self.core.break_realtime()

                    ### Arm the camera
                    self.thorcam1.arm(3)
                    self.core.break_realtime()
                    delay(0.2 * ms)

                    ### Load the MOT
                    self.mot_coils.set_current(self.mot_current_A)
                    delay(0.2 * ms)
                    self.mot_coils.turn_on()
                    delay(0.2 * ms)
                    self.ttl5.on() # OP shutter open
                    delay(0.2 * ms)
                    self.mot_beams.set_aom(self.mot_beams.ch2_center_freq, self.mot_beams.ch2_amp)
                    delay(0.2 * ms)
                    self.mot_beams.turn_on()
                    delay(0.2 * ms)
                    self.absorption_beam.turn_off()
                    with parallel:
                        smp_arr = [0.0] * self.num_power_samples
                        smp_arr_2 = [[0.0] * 8] * self.num_power_samples
                        delay(self.mot_loading_time_s)

                    ### if running a second MOT stage, do that
                    if self.sec_stage_mot_bool:
                        with parallel:
                            self.mot_beams.set_aom(self.sec_stage_mot_aom_freq,
                                                    self.sec_stage_mot_aom_amp_V)
                            self.mot_coils.set_current(self.sec_stage_mot_current_A)
                            delay(self.sec_stage_mot_loading_time_ms * ms)

                    ### Take a signal image after some decay time
                    with parallel:
                        #turn off OP light
                        #start absorption pulse with an appropriate delay
                        # if t_delay > 1.1 * self.absorption_beam.pulse_delay:
                        #     with sequential:
                        #         delay(t_delay - 1.1 * self.absorption_beam.pulse_delay)
                        #         self.absorption_beam.absorp_pulse(self.imag_t_exp_us * us,
                        #                                             0.1 * self.absorption_beam.pulse_delay,
                        #                                             self.imag_aom_freq,
                        #                                             self.imag_aom_amp_V)
                        # else:
                        self.absorption_beam.absorp_pulse(self.imag_t_exp_us * us,
                                                            t_delay,
                                                            self.imag_aom_freq,
                                                            self.imag_aom_amp_V)
                        # after appropriate delay, send imaging pulse to camera and turn off the MOT
                        with sequential:
                            delay(self.absorption_beam.pulse_delay)
                            self.ttl5.off()
                            delay(t_delay - self.camera_trigger_delay)
                            with parallel:
                                self.ttl4.pulse(100 * us)
                                self.mot_coils.turn_off() # MOT coils off
                                self.mot_beams.turn_off()

                    delay(0.5 * ms)
                    self.core.break_realtime()

                    ### Take a brightfield image
                    delay(50 * ms) # let atoms fly away
                    with parallel:
                        # after appropriate delay, send imaging pulse to camera
                        with sequential:
                            delay(self.absorption_beam.pulse_delay - self.camera_trigger_delay)
                            self.ttl4.pulse(100 * us)
                        self.absorption_beam.absorp_pulse(self.imag_t_exp_us * us,
                                                            0.0,
                                                            self.imag_aom_freq,
                                                            self.imag_aom_amp_V)
                    delay(20 * ms)
                    self.core.break_realtime()

                    ### Take a darkfield image
                    # self.mot_beams.turn_off()
                    # delay(10 * ms)
                    self.ttl4.pulse(100 * us)

                    # print("Polling camera for images...")
                    self.core.break_realtime()
                    delay(0.5 * ms)
                    self.thorcam1.poll_for_frame_and_append_to_buffer(['signal', 'bright', 'dark'], 3)
                    self.core.break_realtime()
                    self.thorcam1.disarm()
                    self.core.break_realtime()

                    ### handle missing frame by comparing against the expected number of images at this point
                    if self.thorcam1.get_buffer_len('signal') < img_count + 1:
                        missing_sig_check = 1
                        # print('missing sig frame')
                    if self.thorcam1.get_buffer_len('bright') < img_count + 1:
                        missing_bright_check = 1
                        # print('missing bright frame')
                    if self.thorcam1.get_buffer_len('dark') < img_count + 1:
                        missing_dark_check = 1
                        # print('missing dark frame')
                    if ((missing_sig_check == 0) and
                        (missing_bright_check == 0) and
                        (missing_dark_check == 0)):
                        missing_frame_check = 1
                    if missing_frame_check == 0:
                        print('missing frame caught, retaking image')
                        missing_frame_count += 1
                        if missing_sig_check == 0:
                            # print('popping extra sig frame')
                            self.thorcam1.pop_buffer('signal')
                        if missing_bright_check == 0:
                            # print('popping extra bright frame')
                            self.thorcam1.pop_buffer('bright')
                        if missing_dark_check == 0:
                            # print('popping extra dark frame')
                            self.thorcam1.pop_buffer('dark')
                    self.core.break_realtime()

                img_count += 1
                
        print("Done with run stage.")
        print('Missing frame count: ', missing_frame_count)
        print('Missing frame frac: ', float(missing_frame_count) / shot_count)

    def analyze(self):
        print("Saving...")
        self.thorcam1.save_all(self)
        img_shape = self.thorcam1.get_img_shape()
        data_array_shape = self.data_shape + (img_shape[0] + 1, img_shape[1] + 1)
        # print(data_array_shape)
        print('reading signals')
        signal, signal_err = self.thorcam1.get_buffer_and_buffer_errs('signal')
        # print(np.array(signal).shape)
        print('reading brights')
        bright, bright_err = self.thorcam1.get_buffer_and_buffer_errs('bright')
        print('reading darks')
        dark, dark_err = self.thorcam1.get_buffer_and_buffer_errs('dark')
        print('reshaping data')
        signal = np.reshape(np.array(signal), data_array_shape)
        # print(signal.shape)
        signal_err = np.reshape(np.array(signal_err), data_array_shape)
        bright = np.reshape(np.array(bright), data_array_shape)
        bright_err = np.reshape(np.array(bright_err), data_array_shape)
        dark = np.reshape(np.array(dark), data_array_shape)
        dark_err = np.reshape(np.array(dark_err), data_array_shape)

        #do background subtraction and calculate error in signal counts - check with Jack
        print('performing background subtraction')
        num = (signal - dark)
        denom = (bright - dark)

        num[num<=0] = 1
        denom[denom<=0]=1

        trans = num / denom # no way to meas. power diff. b/w bright & sig, assuming they are equal
        trans_err = trans * np.sqrt((signal_err**2 + dark_err**2 / (num)**2) +
                                    (bright_err**2 + dark_err**2 / (denom)**2))

        # trans[trans<0] = 1
        trans[trans>1] = 1
        trans_mean = np.mean(trans, axis=-3)
        # print(sig_bkg_sub_mean.shape)
        trans_err_mean = np.sqrt(np.sum(trans_err**2, axis=-3) / self.Nshots) 
        trans_std = np.std(trans, axis=-3)

        od_imag = -1 * np.log(trans)
        od_imag_err = np.abs(trans_err / trans)
        od_imag_mean = np.mean(od_imag, axis=-3)
        od_imag_mean_err = np.sqrt(np.sum(od_imag_err**2, axis=-3) / self.Nshots)
        od_imag_std = np.std(od_imag, axis=-3)

        self.set_dataset('od_imags_mean', od_imag_mean)
        self.set_dataset('od_imags_std', od_imag_std)

        sig_mean = np.mean(signal, axis=-3)
        sig_std = np.std(signal, axis=-3)
        sig_err_mean = np.sqrt(np.sum(signal_err**2, axis=-3) / self.Nshots)

        trans_imag_for_plotting = np.reshape(np.mean(trans, axis=-3),
                                            (int(self.n_imags / self.Nshots),
                                            data_array_shape[-2],
                                            data_array_shape[-1])
                                            )
        sig_imag_for_plotting = np.reshape(np.mean(signal, axis=-3),
                                            (int(self.n_imags / self.Nshots),
                                            data_array_shape[-2],
                                            data_array_shape[-1])
                                            )
        bright_imag_for_plotting = np.reshape(np.mean(bright, axis=-3),
                                            (int(self.n_imags / self.Nshots),
                                            data_array_shape[-2],
                                            data_array_shape[-1])
                                            )
        bright_imag_s_param = ((global_parameters.cooling_line_photon_energy *
                                self.thorcam1.counts_to_photons(bright)) /
                                (self.imag_t_exp_us * us *  
                                global_parameters.pixel_area_eff *
                                global_parameters.cooling_line_i_sat_pol))
        bright_imag_s_param_for_plotting = np.reshape(np.mean(bright_imag_s_param, axis=-3),
                                            (int(self.n_imags / self.Nshots),
                                            data_array_shape[-2],
                                            data_array_shape[-1])
                                            )
        x0=40
        y0=40
        wx=10
        wy=20
        # bright_imag_s_param_for_plotting[:,x0-wx:x0+wx,y0-wy:y0+wy] = 1
        dark_imag_for_plotting = np.reshape(np.mean(dark, axis=-3),
                                            (int(self.n_imags / self.Nshots),
                                            data_array_shape[-2],
                                            data_array_shape[-1])
                                            )
        od_imag_for_plotting = np.reshape(np.mean(od_imag, axis=-3),
                                            (int(self.n_imags / self.Nshots),
                                            data_array_shape[-2],
                                            data_array_shape[-1])
                                            )

        # fig = plt.figure(figsize=(2 * self.data_shape[0], 2))
        # grid = ImageGrid(fig, 111, cbar_mode = 'single', share_all = True,
        #                 nrows_ncols = (1 , self.data_shape[0]),
        #                 )
        # clim = (0, 1024)
        # for ax, im in zip(grid, sig_imag_for_plotting): 
        #     im = ax.imshow(im, cmap='GnBu_r', clim=clim)
        # ax.cax.colorbar(im)
        # fig.suptitle('Signal image, in counts')
        # plt.tight_layout()
        # plt.show()

        # fig = plt.figure(figsize=(2 * self.data_shape[0], 2  ))
        # grid = ImageGrid(fig, 111, cbar_mode = 'single', share_all = True,
        #                 nrows_ncols = ( 1, self.data_shape[0]),
        #                 )
        # clim = (0, 1048)
        # for ax, im in zip(grid, bright_imag_for_plotting): 
        #     im = ax.imshow(im, cmap='GnBu_r', clim=clim)
        # ax.cax.colorbar(im)
        # fig.suptitle('Bright background signal, in counts')
        # plt.tight_layout()
        # plt.show()

        fig = plt.figure(figsize=(2 * self.data_shape[0], 2 ))
        grid = ImageGrid(fig, 111, cbar_mode = 'single', share_all = True,
                        nrows_ncols = (1 , self.data_shape[0]),
                        )
        clim = (0, np.max(bright_imag_s_param_for_plotting))
        for ax, im in zip(grid, bright_imag_s_param_for_plotting): 
            im = ax.imshow(im, cmap='GnBu_r', clim=clim)
        ax.cax.colorbar(im)
        fig.suptitle('Bright background signal, in I/Isat')
        plt.tight_layout()
        plt.show()

        # fig = plt.figure(figsize=(2 * self.data_shape[0], 2 * self.data_shape[1] * self.data_shape[2] ))
        # grid = ImageGrid(fig, 111, cbar_mode = 'single', share_all = True,
        #                 nrows_ncols = (self.data_shape[1] * self.data_shape[2] , self.data_shape[0]),
        #                 )
        # clim = (0, 1024)
        # for ax, im in zip(grid, dark_imag_for_plotting): 
        #     im = ax.imshow(im, cmap='GnBu_r', clim=clim)
        # ax.cax.colorbar(im)
        # plt.show()

        fig = plt.figure(figsize=(2 * self.data_shape[0], 2 ))
        grid = ImageGrid(fig, 111, cbar_mode = 'single', share_all = True,
                        nrows_ncols = (1, self.data_shape[0]),
                        )
        clim = (0.0, 1.0)
        for ax, im in zip(grid, trans_imag_for_plotting): 
            im = ax.imshow(im, cmap='GnBu_r', clim=clim)
        ax.cax.colorbar(im)
        fig.suptitle('Transmission images')
        plt.tight_layout()
        plt.show()

        fig = plt.figure(figsize=(2 * self.data_shape[0], 2 ))
        grid = ImageGrid(fig, 111, cbar_mode = 'single', share_all = True,
                        nrows_ncols = (1 , self.data_shape[0]),
                        )
        clim = (0, np.max(od_imag_for_plotting))
        for ax, im in zip(grid, od_imag_for_plotting): 
            im = ax.imshow(im, cmap='GnBu_r', clim=clim)
        ax.cax.colorbar(im)
        fig.suptitle('OD images')
        plt.tight_layout()
        plt.show()

        od_to_atom_num = global_parameters.pixel_area_eff / global_parameters.cooling_line_cross_sec_unpol

        atom_num_imag = od_imag * od_to_atom_num
        atom_num_imag_err = od_imag_err * od_to_atom_num
        atom_num_imag_mean = od_imag_mean * od_to_atom_num
        atom_num_imag_mean_err = od_imag_mean_err * od_to_atom_num
        atom_num_imag_std = od_imag_std * od_to_atom_num

        print(atom_num_imag.shape)
        atom_num = np.sum(atom_num_imag[:,:,x0-wx:x0+wx,y0-wy:y0+wy], axis=(-2,-1))
        # atom_num = np.sum(atom_num_imag[:,:,:,:,:,:], axis=(-2,-1))
        atom_num_mean = np.mean(atom_num, axis=-1)
        atom_num_std = np.std(atom_num, axis=-1)
        atom_num_err = np.sqrt(np.sum(atom_num_imag_mean_err[:,:,:]**2,
            axis=(-2,-1)))

        print('plotting data')
        fig, ax = plt.subplots(figsize=(6,4))

        mec_list = ['darkslategrey', 'maroon', 'indigo', 'black', 'darkgoldenrod', 'darkolivegreen']
        mfc_list = ['darkturquoise', 'indianred', 'darkorchid', 'grey', 'gold', 'greenyellow']
        markers = ['o','s', 'D','^', '*', 'p']
        # for i, amp in enumerate(self.mot_aom_amps_V):
        #     for j, curr in enumerate(self.mot_currents_amps):
        i = 0
        j = 0

        ax.errorbar([t for t in self.imag_delay_times_ms],
                    atom_num_mean,
                    yerr = atom_num_std,# / np.max(fluor_counts_mean_zeroed),
                    ls='', marker=markers[0],
                    ecolor=mec_list[0],
                    mec=mec_list[0],
                    mfc=mfc_list[0])#,
                    # label = 'AOM amp=%.2fV,IMOT=%.2fA'%(self.mot_aom_amps_V[0], curr))
        # ax.legend()
        # ax.set_ylim(-0.05, 1.05)
        ax.set_xlabel(r'$t$ (ms)')
        ax.set_ylabel(r'Atom Num')

        plt.tight_layout()
        plt.show()

        # for i, det in enumerate([d for d in self.mot_detunings_gamma]):
        #     fig, ax = plt.subplots(figsize=(6,4))
        #     delta_aom_amps = self.mot_aom_amps[1] - self.mot_aom_amps[0]
        #     delta_currents = self.mot_currents[1] - self.mot_currents[0]
        #     im = ax.imshow(atom_num_mean[i,:,:], cmap='GnBu_r',
        #                     origin = 'lower', 
        #                     extent = (np.min(self.mot_currents) - 0.5 * delta_currents,
        #                                 np.max(self.mot_currents) + 0.5 * delta_currents,
        #                                 np.min(self.mot_aom_amps) - 0.5 * delta_aom_amps,
        #                                 np.max(self.mot_aom_amps) + 0.5 * delta_aom_amps),
        #                     aspect = 'auto')
        #     plt.colorbar(im)
        #     ax.set_ylabel('MOT AOM Voltage [V]')
        #     ax.set_xlabel('MOT Coil Current [A]')
        #     ax.set_title(r'MOT $\delta=$'+str(det)+r' $[\Gamma]$')

        

        print("Done with analyze stage.")