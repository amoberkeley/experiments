import numpy as np
import scipy.constants as cnst

cooling_line_freq = {'46': 601.615173,
					'48': 601.615878,#80, -440 MHz
					'50': 601.616554} #THz
op_line_freq = {'46': 765.66590, # seems to be 6 MHz red of resonance, but gives highest atom number
				'48': 765.66685, # seems to be 5 MHz blue of resonance, but gives highest atom number
				'50': 765.667732}# seems to be 2.5 MHz blue of resonance, but gives highest atom number
cooling_gamma_linear = 10.8 * cnst.mega
cooling_line_wavelength = 498.3121 * cnst.nano
cooling_line_cross_sec_pol = cooling_line_wavelength**2 * (3.0 / (2 * np.pi))
cooling_line_cross_sec_unpol = cooling_line_wavelength**2 / (2 * np.pi)
cooling_line_photon_energy = cooling_line_freq['48'] * cnst.tera * cnst.Planck
cooling_line_i_sat_pol = ((cnst.Planck * cooling_line_freq['48'] * cnst.tera) *
							(2 * np.pi * cooling_gamma_linear) / 
							(2 * cooling_line_cross_sec_pol))
cooling_line_i_sat_unpol = ((cnst.Planck * cooling_line_freq['48'] * cnst.tera) *
							(2 * np.pi * cooling_gamma_linear) / 
							(2 * cooling_line_cross_sec_unpol))

zelux_pixel_size = 3.45 * cnst.micro
imaging_magnification = 60.0 / 150.0
pixel_area_eff = (zelux_pixel_size / imaging_magnification)**2

defaults = {
	
}