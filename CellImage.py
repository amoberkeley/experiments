from artiq.experiment import *
import numpy as np

class CellImage(EnvExperiment):
    
    def build(self):
        self.setattr_device("core")
        self.setattr_device("ttl4") # ablation laser
        self.setattr_device("ttl5") # camera trigger
        self.setattr_device("ttl6") # redpitaya trigger
        self.setattr_device("urukul0_ch1")
        self.setattr_device("thorcam2")
        #self.setattr_device("redpitaya0")
        #self.setattr_device("mfc0")
        self.setattr_argument("Nshots", NumberValue(default=10, ndecimals=0, step=1))
        self.setattr_argument("Ntrace_avgs", NumberValue(default=100, ndecimals=0, step=1))
        #self.setattr_argument("flowrate", NumberValue(default=1, ndecimals=2, step=1)) #Scannable(default = [NoScan(1.0), RangeScan(start = 0.1, stop = 14, npoints = 6)], ndecimals = 2, global_step = 0.1, global_min = 0, global_max = 14))#, group = "Mass Flow Rate", tooltip = "buffer gas flowrate")
        self.a = 1

    @rpc(flags={"async"})
    def save_imgs(self):
        imgs = self.thorcam2.get_flattened_buffer()
        img_errs = self.thorcam2.get_flattened_buffer('proc_errs')
        self.set_dataset('imgs', imgs)
        self.set_dataset('img_errs', img_errs)
        #self.set_dataset('flowrate', self.flowrate)

    # @rpc(flags={"async"})
    # def save_timing_traces(self):
    #     traces_cha = self.redpitaya0.get_flattened_buffer('a')
    #     traces_chb = self.redpitaya0.get_flattened_buffer('b')
    #     self.set_dataset('timing_traces_cha', traces_cha)
    #     self.set_dataset('timing_traces_chb', traces_chb)

    # @rpc(flags={"async"})
    # def save_abs_traces(self):
    #     traces_cha = self.redpitaya0.get_flattened_buffer('a')
    #     traces_chb = self.redpitaya0.get_flattened_buffer('b')
    #     self.set_dataset('abs_traces_cha', traces_cha)
    #     self.set_dataset('abs_traces_chb', traces_chb)

    @rpc(flags={"async"})
    def save_bright_imgs(self):
        imgs = self.thorcam2.get_flattened_buffer(buffer_type='bright')
        self.set_dataset('bright_imgs', imgs)

    @kernel
    def run(self):
        self.core.wait_until_mu(now_mu())
        self.core.reset()
        self.core.break_realtime()
        self.a

        ### ttl initialization
        self.ttl4.output()
        self.ttl5.output()
        self.ttl6.output()

        ### Urukul initialization
        self.urukul0_ch1.cpld.init()
        delay(1*s)
        self.urukul0_ch1.init()
        delay(10 * ms)
        freq = 350*MHz
        amp = 0.1
        attenuation = 0.0
        self.urukul0_ch1.set_att(attenuation)
        self.urukul0_ch1.set(freq, amplitude = amp)
        self.urukul0_ch1.sw.off()

        ### Thorcam initialization
        self.thorcam2.connect()
        delay(1*s)
        self.thorcam2.set_roi(0,0,1440,1080)
        delay(0.5*s)
        self.thorcam2.arm()
        delay(1*s)

        # mfc initialization
        #self.mfc0.connect()

        qswitch_delay = 120*us # should be 20us less than qswitch delay which is 140

        #ret = self.mfc0.set_flow_rate(self.flowrate)
        
        self.core.break_realtime()
        ### take darkfield bg
        print("taking bg")
        for i in range(self.Nshots):
            delay(0.1*s)
            self.ttl5.pulse(40*us)
            self.thorcam2.rpc_capture_and_append_to_buffer('dark')

        self.core.break_realtime()

        delay(0.5*s)

        ## take signal
        ds = list(range(20))#list(range(20)) + list(range(20,110,10)) + list(range(100,1100,100))
        for d in ds:
            print("delay:",d)
            self.core.break_realtime()
            #ret1 = self.redpitaya0.record_next_N_traces(self.Nshots, 4096, decimation=8)
            self.core.break_realtime()
            for i in range(self.Nshots):
                print(i)
                delay(0.05*s)

                ### bright background
                self.ttl5.pulse(20*us)
                with parallel:
                    self.urukul0_ch1.sw.on()
                    delay(1*us)
                self.urukul0_ch1.sw.off()
                delay(19*us)
                self.thorcam2.rpc_capture_and_append_to_buffer('bright')

                delay(0.05*s)

                ### bright background 2
                with parallel:
                    self.ttl4.pulse(20*us) # trigger flash lamp and 
                    delay(qswitch_delay) # qswitch delay is 135us
                delay(d*us)
                self.ttl5.pulse(20*us)
                delay(20*us)
                self.thorcam2.rpc_capture_and_append_to_buffer('bright2')

                delay(0.1*s)

                ### signal
                with parallel:
                    self.ttl4.pulse(20*us) # trigger flash lamp and 
                    self.ttl6.pulse(20*us)
                    delay(qswitch_delay) # qswitch delay is 140us nominally
                delay(d*us)
                self.ttl5.pulse(20*us)
                with parallel:
                    self.urukul0_ch1.sw.on()
                    delay(1*us)
                self.urukul0_ch1.sw.off()
                delay(19*us)
                delay(5*us)
                self.thorcam2.rpc_capture_and_append_to_buffer()

            print("Adding to buffer...")
            self.thorcam2.process_imgs()
            self.thorcam2.clear_buffer()
            self.thorcam2.clear_buffer(buffer_type="bright")
            self.thorcam2.clear_buffer(buffer_type="bright2")
            #self.redpitaya0.append_traces_to_buffer(avg=True)

        print("Reading out...")
        self.save_imgs()
        #self.save_timing_traces()
        #ret2 = self.redpitaya0.clear_buffers()
        self.core.break_realtime()




        # rec2 = self.redpitaya0.record_next_N_traces(self.Ntrace_avgs, 16384, decimation=64)
        # self.core.break_realtime()
        # for i in range(self.Ntrace_avgs):
        #     print(i)
        #     delay(0.1*s)
        #     with parallel:
        #         self.ttl4.pulse(20*us) # trigger flash lamp and 
        #         self.ttl6.pulse(20*us)
        # print("Adding to buffer...")
        # self.redpitaya0.append_traces_to_buffer(avg=True)

        print("Reading out...")
        #self.save_abs_traces()
        self.thorcam2.show_processed_imgs()
        #self.redpitaya0.show_traces(chb_on=False)
        #self.thorcam2.clear_buffer("all")

        self.core.break_realtime()
        self.urukul0_ch1.sw.on()
        #self.mfc0.set_flow_rate(0.0)
        #self.core.wait_until_mu(now_mu())
        print("Done.")